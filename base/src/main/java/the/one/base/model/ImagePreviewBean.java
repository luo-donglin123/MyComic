package the.one.base.model;

import android.os.Parcel;
import android.os.Parcelable;

import the.one.base.Interface.ImageSnap;

public class ImagePreviewBean implements ImageSnap, Parcelable {

    private String url;
    private String thumbnail;
    private String refer;
    private boolean isVideo = false;
    private int width;
    private int height;

    public ImagePreviewBean() {
    }

    public ImagePreviewBean(ImageSnap data) {
        this.url = data.getImageUrl();
        this.thumbnail = data.getThumbnail();
        this.refer = data.getRefer();
        this.isVideo = data.isVideo();
        this.width = data.getWidth();
        this.height = data.getHeight();
    }

    public ImagePreviewBean(String url) {
        this.url = url;
    }

    public ImagePreviewBean(String url, String thumbnail, String refer, boolean isVideo) {
        this.url = url;
        this.thumbnail = thumbnail;
        this.refer = refer;
        this.isVideo = isVideo;
    }

    public ImagePreviewBean(String url, String refer) {
        this.url = url;
        this.refer = refer;
    }

    public ImagePreviewBean(String url, String refer, boolean isVideo) {
        this.url = url;
        this.refer = refer;
        this.isVideo = isVideo;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public void setRefer(String refer) {
        this.refer = refer;
    }

    public void setVideo(boolean video) {
        isVideo = video;
    }

    @Override
    public String getImageUrl() {
        return url;
    }

    @Override
    public String getThumbnail() {
        return null;
    }

    @Override
    public String getRefer() {
        return refer;
    }

    @Override
    public boolean isVideo() {
        return isVideo;
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.url);
        dest.writeString(this.thumbnail);
        dest.writeString(this.refer);
        dest.writeByte(this.isVideo ? (byte) 1 : (byte) 0);
        dest.writeInt(this.width);
        dest.writeInt(this.height);
    }

    protected ImagePreviewBean(Parcel in) {
        this.url = in.readString();
        this.thumbnail = in.readString();
        this.refer = in.readString();
        this.isVideo = in.readByte() != 0;
        this.width = in.readInt();
        this.height = in.readInt();
    }

    public static final Creator<ImagePreviewBean> CREATOR = new Creator<ImagePreviewBean>() {
        @Override
        public ImagePreviewBean createFromParcel(Parcel source) {
            return new ImagePreviewBean(source);
        }

        @Override
        public ImagePreviewBean[] newArray(int size) {
            return new ImagePreviewBean[size];
        }
    };
}
