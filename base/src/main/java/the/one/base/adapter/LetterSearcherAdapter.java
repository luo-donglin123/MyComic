package the.one.base.adapter;

import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.qmuiteam.qmui.widget.section.QMUISection;
import com.qmuiteam.qmui.widget.section.QMUIStickySectionAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.appcompat.widget.AppCompatImageView;
import the.one.base.R;
import the.one.base.model.LetterSearchSection;
import the.one.base.util.ColorUtils;
import the.one.base.util.glide.GlideUtil;
import the.one.base.widge.CircleTextView;
import the.one.base.widge.TheCheckBox;


//  ┏┓　　　┏┓
//┏┛┻━━━┛┻┓
//┃　　　　　　　┃
//┃　　　━　　　┃
//┃　┳┛　┗┳　┃
//┃　　　　　　　┃
//┃　　　┻　　　┃
//┃　　　　　　　┃
//┗━┓　　　┏━┛
//    ┃　　　┃                  神兽保佑
//    ┃　　　┃                  永无BUG！
//    ┃　　　┗━━━┓
//    ┃　　　　　　　┣┓
//    ┃　　　　　　　┏┛
//    ┗┓┓┏━┳┓┏┛
//      ┃┫┫　┃┫┫
//      ┗┻┛　┗┻┛

/**
 * @author The one
 * @date 2019/7/19 0019
 * @describe TODO
 * @email 625805189@qq.com
 * @remark
 */
public class LetterSearcherAdapter<T> extends BaseQMUISectionAdapter<LetterSearchSection, LetterSearchSection> {

    private static final String TAG = "LetterSearcherAdapter";

    private HashMap<LetterSearchSection, T> mDataMap;
    private HashMap<LetterSearchSection, T> selects;
    private boolean showCheckBox = false;
    private onLetterSearchItemClickListener onLetterSearchItemClickListener;

    public void setOnLetterSearchItemClickListener(LetterSearcherAdapter.onLetterSearchItemClickListener onLetterSearchItemClickListener) {
        this.onLetterSearchItemClickListener = onLetterSearchItemClickListener;
    }

    public LetterSearcherAdapter() {
        super(R.layout.item_letter_search_head, R.layout.item_letter_search_content);
        selects = new HashMap<>();
    }

    public boolean isShowCheckBox() {
        return showCheckBox;
    }

    public void setShowCheckBox(boolean showCheckBox) {
        this.showCheckBox = showCheckBox;
        if (!showCheckBox) {
            selects.clear();
        }
        notifyDataSetChanged();
    }

    public void setSelects(int position) {
        if (position < 0) return;
        LetterSearchSection section = getSectionItem(position);
        if (selects.containsKey(section))
            selects.remove(section);
        else {
            selects.put(section, mDataMap.get(section));
        }
        notifyItemChanged(position);
    }

    public void setSelects(HashMap<LetterSearchSection, T> selects) {
        this.selects = selects;
        notifyDataSetChanged();
    }

    public void selectAll(boolean all) {
        selects.clear();
        if (all) {
            for (LetterSearchSection section : mDataMap.keySet()) {
                selects.put(section, mDataMap.get(section));
            }
        }
        notifyDataSetChanged();
    }

    public void setSelectsMap(HashMap<LetterSearchSection, T> dataMap) {
        mDataMap = dataMap;
    }

    public List<T> getSelects() {
        return (List<T>) new ArrayList<>(selects.values());
    }

    public T getT(int position) {
        LetterSearchSection section = getSectionItem(position);
        return mDataMap.get(section);
    }

    public boolean isAllSelect() {
        return selects.size() == mDataMap.size();
    }

    private boolean isHeaderTextUseColorId = true;

    public void setHeaderTextUseColorId(boolean headerTextUseColorId) {
        isHeaderTextUseColorId = headerTextUseColorId;
    }

    @Override
    protected void onBindSectionHeader(ViewHolder holder, int position, QMUISection<LetterSearchSection, LetterSearchSection> section) {
        CircleTextView tvSection = holder.itemView.findViewById(R.id.tv_section);
        String header = section.getHeader().getFirstPinYin();
        tvSection.setText(header);
        if (isHeaderTextUseColorId) {
            tvSection.setBackColor(ColorUtils.getBackgroundColorId(header, mContext));
        }
    }

    @Override
    protected void onBindSectionItem(final ViewHolder holder, final int position, QMUISection<LetterSearchSection, LetterSearchSection> section, int itemIndex) {
        CircleTextView tvName = holder.itemView.findViewById(R.id.tv_name);
        TextView tvContent = holder.itemView.findViewById(R.id.tv_content);
        TheCheckBox cbSelect = holder.itemView.findViewById(R.id.check_box);
        AppCompatImageView ivLogo = holder.itemView.findViewById(R.id.iv_logo);
        cbSelect.setFocusable(false);
        cbSelect.setOnClickListener(null);
        LetterSearchSection itemSection = section.getItemAt(itemIndex);
        if (TextUtils.isEmpty(itemSection.getLogo())) {
            ivLogo.setVisibility(View.INVISIBLE);
            tvName.setVisibility(View.VISIBLE);
            setCircleTextViewData(tvName, itemSection.name.substring(0, 1));
        } else {
            ivLogo.setVisibility(View.VISIBLE);
            tvName.setVisibility(View.INVISIBLE);
            GlideUtil.load(mContext, itemSection.getLogo(), ivLogo);
        }
        tvContent.setText(itemSection.name);
        if (showCheckBox) {
            cbSelect.setVisibility(View.VISIBLE);
            boolean exist = selects.containsKey(itemSection);
            cbSelect.setCheck(exist);
        } else {
            cbSelect.setVisibility(View.GONE);
        }
        View rootView = holder.itemView.findViewById(R.id.ll_contact);
        rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != onLetterSearchItemClickListener) {
                    onLetterSearchItemClickListener.onSearchItemClick(holder, position);
                }
            }
        });
        rootView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (null != onLetterSearchItemClickListener) {
                    return onLetterSearchItemClickListener.onSearchItemLongClick(holder, position);
                }
                return false;
            }
        });
    }

    private void setCircleTextViewData(CircleTextView textView, String content) {
        textView.setText(content);
        textView.setBackColor(ColorUtils.getBackgroundColorId(content, mContext));
    }

    public interface onLetterSearchItemClickListener {
        void onSearchItemClick(QMUIStickySectionAdapter.ViewHolder holder, int position);

        boolean onSearchItemLongClick(QMUIStickySectionAdapter.ViewHolder holder, int position);
    }

}
