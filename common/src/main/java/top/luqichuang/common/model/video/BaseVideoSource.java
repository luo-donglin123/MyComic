package top.luqichuang.common.model.video;

import top.luqichuang.common.en.VSourceEnum;
import top.luqichuang.common.model.BaseSource;
import top.luqichuang.common.model.Entity;
import top.luqichuang.common.model.EntityInfo;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/6/15 14:24
 * @ver 1.0
 */
public abstract class BaseVideoSource extends BaseSource {

    @Override
    public int getSourceType() {
        return VIDEO;
    }

    @Override
    public Class<? extends Entity> getEntityClass() {
        return Video.class;
    }

    @Override
    public Class<? extends EntityInfo> getInfoClass() {
        return VideoInfo.class;
    }

    public abstract VSourceEnum getVSourceEnum();

    @Override
    public final int getSourceId() {
        return getVSourceEnum().ID;
    }

    @Override
    public final String getSourceName() {
        return getVSourceEnum().NAME;
    }
}
