package top.luqichuang.common.model.builder;

import top.luqichuang.common.model.ChapterInfo;

/**
 * @author 18472
 * @desc
 * @date 2024/3/27 22:24
 * @ver 1.0
 */
public class ChapterInfoBuilder {

    private ChapterInfo info;

    public ChapterInfoBuilder() {
        this.info = new ChapterInfo();
    }

    public ChapterInfo build() {
        if (info.getTitle() != null) {
            return info;
        } else {
            return null;
        }
    }

    public ChapterInfoBuilder buildTitle(String title) {
        info.setTitle(title);
        return this;
    }

    public ChapterInfoBuilder buildUrl(String url) {
        info.setChapterUrl(url);
        return this;
    }

}
