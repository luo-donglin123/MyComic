package top.luqichuang.common.model.comic;

import top.luqichuang.common.model.EntityInfo;

/**
 * @author LuQiChuang
 * @desc
 * @date 2020/8/12 15:25
 * @ver 1.0
 */
public class ComicInfo extends EntityInfo {

    public ComicInfo() {
        super();
    }

    @Deprecated
    public ComicInfo(int sourceId, String title, String author, String detailUrl, String imgUrl, String updateTime, String updateChapter) {
        this.sourceId = sourceId;
        this.title = title;
        this.author = author;
        this.detailUrl = detailUrl;
        this.imgUrl = imgUrl;
        this.updateTime = updateTime;
        this.updateChapter = updateChapter;
    }

    @Override
    public String toString() {
        return "ComicInfo{" +
                "id=" + id +
                ", sourceId=" + sourceId +
                ", title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", detailUrl='" + detailUrl + '\'' +
                ", imgUrl='" + imgUrl + '\'' +
                ", localImgUrl='" + localImgUrl + '\'' +
                ", updateTime='" + updateTime + '\'' +
                ", updateChapter='" + updateChapter + '\'' +
                ", updateStatus='" + updateStatus + '\'' +
                ", curChapterTitle='" + curChapterTitle + '\'' +
                ", intro='" + intro + '\'' +
                ", curChapterId=" + curChapterId +
                ", chapterNum=" + chapterNum +
                ", order=" + order +
                ", chapterInfoList=" + chapterInfoList +
                ", chapterInfoMap=" + chapterInfoMap +
                '}';
    }
}
