package top.luqichuang.common.model;

import org.litepal.crud.LitePalSupport;

import java.util.Date;

/**
 * @author 18472
 * @desc
 * @date 2024/3/12 13:03
 * @ver 1.0
 */
public class CacheData extends LitePalSupport {

    private long id;

    private String key;

    private String json;

    private Date date;

    private long expire;

    private int status;

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public long getExpire() {
        return expire;
    }

    public void setExpire(long expire) {
        this.expire = expire;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
