package top.luqichuang.common.model;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import top.luqichuang.common.model.builder.ChapterInfoBuilder;
import top.luqichuang.common.model.builder.EntityInfoBuilder;
import top.luqichuang.common.starter.ElementNode;
import top.luqichuang.common.starter.ElementStarter;
import top.luqichuang.common.util.SourceHelper;

/**
 * @author 18472
 * @desc
 * @date 2024/3/29 17:17
 * @ver 1.0
 */
public abstract class BaseSource implements Source {

//    default Request getSearchRequest(String url, String searchString) {
//        if (url != null && url.contains("%s")) {
//            return NetUtil.getRequest(String.format(url, searchString));
//        } else {
//            return NetUtil.getRequest(url + searchString);
//        }
//    }

    @Override
    public List<EntityInfo> getInfoList(String html) {
        List<EntityInfo> list = new ArrayList<>();
        InfoListConfig config = getInfoListConfig(new ElementNode(html));
        if (config != null) {
            List<ListConfig<EntityInfo>> configList = config.getConfigList();
            for (ListConfig<EntityInfo> listConfig : configList) {
                if (listConfig != null) {
                    list.addAll(new ElementStarter<EntityInfo>() {
                        @Override
                        protected EntityInfo loadListData(ElementNode node) {
                            return listConfig.getData(node);
                        }
                    }.startList(html, listConfig.getQuery()));
                }
            }
        }
        return list;
    }

    @Override
    public void setInfoDetail(EntityInfo info, String html, Map<String, Object> map) {
        InfoDetailConfig config = getInfoDetailConfig(new ElementNode(html));
        if (config != null) {
            //设置Info
            new ElementStarter<EntityInfo>() {
                @Override
                protected EntityInfo loadData(ElementNode node) {
                    config.setData(info, node);
                    return super.loadData(node);
                }
            }.startData(config.getHtml(html, map));

            //设置list
            List<ChapterInfo> list = new ArrayList<>();
            List<ListConfig<ChapterInfo>> configList = config.getConfigList();
            for (ListConfig<ChapterInfo> listConfig : configList) {
                if (listConfig != null) {
                    SourceHelper.initChapterMap(info,
                            listConfig.getListTitle(new ElementNode(listConfig.getHtml(html, map))),
                            new ElementStarter<ChapterInfo>() {
                                @Override
                                protected boolean needSwap(List<ChapterInfo> list) {
                                    return listConfig.needSwap(list);
                                }

                                @Override
                                protected ChapterInfo loadListData(ElementNode node) {
                                    return listConfig.getData(node);
                                }
                            }.startList(listConfig.getHtml(html, map), listConfig.getQuery()));
                }
            }
            SourceHelper.initChapterList(info);
        }
    }

    @Override
    public List<Content> getContentList(String html, int chapterId, Map<String, Object> map) {
        List<String> list = new ArrayList<>();
        ContentListConfig config = getContentListConfig(new ElementNode(html));
        String prev = "";
        if (config != null) {
            ElementNode node = new ElementNode(config.getHtml(html, map));
            config.addUrlList(node, list, map);
            prev = config.getPrev();
        }
        return SourceHelper.getContentList(list, chapterId, prev);
    }

    @Override
    public Map<String, String> getRankMap() {
        Map<String, String> map = new LinkedHashMap<>();
        RankMapConfig config = getRankMapConfig();
        if (config != null) {
            ElementNode node = new ElementNode(config.getHtml());
            Elements elements = node.getElements(config.getCssQuery());
            for (Element element : elements) {
                node.init(element);
                map.put(config.getTitle(node), config.getUrl(node));
            }
        }
        return map;
    }

    @Override
    public List<EntityInfo> getRankInfoList(String html) {
        List<EntityInfo> list = new ArrayList<>();
        InfoListConfig config = getRankListConfig(new ElementNode(html));
        if (config != null) {
            List<ListConfig<EntityInfo>> configList = config.getConfigList();
            for (ListConfig<EntityInfo> listConfig : configList) {
                if (listConfig != null) {
                    list.addAll(new ElementStarter<EntityInfo>() {
                        @Override
                        protected EntityInfo loadListData(ElementNode node) {
                            return listConfig.getData(node);
                        }
                    }.startList(html, listConfig.getQuery()));
                }
            }
        }
        return list;
    }

    /*============================================================================================*/
    protected InfoListConfig getInfoListConfig(ElementNode node) {
        return new InfoListConfig() {
            @Override
            protected void addListConfig(List<ListConfig<EntityInfo>> configList) {
                configList.add(new ListConfig<EntityInfo>() {
                    @Override
                    protected String[] getQuery() {
                        return new String[]{
                                ""
                        };
                    }

                    @Override
                    protected EntityInfo getData(ElementNode node) {
                        return new EntityInfoBuilder(getInfoClass())
                                .buildSourceId(getSourceId())
                                .buildTitle(null)
                                .buildAuthor(null)
                                .buildUpdateTime(null)
                                .buildUpdateChapter(null)
                                .buildImgUrl(node.src("img"))
                                .buildDetailUrl(node.href("a"))
                                .build();
                    }
                });
            }
        };
    }

    protected InfoDetailConfig getInfoDetailConfig(ElementNode node) {
        return new InfoDetailConfig() {
            @Override
            protected void setData(EntityInfo info, ElementNode node) {
                new EntityInfoBuilder(info)
                        .buildSourceId(getSourceId())
                        .buildTitle(null)
                        .buildAuthor(null)
                        .buildIntro(null)
                        .buildUpdateTime(null)
                        .buildUpdateStatus(null)
                        .buildImgUrl(node.src("img"))
                        .build();
            }

            @Override
            protected void addListConfig(List<ListConfig<ChapterInfo>> configList) {
                configList.add(new ListConfig<ChapterInfo>() {
                    @Override
                    protected String[] getQuery() {
                        return new String[]{
                                ""
                        };
                    }

                    @Override
                    protected ChapterInfo getData(ElementNode node) {
                        return new ChapterInfoBuilder()
                                .buildTitle(node.ownText("a"))
                                .buildUrl(node.href("a"))
                                .build();
                    }
                });
            }
        };
    }

    protected ContentListConfig getContentListConfig(ElementNode node) {
        return new ContentListConfig() {
            @Override
            protected void addUrlList(ElementNode node, List<String> urlList, Map<String, Object> map) {

            }
        };
    }

    protected RankMapConfig getRankMapConfig() {
        return new RankMapConfig() {
            @Override
            protected String getHtml() {
                return "html";
            }

            @Override
            protected String getCssQuery() {
                return "a";
            }

            @Override
            protected String getTitle(ElementNode node) {
                return node.ownText("a");
            }

            @Override
            protected String getUrl(ElementNode node) {
                return node.href("a");
            }
        };
    }

    protected InfoListConfig getRankListConfig(ElementNode node) {
        return getInfoListConfig(node);
    }

    /*============================================================================================*/
    public abstract static class ListConfig<T> {
        protected String getHtml(String html, Map<String, Object> map) {
            return html;
        }

        protected boolean needSwap(List<T> list) {
            return false;
        }

        protected abstract String[] getQuery();

        protected String getListTitle(ElementNode node) {
            return "";
        }

        protected abstract T getData(ElementNode node);
    }

    /*============================================================================================*/
    public abstract static class InfoListConfig {
        private List<ListConfig<EntityInfo>> configList = new ArrayList<>();

        public List<ListConfig<EntityInfo>> getConfigList() {
            if (configList.isEmpty()) {
                addListConfig(configList);
            }
            return configList;
        }

        protected abstract void addListConfig(List<ListConfig<EntityInfo>> configList);
    }

    /*============================================================================================*/
    public abstract static class InfoDetailConfig {
        private List<ListConfig<ChapterInfo>> configList = new ArrayList<>();

        public List<ListConfig<ChapterInfo>> getConfigList() {
            if (configList.isEmpty()) {
                addListConfig(configList);
            }
            return configList;
        }

        protected String getHtml(String html, Map<String, Object> map) {
            return html;
        }

        protected abstract void setData(EntityInfo info, ElementNode node);

        protected abstract void addListConfig(List<ListConfig<ChapterInfo>> configList);
    }

    /*============================================================================================*/
    public abstract static class ContentListConfig {
        protected String getHtml(String html, Map<String, Object> map) {
            return html;
        }

        protected String getPrev() {
            return "";
        }

        protected abstract void addUrlList(ElementNode node, List<String> urlList, Map<String, Object> map);
    }

    /*============================================================================================*/
    public abstract static class RankMapConfig {
        protected abstract String getHtml();

        protected abstract String getCssQuery();

        protected abstract String getTitle(ElementNode node);

        protected abstract String getUrl(ElementNode node);
    }

}
