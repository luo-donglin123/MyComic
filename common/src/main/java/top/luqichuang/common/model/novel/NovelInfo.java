package top.luqichuang.common.model.novel;

import top.luqichuang.common.model.EntityInfo;

/**
 * @author LuQiChuang
 * @desc
 * @date 2020/8/12 15:25
 * @ver 1.0
 */
public class NovelInfo extends EntityInfo {

    public NovelInfo() {
        super();
    }

    @Deprecated
    public NovelInfo(int sourceId, String title, String author, String detailUrl, String imgUrl, String updateTime) {
        this.sourceId = sourceId;
        this.title = title;
        this.author = author;
        this.detailUrl = detailUrl;
        this.imgUrl = imgUrl;
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "NovelInfo{" +
                "id=" + id +
                ", sourceId=" + sourceId +
                ", title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", detailUrl='" + detailUrl + '\'' +
                ", imgUrl='" + imgUrl + '\'' +
                ", localImgUrl='" + localImgUrl + '\'' +
                ", updateTime='" + updateTime + '\'' +
                ", updateChapter='" + updateChapter + '\'' +
                ", updateStatus='" + updateStatus + '\'' +
                ", curChapterTitle='" + curChapterTitle + '\'' +
                ", intro='" + intro + '\'' +
                ", curChapterId=" + curChapterId +
                ", chapterNum=" + chapterNum +
                ", order=" + order +
                ", chapterInfoList=" + chapterInfoList +
                ", chapterInfoMap=" + chapterInfoMap +
                '}';
    }
}
