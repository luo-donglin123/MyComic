package top.luqichuang.common.util;

import com.alibaba.fastjson.JSON;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import top.luqichuang.common.model.CacheData;

/**
 * @author 18472
 * @desc
 * @date 2024/3/12 14:24
 * @ver 1.0
 */
public class CacheUtil {

    public static final long DEFAULT_EXPIRE = 30 * 24 * 60 * 60 * 1000L;

    public static final long SHORT_EXPIRE = 6 * 60 * 60 * 1000L;

    /*-----------------------------------------------------------------------------------------*/
    public static CacheData getCacheData(String key, String json) {
        return getCacheData(key, json, DEFAULT_EXPIRE);
    }

    public static CacheData getCacheData(String key, String json, long expire) {
        if (key == null || json == null) {
            return null;
        }
        CacheData cacheData = new CacheData();
        cacheData.setKey(key);
        cacheData.setJson(json);
        cacheData.setDate(new Date());
        cacheData.setExpire(expire);
        cacheData.setStatus(0);
        return cacheData;
    }

    /*-----------------------------------------------------------------------------------------*/
    public static String toJson(Object obj) {
        try {
            return JSON.toJSONString(obj);
        } catch (Exception e) {
            return null;
        }
    }

    public static <T> T toObject(String json, Class<T> clazz) {
        try {
            return JSON.parseObject(json, clazz);
        } catch (Exception e) {
            return null;
        }
    }

    public static <T> List<T> toArray(String json, Class<T> clazz) {
        try {
            List<T> list = JSON.parseArray(json, clazz);
            if (list != null) {
                return list;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

}
