package top.luqichuang.common.util;

import org.jetbrains.annotations.NotNull;
import org.jsoup.Jsoup;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * @author LuQiChuang
 * @desc 网络连接工具
 * @date 2020/8/12 15:25
 * @ver 1.0
 */
public class NetUtil {

    public static final String USER_AGENT = "Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 Mobile Safari/537.36";

    public static final String USER_AGENT_WEB = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 Safari/537.36";

    private static final String TAG = "NetUtil";

    private static final String ERROR_URL = "https://error_url/";

    private static final Map<String, Integer> ERROR_COUNT_MAP = new HashMap<>();

    public static final int TIME_OUT = 20;

    private static OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .callTimeout(TIME_OUT, TimeUnit.SECONDS)
            .connectTimeout(TIME_OUT, TimeUnit.SECONDS)
            .readTimeout(TIME_OUT, TimeUnit.SECONDS)
            .writeTimeout(TIME_OUT, TimeUnit.SECONDS)
            .build();

    private static OkHttpClient preloadClient = new OkHttpClient.Builder()
            .callTimeout(TIME_OUT, TimeUnit.SECONDS)
            .connectTimeout(TIME_OUT, TimeUnit.SECONDS)
            .readTimeout(TIME_OUT, TimeUnit.SECONDS)
            .writeTimeout(TIME_OUT, TimeUnit.SECONDS)
            .build();

    static {
//        okHttpClient.dispatcher().setMaxRequests(20);
        preloadClient.dispatcher().setMaxRequests(3);
    }

//    private static OkHttpClient client = new OkHttpClient.Builder().build();

    /**
     * 根据url，获得默认request
     *
     * @param url url
     * @return Request
     */
    public static Request getRequest(String url) {
        return getRequest(url, null, null);
    }

    /**
     * 设置获得request的userAgent
     *
     * @param url       url
     * @param userAgent userAgent
     * @return Request
     */
    public static Request getRequest(String url, String userAgent) {
        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("User-Agent", userAgent);
        return getRequest(url, headerMap, null);
    }

    /**
     * 通过map设置请求，获得Request
     *
     * @param url       url
     * @param headerMap 存放头部信息
     * @return Request
     */
    public static Request getRequestByHeader(String url, Map<String, String> headerMap) {
        return getRequest(url, headerMap, null);
    }

    /**
     * 通过map设置请求，获得Request
     *
     * @param url      url
     * @param paramMap 存放param信息
     * @return Request
     */
    public static Request getRequest(String url, Map<String, String> paramMap) {
        return getRequest(url, null, paramMap);
    }

    /**
     * 通过map设置请求，获得Request
     *
     * @param url       url
     * @param headerMap 存放头部信息
     * @param paramMap  存放param信息
     * @return Request
     */
    public static Request getRequest(String url, Map<String, String> headerMap, Map<String, String> paramMap) {
        if (url == null) {
            url = ERROR_URL;
        }
        if (headerMap == null) {
            headerMap = new HashMap<>();
        }
        if (!headerMap.containsKey("User-Agent")) {
            headerMap.put("User-Agent", getAgent(url));
        }
        Headers.Builder headersBuilder = new Headers.Builder();
        for (Map.Entry<String, String> entry : headerMap.entrySet()) {
            headersBuilder.add(entry.getKey(), entry.getValue());
        }
        Headers headers = headersBuilder.build();
        if (paramMap != null) {
            StringBuilder stringBuilder = new StringBuilder(url + "?");
            for (Map.Entry<String, String> entry : paramMap.entrySet()) {
                if (entry.getKey() != null && entry.getValue() != null) {
                    stringBuilder.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
                }
            }
            url = stringBuilder.toString();
        }
        try {
            return new Request.Builder().url(url).headers(headers).method("GET", null).build();
        } catch (Exception e) {
            return new Request.Builder().url(ERROR_URL).build();
        }
    }

    /**
     * 发送post请求
     *
     * @param url  url
     * @param data data
     * @return Request
     */
    public static Request postRequest(String url, String... data) {
        Map<String, String> map = new HashMap<>();
        if (data != null && data.length > 0 && data.length % 2 == 0) {
            for (int i = 0; i < data.length; i = i + 2) {
                map.put(data[i], data[i + 1]);
            }
        }
        return postRequest(url, map);
    }

    /**
     * 发送post请求
     *
     * @param url         url
     * @param formDataMap formDataMap
     * @return Request
     */
    public static Request postRequest(String url, Map<String, String> formDataMap) {
        return postRequest(url, null, formDataMap, null);
    }

    public static Request postRequest(String url, String raw) {
        return postRequest(url, null, null, raw);
    }

    /**
     * 发送post请求
     *
     * @param url         url
     * @param headerMap   headerMap
     * @param formDataMap formDataMap
     * @return Request
     */
    public static Request postRequest(String url, Map<String, String> headerMap, Map<String, String> formDataMap, String raw) {
        if (url == null) {
            url = ERROR_URL;
        }
        if (headerMap == null) {
            headerMap = new HashMap<>();
        }
        if (!headerMap.containsKey("User-Agent")) {
            headerMap.put("User-Agent", getAgent(url));
        }
        Request.Builder builder = new Request.Builder();
        for (Map.Entry<String, String> entry : headerMap.entrySet()) {
            if (entry.getKey() != null && entry.getValue() != null) {
                builder.addHeader(entry.getKey(), entry.getValue());
            }
        }
        RequestBody body;
        String tag;
        if (raw == null) {
            MultipartBody.Builder bodyBuilder = new MultipartBody.Builder().setType(MultipartBody.FORM);
            if (formDataMap == null) {
                formDataMap = new HashMap<>();
            }
            StringBuilder tagBuilder = new StringBuilder();
            for (Map.Entry<String, String> entry : formDataMap.entrySet()) {
                if (entry.getKey() != null && entry.getValue() != null) {
                    bodyBuilder.addFormDataPart(entry.getKey(), entry.getValue());
                    tagBuilder.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
                }
            }
            body = bodyBuilder.build();
            tag = tagBuilder.toString();
        } else {
            MediaType json = MediaType.parse("application/x-www-form-urlencoded");
            body = RequestBody.create(json, raw);
            tag = raw;
        }
        try {
            return builder.url(url).post(body).tag(tag).build();
        } catch (Exception e) {
            return new Request.Builder().url(ERROR_URL).build();
        }
    }

    public static int runningCallsCount() {
        return okHttpClient.dispatcher().runningCallsCount();
    }

    public static int queuedCallsCount() {
        return okHttpClient.dispatcher().queuedCallsCount();
    }

    /**
     * 使用默认request开始连接网络
     *
     * @param url      url
     * @param callback callback
     * @return void
     */
    public static void startLoad(String url, Callback callback) {
        startLoad(getRequest(url), callback);
    }

    /**
     * 使用特定request开始连接网络
     *
     * @param request  request
     * @param callback callback
     * @return void
     */
    public static void startLoad(Request request, Callback callback) {
        startLoad(request, callback, false);
    }

    public static void startLoad(Request request, Callback callback, boolean isPreload) {
//        System.out.println("request.url() = " + request.url());
        System.out.println("request = " + request);
        if (!isPreload) {
            Call call = okHttpClient.newCall(request);
            call.enqueue(callback);
        } else {
            Call call = preloadClient.newCall(request);
            call.enqueue(callback);
        }
    }

    /**
     * 使用Jsoup连接网络
     *
     * @param url url
     * @return String
     */
    public static String startLoadWithJsoup(String url) {
        String html = null;
        try {
            html = Jsoup.connect(url).get().html();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        return html;
    }

    private static String getAgent(String url) {
        if (url != null && url.contains("://m.")) {
            return USER_AGENT;
        } else {
            return USER_AGENT_WEB;
        }
    }

    public static void addErrorUrl(String url) {
        ERROR_COUNT_MAP.put(url, getErrorNum(url) + 1);
    }

    public static boolean needReload(String url, int count) {
        return getErrorNum(url) < count;
    }

    private static int getErrorNum(String url) {
        Integer num = ERROR_COUNT_MAP.get(url);
        if (num == null) {
            return 0;
        } else {
            return num;
        }
    }

    private void demo(String url) {
        //1.创建OkHttpClient对象
        OkHttpClient okHttpClient = new OkHttpClient();
        //2.创建Request对象，设置一个url地址,设置请求方式。
        Request request = new Request.Builder().url(url).method("GET", null).build();
        //3.创建一个call对象,参数就是Request请求对象
        Call call = okHttpClient.newCall(request);
        //4.请求加入调度，重写回调方法
        call.enqueue(new Callback() {
            //请求失败执行的方法
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                //Log.e(TAG, "onFailure: ", e);
            }

            //请求成功执行的方法
            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                String data = response.body().string();
                //Log.i(TAG, "onResponse: " + response.toString());
//                AndroidSchedulers.mainThread().scheduleDirect(new Runnable() {
//                    @Override
//                    public void run() {
//
//                    }
//                });
            }
        });
    }

}
