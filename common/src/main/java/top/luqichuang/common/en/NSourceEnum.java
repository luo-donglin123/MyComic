package top.luqichuang.common.en;

import java.util.LinkedHashMap;
import java.util.Map;

import top.luqichuang.common.model.Source;
import top.luqichuang.common.source.novel.AiYue;
import top.luqichuang.common.source.novel.K17;
import top.luqichuang.common.source.novel.MiKanShu;
import top.luqichuang.common.source.novel.MoYuan;
import top.luqichuang.common.source.novel.QuanShu;
import top.luqichuang.common.source.novel.QuanXiaoShuo;
import top.luqichuang.common.source.novel.TaDu;
import top.luqichuang.common.source.novel.XiaoShuoE;
import top.luqichuang.common.source.novel.XinBiQuGe;
import top.luqichuang.common.source.novel.XinBiQuGe2;
import top.luqichuang.common.source.novel.XuanShu;
import top.luqichuang.mynovel.source.ShuBen;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/1/11 23:51
 * @ver 1.0
 */
public enum NSourceEnum {
    EMPTY_SOURCE(0, "空", null),
    XIN_BI_QU_GE(1, "新笔趣阁", new XinBiQuGe()),
    QUAN_SHU(2, "全书网", new QuanShu()),
    QUAN_XIAO_SHUO(3, "全小说", new QuanXiaoShuo()),
    AI_YUE(4, "爱阅小说", new AiYue()),
    XUAN_SHU(5, "炫书网", new XuanShu()),
    K17(6, "17K小说", new K17()),
    XIAO_SHUO_E(7, "E小说", new XiaoShuoE()),
    MO_YUAN(8, "墨缘文学", new MoYuan()),
    MI_KAN_SHU(9, "Mi看书", new MiKanShu()),
    XIN_BI_QU_GE_2(10, "新笔趣阁[2]", new XinBiQuGe2()),
    SHU_BEN(11, "书本网", new ShuBen()),
    TA_DU(12, "塔读文学", new TaDu()),
    ;

    private static final Map<Integer, Source> MAP = new LinkedHashMap<>();

    static {
        for (NSourceEnum value : values()) {
            if (value.SOURCE != null && value.SOURCE.isValid()) {
                MAP.put(value.ID, (Source) value.SOURCE);
            }
        }
    }

    public static Map<Integer, Source> getMAP() {
        return MAP;
    }

    public final int ID;
    public final String NAME;
    public final Source SOURCE;

    NSourceEnum(int id, String name, Source source) {
        this.ID = id;
        this.NAME = name;
        this.SOURCE = source;
    }
}
