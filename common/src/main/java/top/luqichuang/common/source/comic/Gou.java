package top.luqichuang.common.source.comic;

import java.util.List;
import java.util.Map;

import okhttp3.Request;
import top.luqichuang.common.en.CSourceEnum;
import top.luqichuang.common.model.ChapterInfo;
import top.luqichuang.common.model.EntityInfo;
import top.luqichuang.common.model.builder.ChapterInfoBuilder;
import top.luqichuang.common.model.builder.EntityInfoBuilder;
import top.luqichuang.common.model.comic.BaseComicSource;
import top.luqichuang.common.starter.ElementNode;
import top.luqichuang.common.starter.ElementStarter;
import top.luqichuang.common.util.NetUtil;
import top.luqichuang.common.util.StringUtil;

/**
 * @author 18472
 * @desc
 * @date 2024/3/30 18:08
 * @ver 1.0
 */
public class Gou extends BaseComicSource {
    @Override
    public CSourceEnum getCSourceEnum() {
        return CSourceEnum.GOU;
    }

    @Override
    public String getIndex() {
        return "https://www.manhuagou.com";
    }

    @Override
    public Request getSearchRequest(String searchString) {
        String url = String.format("%s/search?key=%s", getIndex(), searchString);
        return NetUtil.getRequest(url);
    }

    protected InfoListConfig getInfoListConfig(ElementNode node) {
        return new InfoListConfig() {
            @Override
            protected void addListConfig(List<ListConfig<EntityInfo>> configList) {
                configList.add(new ListConfig<EntityInfo>() {
                    @Override
                    protected String[] getQuery() {
                        return new String[]{"ul.fn-clear li"};
                    }

                    @Override
                    protected EntityInfo getData(ElementNode node) {
                        return new EntityInfoBuilder(getInfoClass())
                                .buildSourceId(getSourceId())
                                .buildTitle(node.title("a"))
                                .buildAuthor(node.ownText("p.box-b2-t"))
                                .buildUpdateTime(null)
                                .buildUpdateChapter(node.ownText("i.box-img-txt"))
                                .buildImgUrl(node.attr("img", "data-original"))
                                .buildDetailUrl(getIndex() + node.href("a"))
                                .build();
                    }
                });
            }
        };
    }

    protected InfoDetailConfig getInfoDetailConfig(ElementNode node) {
        return new InfoDetailConfig() {
            @Override
            protected void setData(EntityInfo info, ElementNode node) {
                String updateTime = node.ownText("p.fd-list:eq(3) span");
                updateTime = StringUtil.remove(updateTime, "更新：");
                new EntityInfoBuilder(info)
                        .buildSourceId(getSourceId())
                        .buildTitle(node.ownText("div.crumbs-all"))
                        .buildAuthor(node.ownText("div.crumbs-all a:eq(4)"))
                        .buildIntro(node.ownText("p.vod-jj span"))
                        .buildUpdateTime(updateTime)
                        .buildUpdateStatus(node.ownText("p.fd-list:eq(1) a"))
                        .buildImgUrl(node.src("img"))
                        .build();
            }

            @Override
            protected void addListConfig(List<ListConfig<ChapterInfo>> configList) {
                configList.add(new ListConfig<ChapterInfo>() {
                    @Override
                    protected boolean needSwap(List<ChapterInfo> list) {
                        return true;
                    }

                    @Override
                    protected String[] getQuery() {
                        return new String[]{"div.all-box a"};
                    }

                    @Override
                    protected ChapterInfo getData(ElementNode node) {
                        return new ChapterInfoBuilder()
                                .buildTitle(node.ownText("a"))
                                .buildUrl(getIndex() + node.href("a"))
                                .build();
                    }
                });
            }
        };
    }

    protected ContentListConfig getContentListConfig(ElementNode node) {
        return new ContentListConfig() {
            @Override
            protected void addUrlList(ElementNode node, List<String> urlList, Map<String, Object> map) {
                ElementStarter<String> starter = new ElementStarter<String>() {
                    @Override
                    protected String loadListData(ElementNode node) {
                        return node.attr("img", "data-original");
                    }
                };
                urlList.addAll(starter.startList(node.getHtml(), "div#img-content img"));
            }
        };
    }

    protected RankMapConfig getRankMapConfig() {
        return new RankMapConfig() {
            @Override
            protected String getHtml() {
                return "<div class=\"all-type-layout\"><div class=\"all-type-box fn-clear\"style=\"clear:both\"><span>分类：</span><div class=\"all-box\"><a href=\"/category\"class=\"\">全部</a><a href=\"/category/list/1\"class=\" on\">国产漫画</a><a href=\"/category/list/2\"class=\"\">日本漫画</a><a href=\"/category/list/3\"class=\"\">韩国漫画</a><a href=\"/category/list/4\"class=\"\">欧美漫画</a></div></div><div class=\"all-type-box fn-clear\"><span>类型：</span><div class=\"all-box\"><a href=\"/category/list/1/tags/168\"class=\"\">少年漫画</a><a href=\"/category/list/1/tags/183\"class=\"\">少女漫画</a><a href=\"/category/list/1/tags/197\"class=\"\">青年漫画</a><a href=\"/category/list/1/tags/214\"class=\"\">真人漫画</a><a href=\"/category/list/1/tags/211\"class=\"\">玄幻科幻</a><a href=\"/category/list/1/tags/212\"class=\"\">冒险热血</a><a href=\"/category/list/1/tags/213\"class=\"\">耽美爱情</a><a href=\"/category/list/1/tags/215\"class=\"\">爆笑</a><a href=\"/category/list/1/tags/216\"class=\"\">生活</a><a href=\"/category/list/1/tags/221\"class=\"\">修真</a><a href=\"/category/list/1/tags/218\"class=\"\">神魔</a><a href=\"/category/list/1/tags/219\"class=\"\">日常</a><a href=\"/category/list/1/tags/220\"class=\"\">总裁</a><a href=\"/category/list/1/tags/217\"class=\"\">灵异</a><a href=\"/category/list/1/tags/222\"class=\"\">精品</a><a href=\"/category/list/1/tags/223\"class=\"\">战斗</a><a href=\"/category/list/1/tags/224\"class=\"\">漫改</a><a href=\"/category/list/1/tags/225\"class=\"\">新作</a><a href=\"/category/list/1/tags/226\"class=\"\">神仙</a><a href=\"/category/list/1/tags/227\"class=\"\">改编</a><a href=\"/category/list/1/tags/228\"class=\"\">校园</a><a href=\"/category/list/1/tags/229\"class=\"\">治愈</a><a href=\"/category/list/1/tags/230\"class=\"\">霸总</a><a href=\"/category/list/1/tags/231\"class=\"\">爆更</a><a href=\"/category/list/1/tags/470\"class=\"\">科技n</a><a href=\"/category/list/1/tags/471\"class=\"\">格鬥</a><a href=\"/category/list/1/tags/472\"class=\"\">熱血</a><a href=\"/category/list/1/tags/473\"class=\"\">戀愛</a><a href=\"/category/list/1/tags/474\"class=\"\">少年漫画</a><a href=\"/category/list/1/tags/475\"class=\"\">少女漫画</a><a href=\"/category/list/1/tags/476\"class=\"\">青年漫画</a><a href=\"/category/list/1/tags/477\"class=\"\">儿童漫画</a><a href=\"/category/list/1/tags/499\"class=\"\">冒险热血</a><a href=\"/category/list/1/tags/500\"class=\"\">耽美爱情</a><a href=\"/category/list/1/tags/501\"class=\"\">悬疑</a><a href=\"/category/list/1/tags/502\"class=\"\">武侠</a><a href=\"/category/list/1/tags/504\"class=\"\">玄幻科幻</a><a href=\"/category/list/1/tags/505\"class=\"\">生活</a><a href=\"/category/list/1/tags/506\"class=\"\">其他</a><a href=\"/category/list/1/tags/507\"class=\"\">武侠格斗</a><a href=\"/category/list/1/tags/508\"class=\"\">侦探推理</a><a href=\"/category/list/1/tags/509\"class=\"\">生活漫画</a><a href=\"/category/list/1/tags/510\"class=\"\">游戏竞技</a><a href=\"/category/list/1/tags/511\"class=\"\">惊悚</a><a href=\"/category/list/1/tags/512\"class=\"\">现代</a><a href=\"/category/list/1/tags/513\"class=\"\">西幻</a><a href=\"/category/list/1/tags/514\"class=\"\">純愛</a><a href=\"/category/list/1/tags/515\"class=\"\">後宮</a><a href=\"/category/list/1/tags/516\"class=\"\">劇情</a><a href=\"/category/list/1/tags/517\"class=\"\">喜剧</a><a href=\"/category/list/1/tags/518\"class=\"\">日常</a><a href=\"/category/list/1/tags/519\"class=\"\">亲情</a><a href=\"/category/list/1/tags/520\"class=\"\">养成</a><a href=\"/category/list/1/tags/521\"class=\"\">欢喜</a><a href=\"/category/list/1/tags/522\"class=\"\">北欧</a><a href=\"/category/list/1/tags/523\"class=\"\">彩虹</a><a href=\"/category/list/1/tags/524\"class=\"\">腹黑</a><a href=\"/category/list/1/tags/525\"class=\"\">古装</a><a href=\"/category/list/1/tags/526\"class=\"\">美少女</a><a href=\"/category/list/1/tags/528\"class=\"\">韩国</a><a href=\"/category/list/1/tags/529\"class=\"\">咚漫</a><a href=\"/category/list/1/tags/531\"class=\"\">原创</a><a href=\"/category/list/1/tags/532\"class=\"\">异形</a><a href=\"/category/list/1/tags/533\"class=\"\">偶像</a><a href=\"/category/list/1/tags/534\"class=\"\">歌舞</a><a href=\"/category/list/1/tags/536\"class=\"\">宅斗</a><a href=\"/category/list/1/tags/538\"class=\"\">宅向</a><a href=\"/category/list/1/tags/539\"class=\"\">青春</a><a href=\"/category/list/1/tags/540\"class=\"\">西幻</a><a href=\"/category/list/1/tags/541\"class=\"\">冒险</a><a href=\"/category/list/1/tags/542\"class=\"\">恋爱</a><a href=\"/category/list/1/tags/543\"class=\"\">都市</a><a href=\"/category/list/1/tags/544\"class=\"\">其它</a><a href=\"/category/list/1/tags/545\"class=\"\">战斗</a><a href=\"/category/list/1/tags/546\"class=\"\">其他</a><a href=\"/category/list/1/tags/547\"class=\"\">灵异</a><a href=\"/category/list/1/tags/548\"class=\"\">科幻</a><a href=\"/category/list/1/tags/549\"class=\"\">纯爱</a><a href=\"/category/list/1/tags/550\"class=\"\">现代</a><a href=\"/category/list/1/tags/551\"class=\"\">总裁</a><a href=\"/category/list/1/tags/552\"class=\"\">推理</a><a href=\"/category/list/1/tags/553\"class=\"\">职场</a><a href=\"/category/list/1/tags/554\"class=\"\">剧情</a><a href=\"/category/list/1/tags/555\"class=\"\">校园</a><a href=\"/category/list/1/tags/556\"class=\"\">穿越</a><a href=\"/category/list/1/tags/557\"class=\"\">逆袭</a><a href=\"/category/list/1/tags/558\"class=\"\">古风</a><a href=\"/category/list/1/tags/559\"class=\"\">玄幻</a><a href=\"/category/list/1/tags/560\"class=\"\">热血</a><a href=\"/category/list/1/tags/561\"class=\"\">权谋</a><a href=\"/category/list/1/tags/562\"class=\"\">正能量</a><a href=\"/category/list/1/tags/563\"class=\"\">复仇</a><a href=\"/category/list/1/tags/564\"class=\"\">悬疑</a><a href=\"/category/list/1/tags/565\"class=\"\">奇幻</a><a href=\"/category/list/1/tags/566\"class=\"\">搞笑</a><a href=\"/category/list/1/tags/567\"class=\"\">日常</a><a href=\"/category/list/1/tags/568\"class=\"\">大女主</a><a href=\"/category/list/1/tags/569\"class=\"\">亲情</a><a href=\"/category/list/1/tags/570\"class=\"\">战争</a><a href=\"/category/list/1/tags/571\"class=\"\">脑洞</a><a href=\"/category/list/1/tags/572\"class=\"\">社会</a><a href=\"/category/list/1/tags/573\"class=\"\">重生</a><a href=\"/category/list/1/tags/574\"class=\"\">怪物</a><a href=\"/category/list/1/tags/575\"class=\"\">女神</a><a href=\"/category/list/1/tags/576\"class=\"\">多世界</a><a href=\"/category/list/1/tags/577\"class=\"\">异能</a><a href=\"/category/list/1/tags/578\"class=\"\">治愈</a><a href=\"/category/list/1/tags/579\"class=\"\">浪漫</a><a href=\"/category/list/1/tags/580\"class=\"\">魔幻</a><a href=\"/category/list/1/tags/581\"class=\"\">惊悚</a><a href=\"/category/list/1/tags/582\"class=\"\">妖怪</a><a href=\"/category/list/1/tags/583\"class=\"\">励志</a><a href=\"/category/list/1/tags/584\"class=\"\">美食</a><a href=\"/category/list/1/tags/585\"class=\"\">暗黑</a><a href=\"/category/list/1/tags/586\"class=\"\">系统</a><a href=\"/category/list/1/tags/587\"class=\"\">恶搞</a><a href=\"/category/list/1/tags/588\"class=\"\">机甲</a><a href=\"/category/list/1/tags/589\"class=\"\">猎奇</a><a href=\"/category/list/1/tags/590\"class=\"\">武侠</a><a href=\"/category/list/1/tags/591\"class=\"\">格斗</a><a href=\"/category/list/1/tags/592\"class=\"\">虐心</a><a href=\"/category/list/1/tags/593\"class=\"\">精品</a><a href=\"/category/list/1/tags/594\"class=\"\">娱乐圈</a><a href=\"/category/list/1/tags/595\"class=\"\">生活</a><a href=\"/category/list/1/tags/596\"class=\"\">高甜</a><a href=\"/category/list/1/tags/597\"class=\"\">神仙</a><a href=\"/category/list/1/tags/598\"class=\"\">修仙</a><a href=\"/category/list/1/tags/599\"class=\"\">新作</a><a href=\"/category/list/1/tags/600\"class=\"\">末日</a><a href=\"/category/list/1/tags/601\"class=\"\">宫斗</a><a href=\"/category/list/1/tags/602\"class=\"\">后宫</a><a href=\"/category/list/1/tags/604\"class=\"\">豪快</a><a href=\"/category/list/1/tags/605\"class=\"\">神魔</a><a href=\"/category/list/1/tags/606\"class=\"\">萌系</a><a href=\"/category/list/1/tags/608\"class=\"\">图谱</a><a href=\"/category/list/1/tags/609\"class=\"\">历史</a><a href=\"/category/list/1/tags/610\"class=\"\">霸总</a><a href=\"/category/list/1/tags/611\"class=\"\">电竞</a><a href=\"/category/list/1/tags/612\"class=\"\">游戏</a><a href=\"/category/list/1/tags/613\"class=\"\">虚拟世界</a><a href=\"/category/list/1/tags/614\"class=\"\">少年</a><a href=\"/category/list/1/tags/615\"class=\"\">烧脑</a><a href=\"/category/list/1/tags/616\"class=\"\">体育</a><a href=\"/category/list/1/tags/617\"class=\"\">修真</a><a href=\"/category/list/1/tags/618\"class=\"\">动作</a><a href=\"/category/list/1/tags/619\"class=\"\">性转</a><a href=\"/category/list/1/tags/620\"class=\"\">吸血</a><a href=\"/category/list/1/tags/621\"class=\"\">宠物</a><a href=\"/category/list/1/tags/622\"class=\"\">神豪</a><a href=\"/category/list/1/tags/623\"class=\"\">漫改</a><a href=\"/category/list/1/tags/624\"class=\"\">竞技</a><a href=\"/category/list/1/tags/625\"class=\"\">御姐</a><a href=\"/category/list/1/tags/626\"class=\"\">日更</a><a href=\"/category/list/1/tags/627\"class=\"\">运动</a><a href=\"/category/list/1/tags/628\"class=\"\">丧尸</a><a href=\"/category/list/1/tags/629\"class=\"\">家庭</a><a href=\"/category/list/1/tags/630\"class=\"\">架空</a><a href=\"/category/list/1/tags/631\"class=\"\">萝莉</a><a href=\"/category/list/1/tags/633\"class=\"\">江湖</a><a href=\"/category/list/1/tags/634\"class=\"\">真人</a><a href=\"/category/list/1/tags/635\"class=\"\">异世界</a><a href=\"/category/list/1/tags/637\"class=\"\">防疫</a><a href=\"/category/list/1/tags/638\"class=\"\">氪金</a><a href=\"/category/list/1/tags/639\"class=\"\">唯美</a><a href=\"/category/list/1/tags/641\"class=\"\">名著</a><a href=\"/category/list/1/tags/642\"class=\"\">撒糖</a><a href=\"/category/list/1/tags/644\"class=\"\">快穿</a><a href=\"/category/list/1/tags/645\"class=\"\">国漫</a><a href=\"/category/list/1/tags/647\"class=\"\">机战</a><a href=\"/category/list/1/tags/648\"class=\"\">反套路</a><a href=\"/category/list/1/tags/651\"class=\"\">诡异</a><a href=\"/category/list/1/tags/652\"class=\"\">府邸</a><a href=\"/category/list/1/tags/655\"class=\"\">金手指</a><a href=\"/category/list/1/tags/656\"class=\"\">现言</a><a href=\"/category/list/1/tags/658\"class=\"\">恐怖</a><a href=\"/category/list/1/tags/662\"class=\"\">萌娃</a><a href=\"/category/list/1/tags/663\"class=\"\">穿书</a><a href=\"/category/list/1/tags/665\"class=\"\">强强</a><a href=\"/category/list/1/tags/666\"class=\"\">宠兽</a><a href=\"/category/list/1/tags/667\"class=\"\">言情</a><a href=\"/category/list/1/tags/669\"class=\"\">古言</a><a href=\"/category/list/1/tags/671\"class=\"\">迪化</a><a href=\"/category/list/1/tags/672\"class=\"\">无节操</a><a href=\"/category/list/1/tags/677\"class=\"\">种田</a><a href=\"/category/list/1/tags/678\"class=\"\">无敌流</a><a href=\"/category/list/1/tags/679\"class=\"\">仙侠</a><a href=\"/category/list/1/tags/681\"class=\"\">少女</a><a href=\"/category/list/1/tags/683\"class=\"\">百合</a><a href=\"/category/list/1/tags/684\"class=\"\">爆笑</a><a href=\"/category/list/1/tags/685\"class=\"\">克苏鲁</a><a href=\"/category/list/1/tags/686\"class=\"\">双男主</a><a href=\"/category/list/1/tags/690\"class=\"\">畅销</a><a href=\"/category/list/1/tags/691\"class=\"\">直播</a><a href=\"/category/list/1/tags/693\"class=\"\">古装</a><a href=\"/category/list/1/tags/698\"class=\"\">团宠</a><a href=\"/category/list/1/tags/699\"class=\"\">非人类</a><a href=\"/category/list/1/tags/701\"class=\"\">同人</a><a href=\"/category/list/1/tags/702\"class=\"\">神鬼</a><a href=\"/category/list/1/tags/703\"class=\"\">魔法</a><a href=\"/category/list/1/tags/704\"class=\"\">爱情</a><a href=\"/category/list/1/tags/705\"class=\"\">知音漫客</a><a href=\"/category/list/1/tags/706\"class=\"\">青年</a><a href=\"/category/list/1/tags/708\"class=\"\">强剧情</a><a href=\"/category/list/1/tags/710\"class=\"\">欧风</a><a href=\"/category/list/1/tags/711\"class=\"\">宫廷</a><a href=\"/category/list/1/tags/712\"class=\"\">兄弟情</a><a href=\"/category/list/1/tags/713\"class=\"\">绅士</a><a href=\"/category/list/1/tags/714\"class=\"\">日本</a><a href=\"/category/list/1/tags/715\"class=\"\">港台</a><a href=\"/category/list/1/tags/716\"class=\"\">耽美</a><a href=\"/category/list/1/tags/719\"class=\"\">栏目</a><a href=\"/category/list/1/tags/720\"class=\"\">韩漫</a><a href=\"/category/list/1/tags/722\"class=\"\">幻想</a><a href=\"/category/list/1/tags/723\"class=\"\">懸疑</a></div></div><div class=\"all-type-box fn-clear\"><span>进度：</span><div class=\"all-box\"><a href=\"/category/list/1/finish/1\"class=\"\">连载中</a><a href=\"/category/list/1/finish/2\"class=\"\">已完结</a></div></div></div>";
            }

            @Override
            protected String getCssQuery() {
                return "a";
            }

            @Override
            protected String getTitle(ElementNode node) {
                return node.ownText("a");
            }

            @Override
            protected String getUrl(ElementNode node) {
                return getIndex() + node.href("a") + "/page/%d";
            }
        };
    }
}
