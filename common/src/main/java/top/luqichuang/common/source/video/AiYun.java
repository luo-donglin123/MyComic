package top.luqichuang.common.source.video;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Request;
import top.luqichuang.common.en.VSourceEnum;
import top.luqichuang.common.jsoup.JsoupNode;
import top.luqichuang.common.jsoup.JsoupStarter;
import top.luqichuang.common.model.ChapterInfo;
import top.luqichuang.common.model.Content;
import top.luqichuang.common.model.EntityInfo;
import top.luqichuang.common.model.builder.EntityInfoBuilder;
import top.luqichuang.common.model.video.BaseVideoSource;
import top.luqichuang.common.util.NetUtil;
import top.luqichuang.common.util.SourceHelper;
import top.luqichuang.common.util.StringUtil;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/7/1 15:53
 * @ver 1.0
 */
public class AiYun extends BaseVideoSource {
    @Override
    public VSourceEnum getVSourceEnum() {
        return VSourceEnum.AI_YUN;
    }

    @Override
    public String getIndex() {
        return "https://www.iyunys.com";
    }

    @Override
    public Request getSearchRequest(String searchString) {
        String url = String.format("%s/search?searchString=%s", getIndex(), searchString);
        return NetUtil.getRequest(url);
    }

    @Override
    public List<EntityInfo> getInfoList(String html) {
        JsoupStarter<EntityInfo> starter = new JsoupStarter<EntityInfo>() {
            @Override
            protected EntityInfo dealElement(JsoupNode node) {
                return new EntityInfoBuilder(getInfoClass())
                        .buildSourceId(getSourceId())
                        .buildTitle(node.ownText("h1 a"))
                        .buildAuthor(node.ownText("li", 1))
                        .buildUpdateTime(node.ownText("li", 6))
                        .buildUpdateChapter(null)
                        .buildImgUrl(node.attr("a", "data-original"))
                        .buildDetailUrl(getIndex() + node.href("a"))
                        .build();
            }
        };
        return starter.startElements(html, "dl.fed-deta-info");
    }

    @Override
    public void setInfoDetail(EntityInfo info, String html, Map<String, Object> map) {
        JsoupStarter<ChapterInfo> starter = new JsoupStarter<ChapterInfo>() {
            @Override
            protected void dealInfo(JsoupNode node) {
                new EntityInfoBuilder(info)
                        .buildSourceId(getSourceId())
                        .buildTitle(node.ownText("h1.fed-part-eone.fed-font-xvi"))
                        .buildAuthor(node.ownText("div.fed-part-layout li.fed-col-md6", 1, "a"))
                        .buildIntro(node.ownText("p.fed-padding.fed-part-both.fed-text-muted"))
                        .buildUpdateTime(node.ownText("div.fed-part-layout li.fed-col-md6", 4, "a"))
                        .buildUpdateStatus(node.ownText("div.fed-part-layout li.fed-col-md6", 0, "a"))
                        .buildImgUrl(node.attr("a.fed-list-pics.fed-lazy.fed-part-2by3", "data-original"))
                        .build();
            }

            @Override
            protected ChapterInfo dealElement(JsoupNode node) {
                String title = node.title("a");
                String chapterUrl = getIndex() + "/ckplayer" + node.href("a");
                return new ChapterInfo(title, chapterUrl);
            }
        };
        starter.startInfo(html);
        SourceHelper.initChapterInfoList(info, starter.startElements(html, "ul.fed-part-rows li.fed-padding.fed-col-xs4.fed-col-md3.fed-col-lg2"));
        SourceHelper.initChapterInfoMap(info, html, "ul li.fed-drop-btns.fed-padding.fed-col-xs3.fed-col-md2", "a", "div.all_data_list", "li");
    }

    @Override
    public List<Content> getContentList(String html, int chapterId, Map<String, Object> map) {
        String url = StringUtil.match("\"urls\": \"(.*?)\",", html);
        Content content = new Content(chapterId);
        content.setUrl(url);
        return SourceHelper.getContentList(content);
    }

    @Override
    public Map<String, String> getRankMap() {
        String html = "<div class=\"fed-casc-list fed-part-rows\">\t<dl>\t\t<dt>类别</dt>\t\t<dd><a class=\"fed-this fed-text-green\" href=\"/show\">全部</a></dd>\t\t<dd><a href=\"/show?bigCategoryId=1\">动漫</a></dd>\t\t<dd><a href=\"/show?bigCategoryId=2\">电影</a></dd>\t\t<dd><a href=\"/show?bigCategoryId=3\">电视剧</a></dd>\t\t<dd><a href=\"/show?bigCategoryId=4\">综艺</a></dd>\t</dl>\t<dl>\t\t<dt>状态</dt>\t\t<dd><a href=\"/show?orderBy=weeklyCount&amp;status=1\">连载中</a></dd>\t\t<dd><a href=\"/show?orderBy=weeklyCount&amp;status=2\">已完结</a></dd>\t</dl>\t<dl>\t\t<dt>字母</dt>\t\t<dd><a href=\"/show?orderBy=weeklyCount&amp;charCategoryId=10053\">A</a></dd>\t\t<dd><a href=\"/show?orderBy=weeklyCount&amp;charCategoryId=10055\">B</a></dd>\t\t<dd><a href=\"/show?orderBy=weeklyCount&amp;charCategoryId=10054\">C</a></dd>\t\t<dd><a href=\"/show?orderBy=weeklyCount&amp;charCategoryId=10044\">D</a></dd>\t\t<dd><a href=\"/show?orderBy=weeklyCount&amp;charCategoryId=10063\">E</a></dd>\t\t<dd><a href=\"/show?orderBy=weeklyCount&amp;charCategoryId=10057\">F</a></dd>\t\t<dd><a href=\"/show?orderBy=weeklyCount&amp;charCategoryId=10060\">G</a></dd>\t\t<dd><a href=\"/show?orderBy=weeklyCount&amp;charCategoryId=10046\">H</a></dd>\t\t<dd><a href=\"/show?orderBy=weeklyCount&amp;charCategoryId=10066\">I</a></dd>\t\t<dd><a href=\"/show?orderBy=weeklyCount&amp;charCategoryId=10050\">J</a></dd>\t\t<dd><a href=\"/show?orderBy=weeklyCount&amp;charCategoryId=10056\">K</a></dd>\t\t<dd><a href=\"/show?orderBy=weeklyCount&amp;charCategoryId=10052\">L</a></dd>\t\t<dd><a href=\"/show?orderBy=weeklyCount&amp;charCategoryId=10058\">M</a></dd>\t\t<dd><a href=\"/show?orderBy=weeklyCount&amp;charCategoryId=10059\">N</a></dd>\t\t<dd><a href=\"/show?orderBy=weeklyCount&amp;charCategoryId=10064\">O</a></dd>\t\t<dd><a href=\"/show?orderBy=weeklyCount&amp;charCategoryId=10049\">P</a></dd>\t\t<dd><a href=\"/show?orderBy=weeklyCount&amp;charCategoryId=10062\">Q</a></dd>\t\t<dd><a href=\"/show?orderBy=weeklyCount&amp;charCategoryId=10065\">R</a></dd>\t\t<dd><a href=\"/show?orderBy=weeklyCount&amp;charCategoryId=10048\">S</a></dd>\t\t<dd><a href=\"/show?orderBy=weeklyCount&amp;charCategoryId=10061\">T</a></dd>\t\t<dd><a href=\"/show?orderBy=weeklyCount&amp;charCategoryId=10067\">U</a></dd>\t\t<dd><a href=\"/show?orderBy=weeklyCount&amp;charCategoryId=10068\">V</a></dd>\t\t<dd><a href=\"/show?orderBy=weeklyCount&amp;charCategoryId=10051\">W</a></dd>\t\t<dd><a href=\"/show?orderBy=weeklyCount&amp;charCategoryId=10043\">X</a></dd>\t\t<dd><a href=\"/show?orderBy=weeklyCount&amp;charCategoryId=10045\">Y</a></dd>\t\t<dd><a href=\"/show?orderBy=weeklyCount&amp;charCategoryId=10047\">Z</a></dd>\t</dl></div>";
        Map<String, String> map = new LinkedHashMap<>();
        JsoupNode node = new JsoupNode(html);
        Elements elements = node.getElements("a");
        for (Element element : elements) {
            node.init(element);
            map.put(node.ownText("a"), getIndex() + node.href("a"));
        }
        return map;
    }

    @Override
    public List<EntityInfo> getRankInfoList(String html) {
        JsoupStarter<EntityInfo> starter = new JsoupStarter<EntityInfo>() {
            @Override
            protected EntityInfo dealElement(JsoupNode node) {
                return new EntityInfoBuilder(getInfoClass())
                        .buildSourceId(getSourceId())
                        .buildTitle(node.ownText("a.fed-list-title"))
                        .buildAuthor(null)
                        .buildUpdateTime(node.ownText("span.fed-list-desc"))
                        .buildUpdateChapter(null)
                        .buildImgUrl(node.attr("a", "data-original"))
                        .buildDetailUrl(getIndex() + node.href("a"))
                        .build();
            }
        };
        return starter.startElements(html, "li.fed-list-item");
    }
}
