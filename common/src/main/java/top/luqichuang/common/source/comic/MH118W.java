package top.luqichuang.common.source.comic;

import java.util.List;
import java.util.Map;

import okhttp3.Request;
import top.luqichuang.common.en.CSourceEnum;
import top.luqichuang.common.jsoup.JsoupNode;
import top.luqichuang.common.jsoup.JsoupStarter;
import top.luqichuang.common.model.ChapterInfo;
import top.luqichuang.common.model.Content;
import top.luqichuang.common.model.EntityInfo;
import top.luqichuang.common.model.comic.BaseComicSource;
import top.luqichuang.common.model.comic.ComicInfo;
import top.luqichuang.common.util.DecryptUtil;
import top.luqichuang.common.util.NetUtil;
import top.luqichuang.common.util.SourceHelper;
import top.luqichuang.common.util.StringUtil;

/**
 * @author LuQiChuang
 * @desc
 * @date 2020/11/26 20:17
 * @ver 1.0
 */
@Deprecated
public class MH118W extends BaseComicSource {
    @Override
    public CSourceEnum getCSourceEnum() {
        return CSourceEnum.MH_118_2;
    }

    @Override
    public String getIndex() {
        return "http://m.xzcstjx.com";
    }

    @Override
    public boolean isValid() {
        return false;
    }

    @Override
    public Request getSearchRequest(String searchString) {
        String url = "http://www.xzcstjx.com/statics/search.aspx?key=" + searchString;
        return NetUtil.getRequest(url);
    }

    @Override
    public List<EntityInfo> getInfoList(String html) {
        JsoupStarter<EntityInfo> starter = new JsoupStarter<EntityInfo>() {
            @Override
            protected EntityInfo dealElement(JsoupNode node) {
                String title = node.ownText("li.title a");
                String author = null;
                String updateTime = null;
                String updateChapter = null;
                String imgUrl = node.src("img");
                String detailUrl = getIndex() + node.href("li.title a");
                return new ComicInfo(getSourceId(), title, author, detailUrl, imgUrl, updateTime, updateChapter);
            }
        };
        return starter.startElements(html, "div.cy_list_mh ul");
    }

    @Override
    public void setInfoDetail(EntityInfo info, String html, Map<String, Object> map) {
        JsoupStarter<ChapterInfo> starter = new JsoupStarter<ChapterInfo>() {
            @Override
            protected void dealInfo(JsoupNode node) {
                String title = node.ownText("div.title h1");
                String imgUrl = node.src("div#Cover img");
                String author = node.ownText("p.txtItme:eq(1)");
                String intro = node.ownText("p.txtDesc");
                String updateStatus = node.ownText("p.txtItme:eq(2) :eq(3)");
                String updateTime = node.ownText("p.txtItme span.date");
                try {
                    intro = intro.substring(intro.indexOf(':') + 1);
                } catch (Exception ignored) {
                }
                info.setDetail(title, imgUrl, author, updateTime, updateStatus, intro);
            }

            @Override
            protected ChapterInfo dealElement(JsoupNode node) {
                String title = node.ownText("span");
                String chapterUrl = "http://m.xzcstjx.com" + node.href("a");
                if (title == null) {
                    return null;
                }
                return new ChapterInfo(title, chapterUrl);
            }
        };
        starter.startInfo(html);
        SourceHelper.initChapterInfoList(info, starter.startElements(html, "ul#mh-chapter-list-ol-0 li"));
    }

    @Override
    public List<Content> getContentList(String html, int chapterId, Map<String, Object> map) {
        String[] urls = null;
        try {
            String chapterStr = StringUtil.match("qTcms_S_m_murl_e=\"(.*?)\"", html);
            String httpUrlStr = StringUtil.match("qTcms_S_m_mhttpurl=\"(.*?)\"", html);
            String picId = StringUtil.match("qTcms_S_m_id=\"(.*?)\"", html);
            chapterStr = DecryptUtil.decryptBase64(chapterStr);
            httpUrlStr = DecryptUtil.decryptBase64(httpUrlStr);
            urls = chapterStr.split("\\$qingtiandy\\$");
            String baseUrl = "http://res.xzcstjx.com/statics/pic/?p=%s&picid=%s&m_httpurl=%s";
            for (int i = 0; i < urls.length; i++) {
                urls[i] = String.format(baseUrl, urls[i], picId, httpUrlStr);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return SourceHelper.getContentList(urls, chapterId);
    }

    @Override
    public Map<String, String> getRankMap() {
        return null;
    }

    @Override
    public List<EntityInfo> getRankInfoList(String html) {
        return null;
    }

}