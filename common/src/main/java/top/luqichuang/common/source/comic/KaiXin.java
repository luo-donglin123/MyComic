package top.luqichuang.common.source.comic;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import okhttp3.Request;
import top.luqichuang.common.en.CSourceEnum;
import top.luqichuang.common.json.JsonNode;
import top.luqichuang.common.json.JsonStarter;
import top.luqichuang.common.jsoup.JsoupNode;
import top.luqichuang.common.jsoup.JsoupStarter;
import top.luqichuang.common.model.ChapterInfo;
import top.luqichuang.common.model.Content;
import top.luqichuang.common.model.EntityInfo;
import top.luqichuang.common.model.builder.EntityInfoBuilder;
import top.luqichuang.common.model.comic.BaseComicSource;
import top.luqichuang.common.util.NetUtil;
import top.luqichuang.common.util.SourceHelper;
import top.luqichuang.common.util.StringUtil;

/**
 * @author 18472
 * @desc
 * @date 2024/1/28 11:33
 * @ver 1.0
 */
public class KaiXin extends BaseComicSource {

    @Override
    public CSourceEnum getCSourceEnum() {
        return CSourceEnum.KAI_XIN;
    }

    @Override
    public String getIndex() {
        return "https://yydscomic.com";
    }

    @Override
    public Request getSearchRequest(String searchString) {
        String url = String.format("%s/api/front/index/search", getIndex());
        return NetUtil.postRequest(url, "key", searchString);
    }

    @Override
    public Request buildRequest(String html, String tag, Map<String, Object> data, Map<String, Object> map) {
        if (CONTENT.equals(tag)) {
            if (map.get("html") == null) {
                map.put("html", html);
            } else {
                map.put("html", map.get("html") + html);
            }
            JsoupNode node = new JsoupNode(html);
            Elements elements = node.getElements("div.chapter-page-nav>div");
            for (Element element : elements) {
                node.init(element);
                String text = node.text("div");
                if (Objects.equals(text, "下一页")) {
                    String url = getIndex() + node.href("a");
                    return NetUtil.getRequest(url);
                }
            }
        }
        return super.buildRequest(html, tag, data, map);
    }

    @Override
    public List<EntityInfo> getInfoList(String html) {
        JsonStarter<EntityInfo> starter = new JsonStarter<EntityInfo>() {
            @Override
            protected EntityInfo dealDataList(JsonNode node) {
                String title = node.string("name");
                if (title != null && title.endsWith("~")) {
                    title = title.substring(0, title.length() - 1);
                }
                return new EntityInfoBuilder(getInfoClass())
                        .buildSourceId(getSourceId())
                        .buildTitle(title)
                        .buildAuthor(node.string("author"))
                        .buildUpdateTime(node.string("lastupdate_a"))
                        .buildUpdateChapter(node.string("lastchapter"))
                        .buildImgUrl(node.string("bcover"))
                        .buildDetailUrl(getIndex() + node.string("info_url"))
                        .build();
            }
        };
        return starter.startDataList(html, "data");
    }

    @Override
    public void setInfoDetail(EntityInfo info, String html, Map<String, Object> map) {
        JsoupStarter<ChapterInfo> starter = new JsoupStarter<ChapterInfo>() {
            @Override
            protected boolean isDESC() {
                return false;
            }

            @Override
            protected void dealInfo(JsoupNode node) {
                String title = node.ownText("h1.title");
                if (title != null && title.endsWith("~")) {
                    title = title.substring(0, title.length() - 1);
                }
                String updateTime = node.ownText("div.hd span");
                updateTime = StringUtil.remove(updateTime, "更新至");
                new EntityInfoBuilder(info)
                        .buildSourceId(getSourceId())
                        .buildTitle(title)
                        .buildAuthor(node.ownText("div.auth p.name"))
                        .buildIntro(node.ownText("p#js_comciDesc span.desc-content"))
                        .buildUpdateTime(updateTime)
                        .buildUpdateStatus(node.ownText("p.sort span:eq(1) em"))
                        .buildImgUrl(node.attr("meta[property=\"og:image\"]", "content"))
                        .buildChapterInfoList(startElements(html, "div.bd li"))
                        .build();
            }

            @Override
            protected ChapterInfo dealElement(JsoupNode node) {
                String title = node.title("a");
                String chapterUrl = getIndex() + node.href("a");
                return new ChapterInfo(title, chapterUrl);
            }
        };
        starter.startInfo(html);
    }

    @Override
    public List<Content> getContentList(String html, int chapterId, Map<String, Object> map) {
        List<String> urlList = new ArrayList<>();
        JsoupNode node = new JsoupNode((String) map.get("html"));
        Elements elements = node.getElements("div#img-box>div");
        for (Element element : elements) {
            node.init(element);
            urlList.add(node.src("img"));
        }
        return SourceHelper.getContentList(urlList, chapterId);
    }

    @Override
    public Map<String, String> getRankMap() {
        String html = "<div class=\"acgn-bd\"><div class=\"acgn-sort-select\"></div><dl class=\"acgn-sort-filter J_filter\"><dt class=\"acgn-hd\">题材</dt><dd class=\"acgn-bd\"><a href=\"/manga-lists/9/全部/3/1.html\"class=\"acgn-sort-attr acgn-active\"title=\"全部\">全部</a><a href=\"/manga-lists/9/长条/3/1.html\"class=\"acgn-sort-attr \"title=\"长条\">长条</a><a href=\"/manga-lists/9/大女主/3/1.html\"class=\"acgn-sort-attr \"title=\"大女主\">大女主</a><a href=\"/manga-lists/9/百合/3/1.html\"class=\"acgn-sort-attr \"title=\"百合\">百合</a><a href=\"/manga-lists/9/耽美/3/1.html\"class=\"acgn-sort-attr \"title=\"耽美\">耽美</a><a href=\"/manga-lists/9/纯爱/3/1.html\"class=\"acgn-sort-attr \"title=\"纯爱\">纯爱</a><a href=\"/manga-lists/9/後宫/3/1.html\"class=\"acgn-sort-attr \"title=\"後宫\">後宫</a><a href=\"/manga-lists/9/韩漫/3/1.html\"class=\"acgn-sort-attr \"title=\"韩漫\">韩漫</a><a href=\"/manga-lists/9/奇幻/3/1.html\"class=\"acgn-sort-attr \"title=\"奇幻\">奇幻</a><a href=\"/manga-lists/9/轻小说/3/1.html\"class=\"acgn-sort-attr \"title=\"轻小说\">轻小说</a><a href=\"/manga-lists/9/生活/3/1.html\"class=\"acgn-sort-attr \"title=\"生活\">生活</a><a href=\"/manga-lists/9/悬疑/3/1.html\"class=\"acgn-sort-attr \"title=\"悬疑\">悬疑</a><a href=\"/manga-lists/9/格斗/3/1.html\"class=\"acgn-sort-attr \"title=\"格斗\">格斗</a><a href=\"/manga-lists/9/搞笑/3/1.html\"class=\"acgn-sort-attr \"title=\"搞笑\">搞笑</a><a href=\"/manga-lists/9/伪娘/3/1.html\"class=\"acgn-sort-attr \"title=\"伪娘\">伪娘</a><a href=\"/manga-lists/9/竞技/3/1.html\"class=\"acgn-sort-attr \"title=\"竞技\">竞技</a><a href=\"/manga-lists/9/职场/3/1.html\"class=\"acgn-sort-attr \"title=\"职场\">职场</a><a href=\"/manga-lists/9/萌系/3/1.html\"class=\"acgn-sort-attr \"title=\"萌系\">萌系</a><a href=\"/manga-lists/9/冒险/3/1.html\"class=\"acgn-sort-attr \"title=\"冒险\">冒险</a><a href=\"/manga-lists/9/治愈/3/1.html\"class=\"acgn-sort-attr \"title=\"治愈\">治愈</a><a href=\"/manga-lists/9/都市/3/1.html\"class=\"acgn-sort-attr \"title=\"都市\">都市</a><a href=\"/manga-lists/9/霸总/3/1.html\"class=\"acgn-sort-attr \"title=\"霸总\">霸总</a><a href=\"/manga-lists/9/神鬼/3/1.html\"class=\"acgn-sort-attr \"title=\"神鬼\">神鬼</a><a href=\"/manga-lists/9/侦探/3/1.html\"class=\"acgn-sort-attr \"title=\"侦探\">侦探</a><a href=\"/manga-lists/9/爱情/3/1.html\"class=\"acgn-sort-attr \"title=\"爱情\">爱情</a><a href=\"/manga-lists/9/古风/3/1.html\"class=\"acgn-sort-attr \"title=\"古风\">古风</a><a href=\"/manga-lists/9/欢乐向/3/1.html\"class=\"acgn-sort-attr \"title=\"欢乐向\">欢乐向</a><a href=\"/manga-lists/9/科幻/3/1.html\"class=\"acgn-sort-attr \"title=\"科幻\">科幻</a><a href=\"/manga-lists/9/穿越/3/1.html\"class=\"acgn-sort-attr \"title=\"穿越\">穿越</a><a href=\"/manga-lists/9/性转换/3/1.html\"class=\"acgn-sort-attr \"title=\"性转换\">性转换</a><a href=\"/manga-lists/9/校园/3/1.html\"class=\"acgn-sort-attr \"title=\"校园\">校园</a><a href=\"/manga-lists/9/美食/3/1.html\"class=\"acgn-sort-attr \"title=\"美食\">美食</a><a href=\"/manga-lists/9/悬疑/3/1.html\"class=\"acgn-sort-attr \"title=\"悬疑\">悬疑</a><a href=\"/manga-lists/9/剧情/3/1.html\"class=\"acgn-sort-attr \"title=\"剧情\">剧情</a><a href=\"/manga-lists/9/热血/3/1.html\"class=\"acgn-sort-attr \"title=\"热血\">热血</a><a href=\"/manga-lists/9/节操/3/1.html\"class=\"acgn-sort-attr \"title=\"节操\">节操</a><a href=\"/manga-lists/9/励志/3/1.html\"class=\"acgn-sort-attr \"title=\"励志\">励志</a><a href=\"/manga-lists/9/异世界/3/1.html\"class=\"acgn-sort-attr \"title=\"异世界\">异世界</a><a href=\"/manga-lists/9/历史/3/1.html\"class=\"acgn-sort-attr \"title=\"历史\">历史</a><a href=\"/manga-lists/9/战争/3/1.html\"class=\"acgn-sort-attr \"title=\"战争\">战争</a><a href=\"/manga-lists/9/恐怖/3/1.html\"class=\"acgn-sort-attr \"title=\"恐怖\">恐怖</a><a href=\"/manga-lists/9/霸总/3/1.html\"class=\"acgn-sort-attr \"title=\"霸总\">霸总</a></dd></dl><dl class=\"acgn-sort-filter J_filter\"><dt class=\"acgn-hd\">地区</dt><dd class=\"acgn-bd\"><a href=\"/manga-lists/1/全部/3/1.html\"class=\"acgn-sort-attr \"title=\"日漫\">日漫</a><a href=\"/manga-lists/2/全部/3/1.html\"class=\"acgn-sort-attr \"title=\"港台\">港台</a><a href=\"/manga-lists/3/全部/3/1.html\"class=\"acgn-sort-attr \"title=\"美漫\">美漫</a><a href=\"/manga-lists/4/全部/3/1.html\"class=\"acgn-sort-attr \"title=\"国漫\">国漫</a><a href=\"/manga-lists/5/全部/3/1.html\"class=\"acgn-sort-attr \"title=\"韩漫\">韩漫</a><a href=\"/manga-lists/6/全部/3/1.html\"class=\"acgn-sort-attr \"title=\"未分类\">未分类</a></dd><dt class=\"acgn-hd\">进度</dt><dd class=\"acgn-bd\"><a href=\"/manga-lists/9/全部/4/1.html\"class=\"acgn-sort-attr \"title=\"连载中\">连载中</a><a href=\"/manga-lists/9/全部/1/1.html\"class=\"acgn-sort-attr \"title=\"已完结\">已完结</a></dd></dl></div>";
        Map<String, String> map = new LinkedHashMap<>();
        JsoupNode node = new JsoupNode(html);
        Elements elements = node.getElements("a");
        for (Element element : elements) {
            node.init(element);
            String url = getIndex() + node.href("a");
            url = StringUtil.replace(url, "1.html", "%d.html");
            map.put(node.ownText("a"), url);
        }
        return map;
    }

    @Override
    public List<EntityInfo> getRankInfoList(String html) {
        JsoupStarter<EntityInfo> starter = new JsoupStarter<EntityInfo>() {
            @Override
            protected EntityInfo dealElement(JsoupNode node) {
                String title = node.ownText("h3.acgn-title a");
                if (title != null && title.endsWith("~")) {
                    title = title.substring(0, title.length() - 1);
                }
                return new EntityInfoBuilder(getInfoClass())
                        .buildSourceId(getSourceId())
                        .buildTitle(title)
                        .buildAuthor(null)
                        .buildUpdateTime(null)
                        .buildUpdateChapter(node.ownText("span.acgn-chapter"))
                        .buildImgUrl(node.src("img"))
                        .buildDetailUrl(getIndex() + node.href("a"))
                        .build();
            }
        };
        return starter.startElements(html, "ul#J_comicList>li");
    }
}
