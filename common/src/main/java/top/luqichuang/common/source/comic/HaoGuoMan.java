package top.luqichuang.common.source.comic;

import java.util.List;
import java.util.Map;

import okhttp3.Request;
import top.luqichuang.common.en.CSourceEnum;
import top.luqichuang.common.model.ChapterInfo;
import top.luqichuang.common.model.EntityInfo;
import top.luqichuang.common.model.builder.ChapterInfoBuilder;
import top.luqichuang.common.model.builder.EntityInfoBuilder;
import top.luqichuang.common.model.comic.BaseComicSource;
import top.luqichuang.common.starter.ElementNode;
import top.luqichuang.common.util.DecryptUtil;
import top.luqichuang.common.util.NetUtil;
import top.luqichuang.common.util.StringUtil;

/**
 * @author 18472
 * @desc
 * @date 2024/3/30 18:34
 * @ver 1.0
 */
public class HaoGuoMan extends BaseComicSource {
    @Override
    public CSourceEnum getCSourceEnum() {
        return CSourceEnum.HAO_GUO_MAN;
    }

    @Override
    public String getIndex() {
        return "https://www.haoguoman.com";
    }

    @Override
    public Request getSearchRequest(String searchString) {
        String url = String.format("%s/search?q=%s", getIndex(), searchString);
        return NetUtil.getRequest(url);
    }

    protected InfoListConfig getInfoListConfig(ElementNode node) {
        return new InfoListConfig() {
            @Override
            protected void addListConfig(List<ListConfig<EntityInfo>> configList) {
                configList.add(new ListConfig<EntityInfo>() {
                    @Override
                    protected String[] getQuery() {
                        return new String[]{"div.columns > div"};
                    }

                    @Override
                    protected EntityInfo getData(ElementNode node) {
                        return new EntityInfoBuilder(getInfoClass())
                                .buildSourceId(getSourceId())
                                .buildTitle(node.ownText("div.title a"))
                                .buildAuthor(null)
                                .buildUpdateTime(null)
                                .buildUpdateChapter(node.ownText("ul li:eq(1) span"))
                                .buildImgUrl(node.src("img"))
                                .buildDetailUrl(getIndex() + node.href("a"))
                                .build();
                    }
                });
            }
        };
    }

    protected InfoDetailConfig getInfoDetailConfig(ElementNode node) {
        return new InfoDetailConfig() {
            @Override
            protected void setData(EntityInfo info, ElementNode node) {
                String author = node.ownText("span.author");
                author = StringUtil.remove(author, "作者：");
                new EntityInfoBuilder(info)
                        .buildSourceId(getSourceId())
                        .buildTitle(node.ownText("h1.metas-title"))
                        .buildAuthor(author)
                        .buildIntro(node.ownText("div.metas-desc p"))
                        .buildUpdateTime(node.ownText("span.has-text-danger"))
                        .buildUpdateStatus(node.ownText("span.has-text-success"))
                        .buildImgUrl(node.src("div.metas-image img"))
                        .build();
            }

            @Override
            protected void addListConfig(List<ListConfig<ChapterInfo>> configList) {
                configList.add(new ListConfig<ChapterInfo>() {
                    @Override
                    protected boolean needSwap(List<ChapterInfo> list) {
                        return true;
                    }

                    @Override
                    protected String[] getQuery() {
                        return new String[]{"ul.chapter_list li"};
                    }

                    @Override
                    protected ChapterInfo getData(ElementNode node) {
                        return new ChapterInfoBuilder()
                                .buildTitle(node.ownText("a"))
                                .buildUrl(getIndex() + node.href("a"))
                                .build();
                    }
                });
            }
        };
    }

    protected ContentListConfig getContentListConfig(ElementNode node) {
        return new ContentListConfig() {
            @Override
            protected void addUrlList(ElementNode node, List<String> urlList, Map<String, Object> map) {
                String code = StringUtil.match("params = '(.*?)'", node.getHtml());
                code = StringUtil.remove(code, "YxUpqGrs5$3wYg");
                code = DecryptUtil.decryptBase64(code);
                code = StringUtil.match("\"chapter_images\":\"(.*?)\"", code);
                code = StringUtil.remove(code, "\\");
                String[] urls = StringUtil.split(code, "###");
                for (String url : urls) {
                    urlList.add("https://res.xiaoqinre.com/" + url);
                }
            }
        };
    }

    protected RankMapConfig getRankMapConfig() {
        return new RankMapConfig() {
            @Override
            protected String getHtml() {
                return "<div class=\"panel is-filter\"><div class=\"panel-block\"><div class=\"tabs-group\"><div class=\"name\">地区</div><ul><li class=\"\"><a href=\"/category/order/update\">全部</a></li><li class=\"active\"><a href=\"/category/region/guoman/order/update\">国漫</a></li><li class=\"\"><a href=\"/category/region/riman/order/update\">日漫</a></li><li class=\"\"><a href=\"/category/region/hanman/order/update\">韩漫</a></li><li class=\"\"><a href=\"/category/region/oumei/order/update\">欧美</a></li></ul></div></div><div class=\"panel-block\"><div class=\"tabs-group\"><div class=\"name\">题材</div><ul><li class=\"\"><a href=\"/category/region/guoman/theme/rexue/order/update\">热血</a></li><li class=\"\"><a href=\"/category/region/guoman/theme/gedou/order/update\">格斗</a></li><li class=\"\"><a href=\"/category/region/guoman/theme/maoxian/order/update\">冒险</a></li><li class=\"\"><a href=\"/category/region/guoman/theme/wuxia/order/update\">武侠</a></li><li class=\"\"><a href=\"/category/region/guoman/theme/xianxia/order/update\">仙侠</a></li><li class=\"\"><a href=\"/category/region/guoman/theme/kehuan/order/update\">科幻</a></li><li class=\"\"><a href=\"/category/region/guoman/theme/qihuan/order/update\">奇幻</a></li><li class=\"\"><a href=\"/category/region/guoman/theme/yineng/order/update\">异能</a></li><li class=\"\"><a href=\"/category/region/guoman/theme/chongsheng/order/update\">重生</a></li><li class=\"\"><a href=\"/category/region/guoman/theme/xuanyi/order/update\">悬疑</a></li><li class=\"\"><a href=\"/category/region/guoman/theme/tuili/order/update\">推理</a></li><li class=\"\"><a href=\"/category/region/guoman/theme/jingji/order/update\">竞技</a></li><li class=\"\"><a href=\"/category/region/guoman/theme/kongbu/order/update\">恐怖</a></li><li class=\"\"><a href=\"/category/region/guoman/theme/gaoxiao/order/update\">搞笑</a></li><li class=\"\"><a href=\"/category/region/guoman/theme/xiaoyuan/order/update\">校园</a></li><li class=\"\"><a href=\"/category/region/guoman/theme/dushi/order/update\">都市</a></li><li class=\"\"><a href=\"/category/region/guoman/theme/lianai/order/update\">恋爱</a></li><li class=\"\"><a href=\"/category/region/guoman/theme/baihe/order/update\">百合</a></li><li class=\"\"><a href=\"/category/region/guoman/theme/danmei/order/update\">耽美</a></li><li class=\"\"><a href=\"/category/region/guoman/theme/mengxi/order/update\">萌系</a></li><li class=\"\"><a href=\"/category/region/guoman/theme/weiniang/order/update\">伪娘</a></li><li class=\"\"><a href=\"/category/region/guoman/theme/lishi/order/update\">历史</a></li><li class=\"\"><a href=\"/category/region/guoman/theme/zhanzheng/order/update\">战争</a></li><li class=\"\"><a href=\"/category/region/guoman/theme/shenghuo/order/update\">生活</a></li></ul></div></div><div class=\"panel-block\"><div class=\"tabs-group\"><div class=\"name\">进度</div><ul><li class=\"\"><a href=\"/category/region/guoman/state/lianzai/order/update\">连载中</a></li><li class=\"\"><a href=\"/category/region/guoman/state/wanjie/order/update\">已完结</a></li></ul></div></div></div>";
            }

            @Override
            protected String getCssQuery() {
                return "a";
            }

            @Override
            protected String getTitle(ElementNode node) {
                return node.ownText("a");
            }

            @Override
            protected String getUrl(ElementNode node) {
                return getIndex() + node.href("a") + "/page/%d";
            }
        };
    }
}
