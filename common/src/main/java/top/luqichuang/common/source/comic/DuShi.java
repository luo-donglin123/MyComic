package top.luqichuang.common.source.comic;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Request;
import top.luqichuang.common.en.CSourceEnum;
import top.luqichuang.common.jsoup.JsoupNode;
import top.luqichuang.common.jsoup.JsoupStarter;
import top.luqichuang.common.model.ChapterInfo;
import top.luqichuang.common.model.Content;
import top.luqichuang.common.model.EntityInfo;
import top.luqichuang.common.model.comic.BaseComicSource;
import top.luqichuang.common.model.comic.ComicInfo;
import top.luqichuang.common.util.NetUtil;
import top.luqichuang.common.util.SourceHelper;
import top.luqichuang.common.util.StringUtil;

/**
 * @author LuQiChuang
 * @desc
 * @date 2022/10/16 16:45
 * @ver 1.0
 */
@Deprecated
public class DuShi extends BaseComicSource {
    @Override
    public CSourceEnum getCSourceEnum() {
        return CSourceEnum.DU_SHI;
    }

    @Override
    public String getIndex() {
        return "https://m.dushimh.com";
    }

    @Override
    public boolean isValid() {
        return false;
    }

    @Override
    public Request getSearchRequest(String searchString) {
        String url = String.format("%s/search/?keywords=%s", getIndex(), searchString);
        return NetUtil.getRequest(url);
    }

    @Override
    public List<EntityInfo> getInfoList(String html) {
        JsoupStarter<EntityInfo> starter = new JsoupStarter<EntityInfo>() {
            @Override
            protected EntityInfo dealElement(JsoupNode node) {
                String title = node.ownText("a.title");
                String author = node.ownText("p.txtItme");
                String updateTime = node.ownText("span.date");
                String updateChapter = null;
                String imgUrl = node.src("img");
                String detailUrl = node.href("a.title");
                return new ComicInfo(getSourceId(), title, author, detailUrl, imgUrl, updateTime, updateChapter);
            }
        };
        return starter.startElements(html, "div.itemBox");
    }

    @Override
    public void setInfoDetail(EntityInfo info, String html, Map<String, Object> map) {
        JsoupStarter<ChapterInfo> starter = new JsoupStarter<ChapterInfo>() {
            @Override
            protected boolean isDESC() {
                return false;
            }

            @Override
            protected void dealInfo(JsoupNode node) {
                String title = node.ownText("div#comicName");
                String imgUrl = node.src("div#Cover img");
                String author = node.ownText("p.txtItme");
                String intro = node.ownText("p#full-des");
                String updateStatus = null;
                String updateTime = node.ownText("span.date");
                info.setDetail(title, imgUrl, author, updateTime, updateStatus, intro);
            }

            @Override
            protected ChapterInfo dealElement(JsoupNode node) {
                String title = node.ownText("span");
                String chapterUrl = node.href("a");
                if (chapterUrl.contains("html")) {
                    if (!chapterUrl.startsWith("http")) {
                        chapterUrl = getIndex() + chapterUrl;
                    }
                    chapterUrl = chapterUrl.replace("//m.", "//www.");
                    return new ChapterInfo(title, chapterUrl);
                } else {
                    return null;
                }
            }
        };
        starter.startInfo(html);
        SourceHelper.initChapterInfoList(info, starter.startElements(html, "ul#chapter-list-10 li", "ul#chapter-list-1 li"));
    }

    @Override
    public List<Content> getContentList(String html, int chapterId, Map<String, Object> map) {
        String chapterImagesStr = StringUtil.match("chapterImages = \\[(.*?)\\]", html);
//        String chapterPath = StringUtil.match("var chapterPath = \"(.*?)\";", html);
        String[] urls = null;
        if (chapterImagesStr != null && !chapterImagesStr.equals("")) {
            urls = chapterImagesStr.split(",");
//            String server = "https://n2a.gjmrk.com/";
            for (int i = 0; i < urls.length; i++) {
                urls[i] = urls[i].replace("\"", "").replace("\\", "");
            }
        }
        return SourceHelper.getContentList(urls, chapterId);
    }

    @Override
    public Map<String, String> getRankMap() {
        String html = "<ul><li><a class=\"\"href=\"/list/juqing/\">剧情</a></li><li><a class=\"\"href=\"/list/rexue/\">热血</a></li><li><a class=\"\"href=\"/list/maoxian/\">冒险</a></li><li><a class=\"\"href=\"/list/lianai/\">恋爱</a></li><li><a class=\"\"href=\"/list/yundong/\">运动</a></li><li><a class=\"\"href=\"/list/qihuan/\">奇幻</a></li><li><a class=\"\"href=\"/list/xuanhuan/\">玄幻</a></li><li><a class=\"\"href=\"/list/dongzuo/\">动作</a></li><li><a class=\"\"href=\"/list/jiakong/\">架空</a></li><li><a class=\"\"href=\"/list/gufeng/\">古风</a></li><li><a class=\"\"href=\"/list/youxi/\">游戏</a></li><li><a class=\"\"href=\"/list/xiuzhen/\">修真</a></li><li><a class=\"\"href=\"/list/qita/\">其他</a></li><li><a class=\"\"href=\"/list/shaonan/\">少男</a></li><li><a class=\"\"href=\"/list/kehuan/\">科幻</a></li><li><a class=\"\"href=\"/list/jingji/\">竞技</a></li><li><a class=\"\"href=\"/list/lianzai/\">连载</a></li><li><a class=\"\"href=\"/list/chuanyue/\">穿越</a></li><li><a class=\"\"href=\"/list/hougong/\">后宫</a></li><li><a class=\"\"href=\"/list/dushi/\">都市</a></li><li><a class=\"\"href=\"/list/xiaoyuan/\">校园</a></li><li><a class=\"\"href=\"/list/lishi/\">历史</a></li><li><a class=\"\"href=\"/list/shaonv/\">少女</a></li><li><a class=\"\"href=\"/list/mankezhan/\">漫客栈</a></li><li><a class=\"\"href=\"/list/mangai/\">漫改</a></li><li><a class=\"\"href=\"/list/gaoxiao/\">搞笑</a></li><li><a class=\"\"href=\"/list/zhanzheng/\">战争</a></li><li><a class=\"\"href=\"/list/shenghuo/\">生活</a></li><li><a class=\"\"href=\"/list/samanhua/\">飒漫画</a></li><li><a class=\"\"href=\"/list/qingchun/\">青春</a></li><li><a class=\"\"href=\"/list/zhandou/\">战斗</a></li><li><a class=\"\"href=\"/list/mohuan/\">魔幻</a></li><li><a class=\"\"href=\"/list/chunai/\">纯爱</a></li><li><a class=\"\"href=\"/list/naodong/\">脑洞</a></li><li><a class=\"\"href=\"/list/xuanyi/\">悬疑</a></li><li><a class=\"\"href=\"/list/baoxiao/\">爆笑</a></li><li><a class=\"\"href=\"/list/danvzhu/\">大女主</a></li><li><a class=\"\"href=\"/list/tuili/\">推理</a></li><li><a class=\"\"href=\"/list/lingyi/\">灵异</a></li><li><a class=\"\"href=\"/list/egao/\">恶搞</a></li><li><a class=\"\"href=\"/list/qingsong/\">轻松</a></li><li><a class=\"\"href=\"/list/wangyou/\">网游</a></li><li><a class=\"\"href=\"/list/bazong/\">霸总</a></li><li><a class=\"\"href=\"/list/gaibian/\">改编</a></li><li><a class=\"\"href=\"/list/jingdian/\">经典</a></li><li><a class=\"\"href=\"/list/shenhua/\">神话</a></li><li><a class=\"\"href=\"/list/shaonian/\">少年</a></li><li><a class=\"\"href=\"/list/guaiwu/\">怪物</a></li><li><a class=\"\"href=\"/list/nixi/\">逆袭</a></li><li><a class=\"\"href=\"/list/zhongsheng/\">重生</a></li><li><a class=\"\"href=\"/list/yulequan/\">娱乐圈</a></li><li><a class=\"\"href=\"/list/jingsong/\">惊悚</a></li><li><a class=\"\"href=\"/list/qihuanmaoxian/\">奇幻冒险</a></li><li><a class=\"\"href=\"/list/youmogaoxiao/\">幽默搞笑</a></li><li><a class=\"\"href=\"/list/langmanaiqing/\">浪漫爱情</a></li><li><a class=\"\"href=\"/list/mofa/\">魔法</a></li><li><a class=\"\"href=\"/list/baihe/\">百合</a></li><li><a class=\"\"href=\"/list/fuchou/\">复仇</a></li><li><a class=\"\"href=\"/list/xiuxian/\">修仙</a></li><li><a class=\"\"href=\"/list/moshi/\">末世</a></li><li><a class=\"\"href=\"/list/quandou/\">权斗</a></li><li><a class=\"\"href=\"/list/caihong/\">彩虹</a></li><li><a class=\"\"href=\"/list/danmei/\">耽美</a></li><li><a class=\"\"href=\"/list/wuxia/\">武侠</a></li><li><a class=\"\"href=\"/list/kongbu/\">恐怖</a></li><li><a class=\"\"href=\"/list/yanqing/\">言情</a></li><li><a class=\"\"href=\"/list/zhenren/\">真人</a></li><li><a class=\"\"href=\"/list/zhidou/\">智斗</a></li><li><a class=\"\"href=\"/list/lieqi/\">猎奇</a></li><li><a class=\"\"href=\"/list/jingqi/\">惊奇</a></li><li><a class=\"\"href=\"/list/gedou/\">格斗</a></li><li><a class=\"\"href=\"/list/gongdou/\">宫斗</a></li><li><a class=\"\"href=\"/list/xitong/\">系统</a></li><li><a class=\"\"href=\"/list/zongcai/\">总裁</a></li><li><a class=\"\"href=\"/list/langman/\">浪漫</a></li><li><a class=\"\"href=\"/list/mori/\">末日</a></li><li><a class=\"\"href=\"/list/gaotian/\">高甜</a></li><li><a class=\"\"href=\"/list/dianjing/\">电竞</a></li><li><a class=\"\"href=\"/list/xianxia/\">仙侠</a></li><li><a class=\"\"href=\"/list/xiongdiqing/\">兄弟情</a></li><li><a class=\"\"href=\"/list/richang/\">日常</a></li><li><a class=\"\"href=\"/list/hanman/\">韩漫</a></li><li><a class=\"\"href=\"/list/nuexin/\">虐心</a></li><li><a class=\"\"href=\"/list/zhiyu/\">治愈</a></li><li><a class=\"\"href=\"/list/mingxing/\">明星</a></li><li><a class=\"\"href=\"/list/weimei/\">唯美</a></li><li><a class=\"\"href=\"/list/sangshi/\">丧尸</a></li><li><a class=\"\"href=\"/list/shenxian/\">神仙</a></li><li><a class=\"\"href=\"/list/zhichang/\">职场</a></li><li><a class=\"\"href=\"/list/qiangwei/\">蔷薇</a></li><li><a class=\"\"href=\"/list/zhuangbi/\">装逼</a></li><li><a class=\"\"href=\"/list/nansheng/\">男生</a></li><li><a class=\"\"href=\"/list/nvsheng/\">女生</a></li><li><a class=\"\"href=\"/list/huanlexiang/\">欢乐向</a></li><li><a class=\"\"href=\"/list/aiqing/\">爱情</a></li><li><a class=\"\"href=\"/list/qingxiaoshuo/\">轻小说</a></li><li><a class=\"\"href=\"/list/zhentan/\">侦探</a></li><li><a class=\"\"href=\"/list/unknown/\">ゆり</a></li><li><a class=\"\"href=\"/list/lizhi/\">励志</a></li><li><a class=\"\"href=\"/list/tiyu/\">体育</a></li><li><a class=\"\"href=\"/list/meishi/\">美食</a></li><li><a class=\"\"href=\"/list/shengui/\">神鬼</a></li><li><a class=\"\"href=\"/list/mengxi/\">萌系</a></li><li><a class=\"\"href=\"/list/wenxin/\">温馨</a></li><li><a class=\"\"href=\"/list/gandong/\">感动</a></li><li><a class=\"\"href=\"/list/gongting/\">宫廷</a></li><li><a class=\"\"href=\"/list/yishijie/\">异世界</a></li><li><a class=\"\"href=\"/list/zhengju/\">正剧</a></li><li><a class=\"\"href=\"/list/AA/\">AA</a></li><li><a class=\"\"href=\"/list/xiuji/\">秀吉</a></li><li><a class=\"\"href=\"/list/yineng/\">异能</a></li><li><a class=\"\"href=\"/list/shangzhan/\">商战</a></li><li><a class=\"\"href=\"/list/chongwu/\">宠物</a></li><li><a class=\"\"href=\"/list/shuangliu/\">爽流</a></li><li><a class=\"\"href=\"/list/jiating/\">家庭</a></li><li><a class=\"\"href=\"/list/hunyin/\">婚姻</a></li><li><a class=\"\"href=\"/list/haomen/\">豪门</a></li><li><a class=\"\"href=\"/list/shaonvaiqing/\">少女爱情</a></li><li><a class=\"\"href=\"/list/anhei/\">暗黑</a></li><li><a class=\"\"href=\"/list/yaoguai/\">妖怪</a></li><li><a class=\"\"href=\"/list/jijia/\">机甲</a></li><li><a class=\"\"href=\"/list/xingzhuan/\">性转</a></li><li><a class=\"\"href=\"/list/quanmou/\">权谋</a></li><li><a class=\"\"href=\"/list/sige/\">四格</a></li><li><a class=\"\"href=\"/list/yanyi/\">颜艺</a></li><li><a class=\"\"href=\"/list/gaoxiaoxiju/\">搞笑喜剧</a></li><li><a class=\"\"href=\"/list/dongfang/\">东方</a></li><li><a class=\"\"href=\"/list/xingzhuanhuan/\">性转换</a></li><li><a class=\"\"href=\"/list/neihan/\">内涵</a></li><li><a class=\"\"href=\"/list/duanzi/\">段子</a></li><li><a class=\"\"href=\"/list/jiakongshijie/\">架空世界</a></li><li><a class=\"\"href=\"/list/shenhao/\">神豪</a></li><li><a class=\"\"href=\"/list/juwei/\">橘味</a></li><li><a class=\"\"href=\"/list/jingxian/\">惊险</a></li><li><a class=\"\"href=\"/list/guzhuang/\">古装</a></li><li><a class=\"\"href=\"/list/zhiyu2/\">致郁</a></li><li><a class=\"\"href=\"/list/shenmo/\">神魔</a></li><li><a class=\"\"href=\"/list/jizhan/\">机战</a></li><li><a class=\"\"href=\"/list/haokuai/\">豪快</a></li><li><a class=\"\"href=\"/list/youmo/\">幽默</a></li><li><a class=\"\"href=\"/list/meixing/\">美型</a></li><li><a class=\"\"href=\"/list/weilai/\">未来</a></li><li><a class=\"\"href=\"/list/gufengchuanyue/\">古风穿越</a></li><li><a class=\"\"href=\"/list/zhenhan/\">震撼</a></li><li><a class=\"\"href=\"/list/fuhei/\">腹黑</a></li><li><a class=\"\"href=\"/list/dushidanvzhu/\">都市大女主</a></li><li><a class=\"\"href=\"/list/nuelian/\">虐恋</a></li><li><a class=\"\"href=\"/list/jianniang/\">舰娘</a></li><li><a class=\"\"href=\"/list/yinv/\">乙女</a></li><li><a class=\"\"href=\"/list/yinyuewudao/\">音乐舞蹈</a></li><li><a class=\"\"href=\"/list/zongheqita/\">综合其它</a></li><li><a class=\"\"href=\"/list/shishi/\">史诗</a></li><li><a class=\"\"href=\"/list/jinshouzhi/\">金手指</a></li><li><a class=\"\"href=\"/list/ouxiang/\">偶像</a></li><li><a class=\"\"href=\"/list/qingnian/\">青年</a></li><li><a class=\"\"href=\"/list/shaonao/\">烧脑</a></li><li><a class=\"\"href=\"/list/xifangmohuan/\">西方魔幻</a></li><li><a class=\"\"href=\"/list/tongren/\">同人</a></li><li><a class=\"\"href=\"/list/qita2/\">其它</a></li><li><a class=\"\"href=\"/list/BL/\">BL</a></li><li><a class=\"\"href=\"/list/xiaojiangshi/\">小僵尸</a></li><li><a class=\"\"href=\"/list/zhaohuanshou/\">召唤兽</a></li><li><a class=\"\"href=\"/list/meishaonv/\">美少女</a></li><li><a class=\"\"href=\"/list/dujia/\">独家</a></li><li><a class=\"\"href=\"/list/baoxiaoxiju/\">爆笑喜剧</a></li><li><a class=\"\"href=\"/list/xiangcun/\">乡村</a></li><li><a class=\"\"href=\"/list/keji/\">科技</a></li><li><a class=\"\"href=\"/list/riman/\">日漫</a></li><li><a class=\"\"href=\"/list/gushimanhua/\">故事漫画</a></li><li><a class=\"\"href=\"/list/qinggan/\">情感</a></li><li><a class=\"\"href=\"/list/TS/\">TS</a></li><li><a class=\"\"href=\"/list/gaoqingdanxing/\">高清单行</a></li><li><a class=\"\"href=\"/list/zhaixi/\">宅系</a></li><li><a class=\"\"href=\"/list/weiniang/\">伪娘</a></li><li><a class=\"\"href=\"/list/jiujie/\">纠结</a></li><li><a class=\"\"href=\"/list/wanjie/\">完结</a></li><li><a class=\"\"href=\"/list/shougong/\">手工</a></li><li><a class=\"\"href=\"/list/shaoer/\">少儿</a></li><li><a class=\"\"href=\"/list/jiecao/\">节操</a></li><li><a class=\"\"href=\"/list/lunli/\">伦理</a></li><li><a class=\"\"href=\"/list/xianzhiji/\">限制级</a></li><li><a class=\"\"href=\"/list/zhaidou/\">宅斗</a></li><li><a class=\"\"href=\"/list/nuanmeng/\">暖萌</a></li><li><a class=\"\"href=\"/list/zhupu/\">主仆</a></li><li><a class=\"\"href=\"/list/shenshi/\">绅士</a></li><li><a class=\"\"href=\"/list/zhiyinmanke/\">知音漫客</a></li><li><a class=\"\"href=\"/list/qitamanhua/\">其他漫画</a></li><li><a class=\"\"href=\"/list/youyaoqi/\">有妖气</a></li><li><a class=\"\"href=\"/list/wuxiagedou/\">武侠格斗</a></li><li><a class=\"\"href=\"/list/gaoxiaoegao/\">搞笑恶搞</a></li><li><a class=\"\"href=\"/list/duoshijie/\">多世界</a></li><li><a class=\"\"href=\"/list/changtiao/\">长条</a></li><li><a class=\"\"href=\"/list/kejin/\">氪金</a></li><li><a class=\"\"href=\"/list/xinzuo/\">新作</a></li><li><a class=\"\"href=\"/list/shehui/\">社会</a></li><li><a class=\"\"href=\"/list/lianaishenghuo/\">恋爱生活</a></li><li><a class=\"\"href=\"/list/jiangshi/\">僵尸</a></li><li><a class=\"\"href=\"/list/luoli/\">萝莉</a></li><li><a class=\"\"href=\"/list/furui/\">福瑞</a></li><li><a class=\"\"href=\"/list/sigeduoge/\">四格多格</a></li><li><a class=\"\"href=\"/list/gongtingdongfang/\">宫廷东方</a></li><li><a class=\"\"href=\"/list/fanai/\">泛爱</a></li><li><a class=\"\"href=\"/list/rexuemaoxian/\">热血冒险</a></li><li><a class=\"\"href=\"/list/shengcun/\">生存</a></li><li><a class=\"\"href=\"/list/2021dasai/\">2021大赛</a></li><li><a class=\"\"href=\"/list/lianaixiaoyuanshenghuo/\">恋爱校园生活</a></li><li><a class=\"\"href=\"/list/xiandai/\">现代</a></li><li><a class=\"\"href=\"/list/xihuan/\">西幻</a></li><li><a class=\"\"href=\"/list/xixie/\">吸血</a></li><li><a class=\"\"href=\"/list/qinqing/\">亲情</a></li><li><a class=\"\"href=\"/list/zhentantuili/\">侦探推理</a></li><li><a class=\"\"href=\"/list/wuxiaxianxia/\">武侠仙侠</a></li><li><a class=\"\"href=\"/list/youxijingji/\">游戏竞技</a></li><li><a class=\"\"href=\"/list/nvshen/\">女神</a></li><li><a class=\"\"href=\"/list/chaojiyingxiong/\">超级英雄</a></li><li><a class=\"\"href=\"/list/xuanyilingyi/\">悬疑灵异</a></li><li><a class=\"\"href=\"/list/weilaimanhuajia/\">未来漫画家</a></li><li><a class=\"\"href=\"/list/zhengnengliang/\">正能量</a></li><li><a class=\"\"href=\"/list/xiuzhenlianaigufeng/\">修真恋爱古风</a></li><li><a class=\"\"href=\"/list/xuexing/\">血腥</a></li><li><a class=\"\"href=\"/list/oufeng/\">欧风</a></li><li><a class=\"\"href=\"/list/GL/\">GL</a></li><li><a class=\"\"href=\"/list/qihuanaiqing/\">奇幻爱情</a></li><li><a class=\"\"href=\"/list/juxi/\">橘系</a></li><li><a class=\"\"href=\"/list/dongzuomaoxian/\">动作冒险</a></li><li><a class=\"\"href=\"/list/oushigongting/\">欧式宫廷</a></li><li><a class=\"\"href=\"/list/gudaigongting/\">古代宫廷</a></li><li><a class=\"\"href=\"/list/judiao/\">橘调</a></li><li><a class=\"\"href=\"/list/yangcheng/\">养成</a></li><li><a class=\"\"href=\"/list/guaitan/\">怪谈</a></li><li><a class=\"\"href=\"/list/taiwanyuanchuangzuopin/\">台湾原创作品</a></li><li><a class=\"\"href=\"/list/shenman/\">神漫</a></li><li><a class=\"\"href=\"/list/lianaidanmei/\">恋爱耽美</a></li><li><a class=\"\"href=\"/list/yingshihua/\">影视化</a></li></ul>";
        Map<String, String> map = new LinkedHashMap<>();
        JsoupNode node = new JsoupNode(html);
        Elements elements = node.getElements("a");
        for (Element element : elements) {
            node.init(element);
            map.put(node.ownText("a"), getIndex() + node.href("a"));
        }
        return map;
    }

    @Override
    public List<EntityInfo> getRankInfoList(String html) {
        List<EntityInfo> list = getInfoList(html);
        if (list.size() > 0) {
            return list;
        } else {
            JsoupStarter<EntityInfo> starter = new JsoupStarter<EntityInfo>() {
                @Override
                protected EntityInfo dealElement(JsoupNode node) {
                    String title = node.ownText("a.txtA");
                    String author = node.ownText("span.info");
                    String updateTime = null;
                    String updateChapter = null;
                    String imgUrl = node.src("img");
                    String detailUrl = node.href("a.txtA");
                    return new ComicInfo(getSourceId(), title, author, detailUrl, imgUrl, updateTime, updateChapter);
                }
            };
            return starter.startElements(html, "li.list-comic");
        }
    }

}
