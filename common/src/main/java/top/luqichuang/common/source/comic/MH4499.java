package top.luqichuang.common.source.comic;

import java.util.List;
import java.util.Map;

import okhttp3.Request;
import top.luqichuang.common.en.CSourceEnum;
import top.luqichuang.common.model.ChapterInfo;
import top.luqichuang.common.model.EntityInfo;
import top.luqichuang.common.model.builder.ChapterInfoBuilder;
import top.luqichuang.common.model.builder.EntityInfoBuilder;
import top.luqichuang.common.model.comic.BaseComicSource;
import top.luqichuang.common.starter.ElementNode;
import top.luqichuang.common.starter.ElementStarter;
import top.luqichuang.common.util.NetUtil;
import top.luqichuang.common.util.StringUtil;

/**
 * @author 18472
 * @desc
 * @date 2024/3/28 15:51
 * @ver 1.0
 */
public class MH4499 extends BaseComicSource {

    @Override
    public CSourceEnum getCSourceEnum() {
        return CSourceEnum.MH_4499;
    }

    @Override
    public String getIndex() {
        return "https://www.m4499.com";
    }

    @Override
    public Request getSearchRequest(String searchString) {
        String url = String.format("%s/so/search.html?searchkey=%s", getIndex(), searchString);
        return NetUtil.getRequest(url);
    }

    @Override
    public Request buildRequest(String html, String tag, Map<String, Object> data, Map<String, Object> map) {
        if (DETAIL.equals(tag) && map.isEmpty()) {
            map.put("infoHtml", html);
            ElementNode node = new ElementNode(html);
            String listUrl = getIndex() + node.href("span.more a");
            return NetUtil.getRequest(listUrl);
        }
        return super.buildRequest(html, tag, data, map);
    }

    @Override
    protected InfoListConfig getInfoListConfig(ElementNode node) {
        return new InfoListConfig() {
            @Override
            protected void addListConfig(List<ListConfig<EntityInfo>> configList) {
                configList.add(new ListConfig<EntityInfo>() {
                    @Override
                    protected String[] getQuery() {
                        return new String[]{"ul.library li"};
                    }

                    @Override
                    protected EntityInfo getData(ElementNode node) {
                        String updateTime = node.ownText("p");
                        updateTime = StringUtil.remove(updateTime, "作者：");
                        String imgUrl = node.src("img");
                        if (imgUrl != null && !imgUrl.startsWith("http")) {
                            imgUrl = getIndex() + imgUrl;
                        }
                        return new EntityInfoBuilder(getInfoClass())
                                .buildSourceId(getSourceId())
                                .buildTitle(node.ownText("a.bookname"))
                                .buildAuthor(node.ownText("a.author"))
                                .buildUpdateTime(updateTime)
                                .buildUpdateChapter(node.ownText("a.chapter"))
                                .buildImgUrl(imgUrl)
                                .buildDetailUrl(getIndex() + node.href("a"))
                                .build();
                    }
                });
            }
        };
    }

    @Override
    protected InfoDetailConfig getInfoDetailConfig(ElementNode node) {
        return new InfoDetailConfig() {
            @Override
            protected String getHtml(String html, Map<String, Object> map) {
                return (String) map.get("infoHtml");
            }

            @Override
            protected void setData(EntityInfo info, ElementNode node) {
                new EntityInfoBuilder(info)
                        .buildSourceId(getSourceId())
                        .buildTitle(node.attr("meta[property='og:title']", "content"))
                        .buildAuthor(node.attr("meta[property='og:novel:author']", "content"))
                        .buildIntro(node.attr("meta[property='og:description']", "content"))
                        .buildUpdateTime(node.attr("meta[property='og:novel:update_time']", "content"))
                        .buildUpdateStatus(node.attr("meta[property='og:novel:status']", "content"))
                        .buildImgUrl(getIndex() + node.src("a.bookimg img"))
                        .build();
            }

            @Override
            protected void addListConfig(List<ListConfig<ChapterInfo>> configList) {
                configList.add(new ListConfig<ChapterInfo>() {
                    @Override
                    protected boolean needSwap(List<ChapterInfo> list) {
                        int start = 0;
                        int end = 0;
                        if (!list.isEmpty()) {
                            int num = 10;
                            for (int i = 0; i < Math.min(num, list.size()); i++) {
                                String s = StringUtil.match("第(\\d+)", list.get(i).getTitle());
                                if (s != null) {
                                    start = Integer.parseInt(s);
                                    break;
                                }
                            }
                            for (int i = list.size() - 1; i >= Math.max(0, list.size() - 10); i--) {
                                String s = StringUtil.match("第(\\d+)", list.get(i).getTitle());
                                if (s != null) {
                                    end = Integer.parseInt(s);
                                    break;
                                }
                            }
                        }
                        return start <= end;
                    }

                    @Override
                    protected String[] getQuery() {
                        return new String[]{"div.read dl:eq(1) dd"};
                    }

                    @Override
                    protected ChapterInfo getData(ElementNode node) {
                        return new ChapterInfoBuilder()
                                .buildTitle(node.ownText("a"))
                                .buildUrl(getIndex() + node.href("a"))
                                .build();
                    }
                });
            }
        };
    }

    @Override
    protected ContentListConfig getContentListConfig(ElementNode node) {
        return new ContentListConfig() {
            @Override
            protected void addUrlList(ElementNode node, List<String> urlList, Map<String, Object> map) {
                ElementStarter<String> starter = new ElementStarter<String>() {
                    @Override
                    protected String loadListData(ElementNode node) {
                        return node.attr("img", "data-src");
                    }
                };
                urlList.addAll(starter.startList(node.getHtml(), "div#content img"));
            }
        };
    }

    @Override
    protected RankMapConfig getRankMapConfig() {
        return new RankMapConfig() {
            @Override
            protected String getHtml() {
                return "<ul class=\"top\"><li><a href=\"/top/allvisit.html\">总点击漫画榜</a></li><li><a href=\"/top/monthvisit.html\">月点击漫画榜</a></li><li><a href=\"/top/weekvisit.html\">周点击漫画榜</a></li><li><a href=\"/top/dayvisit.html\">日点击漫画榜</a></li><li><a href=\"/top/votenum.html\">总推荐漫画榜</a></li><li><a href=\"/top/marknum.html\">总收藏漫画榜</a></li><li><a href=\"/top/downnum.html\">总下载漫画榜</a></li><li><a href=\"/top/lastupdate.html\">最近更新榜</a></li><li><a href=\"/top/postdate.html\">入库新书榜</a></li></ul><ul class=\"filter\"><li><dl><dt>分类：</dt><dd class=\"on\"><a href=\"/sort/all_0_0_lastupdate_1.html\">全部漫画</a></dd><dd><a href=\"/sort/guonei_0_0_lastupdate_1.html\">国内漫画</a></dd><dd><a href=\"/sort/shaonv_0_0_lastupdate_1.html\">少女漫画</a></dd><dd><a href=\"/sort/hanguo_0_0_lastupdate_1.html\">韩国漫画</a></dd><dd><a href=\"/sort/riben_0_0_lastupdate_1.html\">日本漫画</a></dd><dd><a href=\"/sort/dsyq_0_0_lastupdate_1.html\">都市言情</a></dd><dd><a href=\"/sort/lscy_0_0_lastupdate_1.html\">历史穿越</a></dd><dd><a href=\"/sort/jskh_0_0_lastupdate_1.html\">军事科幻</a></dd><dd><a href=\"/sort/yxjj_0_0_lastupdate_1.html\">游戏竞技</a></dd><dd><a href=\"/sort/kbxy_0_0_lastupdate_1.html\">恐怖悬疑</a></dd><dd><a href=\"/sort/nvzq_0_0_lastupdate_1.html\">女生专区</a></dd><dd><a href=\"/sort/zhlx_0_0_lastupdate_1.html\">综合类型</a></dd></dl></li><li><dl><dt>状态：</dt><dd><a href=\"/sort/all_0_1_lastupdate_1.html\">连载中</a></dd><dd><a href=\"/sort/all_0_2_lastupdate_1.html\">已完本</a></dd></dl></li><li><dl><dt>章节：</dt><ds><a href=\"/sort/all_1_0_lastupdate_1.html\">500章以下</a></ds><ds><a href=\"/sort/all_2_0_lastupdate_1.html\">500章-1000章</a></ds><ds><a href=\"/sort/all_3_0_lastupdate_1.html\">1000章以上</a></ds></dl></li></ul>";
            }

            @Override
            protected String getCssQuery() {
                return "a";
            }

            @Override
            protected String getTitle(ElementNode node) {
                return node.ownText("a");
            }

            @Override
            protected String getUrl(ElementNode node) {
                String url = getIndex() + node.href("a");
                return StringUtil.replace(url, "_1.html", "_%d.html");
            }
        };
    }

    @Override
    protected InfoListConfig getRankListConfig(ElementNode node) {
        return new InfoListConfig() {
            @Override
            protected void addListConfig(List<ListConfig<EntityInfo>> configList) {
                configList.addAll(getInfoListConfig(node).getConfigList());
                configList.add(new ListConfig<EntityInfo>() {
                    @Override
                    protected String[] getQuery() {
                        return new String[]{"ul.rank li"};
                    }

                    @Override
                    protected EntityInfo getData(ElementNode node) {
                        String updateTime = node.ownText("p.chapter");
                        updateTime = StringUtil.remove(updateTime, "最新章节：");
                        String imgUrl = node.src("img");
                        if (imgUrl != null && !imgUrl.startsWith("http")) {
                            imgUrl = getIndex() + imgUrl;
                        }
                        return new EntityInfoBuilder(getInfoClass())
                                .buildSourceId(getSourceId())
                                .buildTitle(node.attr("img", "alt"))
                                .buildAuthor(node.ownText("p:eq(4) a"))
                                .buildUpdateTime(updateTime)
                                .buildUpdateChapter(node.ownText("p.chapter a"))
                                .buildImgUrl(imgUrl)
                                .buildDetailUrl(getIndex() + node.href("a"))
                                .build();
                    }
                });
            }
        };
    }
}
