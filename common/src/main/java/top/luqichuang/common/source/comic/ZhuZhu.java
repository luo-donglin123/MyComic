package top.luqichuang.common.source.comic;

import java.util.List;
import java.util.Map;

import okhttp3.Request;
import top.luqichuang.common.en.CSourceEnum;
import top.luqichuang.common.model.ChapterInfo;
import top.luqichuang.common.model.EntityInfo;
import top.luqichuang.common.model.builder.ChapterInfoBuilder;
import top.luqichuang.common.model.builder.EntityInfoBuilder;
import top.luqichuang.common.model.comic.BaseComicSource;
import top.luqichuang.common.starter.ElementNode;
import top.luqichuang.common.util.DecryptUtil;
import top.luqichuang.common.util.NetUtil;
import top.luqichuang.common.util.StringUtil;

/**
 * @author 18472
 * @desc
 * @date 2024/3/30 14:37
 * @ver 1.0
 */
public class ZhuZhu extends BaseComicSource {
    @Override
    public CSourceEnum getCSourceEnum() {
        return CSourceEnum.ZHU_ZHU;
    }

    @Override
    public String getIndex() {
        return "https://www.zhuzhumh.com";
    }

    @Override
    public Request getSearchRequest(String searchString) {
        String url = String.format("%s/search.html?keyword=%s", getIndex(), searchString);
        return NetUtil.getRequest(url);
    }

    @Override
    public Request buildRequest(String html, String tag, Map<String, Object> data, Map<String, Object> map) {
        if (DETAIL.equals(tag) && map.isEmpty()) {
            map.put("infoHtml", html);
            ElementNode node = new ElementNode(html);
            String dataId = node.attr("a#zhankai", "data-id");
            String dataVid = node.attr("a#zhankai", "data-vid");
            String url = String.format("%s/api/bookchapter?id=%s&id2=%s", getIndex(), dataId, dataVid);
            return NetUtil.getRequest(url);
        }
        return super.buildRequest(html, tag, data, map);
    }

    @Override
    protected InfoListConfig getInfoListConfig(ElementNode node) {
        return new InfoListConfig() {
            @Override
            protected void addListConfig(List<ListConfig<EntityInfo>> configList) {
                configList.add(new ListConfig<EntityInfo>() {
                    @Override
                    protected String[] getQuery() {
                        return new String[]{"div.cy_list_mh ul"};
                    }

                    @Override
                    protected EntityInfo getData(ElementNode node) {
                        return new EntityInfoBuilder(getInfoClass())
                                .buildSourceId(getSourceId())
                                .buildTitle(node.title("a"))
                                .buildAuthor(null)
                                .buildUpdateTime(null)
                                .buildUpdateChapter(null)
                                .buildImgUrl(node.src("img"))
                                .buildDetailUrl(node.href("a"))
                                .build();
                    }
                });
            }
        };
    }

    protected InfoDetailConfig getInfoDetailConfig(ElementNode node) {
        return new InfoDetailConfig() {
            @Override
            protected String getHtml(String html, Map<String, Object> map) {
                return (String) map.get("infoHtml");
            }

            @Override
            protected void setData(EntityInfo info, ElementNode node) {
                String author = node.ownText("div.cy_xinxi span");
                author = StringUtil.remove(author, "作者：");
                new EntityInfoBuilder(info)
                        .buildSourceId(getSourceId())
                        .buildTitle(node.ownText("div.cy_title h1"))
                        .buildAuthor(author)
                        .buildIntro(node.ownText("p#comic-description"))
                        .buildUpdateTime(node.ownText("div.cy_zhangjie_top font"))
                        .buildUpdateStatus(node.ownText("div.cy_xinxi font"))
                        .buildImgUrl(node.src("img"))
                        .build();
            }

            @Override
            protected void addListConfig(List<ListConfig<ChapterInfo>> configList) {
                configList.add(new ListConfig<ChapterInfo>() {
                    @Override
                    protected String getHtml(String html, Map<String, Object> map) {
                        return (String) map.get("infoHtml");
                    }

                    @Override
                    protected String[] getQuery() {
                        return new String[]{"ul#mh-chapter-list-ol-0 li"};
                    }

                    @Override
                    protected ChapterInfo getData(ElementNode node) {
                        return new ChapterInfoBuilder()
                                .buildTitle(node.ownText("p"))
                                .buildUrl(node.href("a"))
                                .build();
                    }
                });
                configList.add(new ListConfig<ChapterInfo>() {
                    @Override
                    protected String[] getQuery() {
                        return new String[]{""};
                    }

                    @Override
                    protected ChapterInfo getData(ElementNode node) {
                        String id = node.getString("chapterid");
                        String chapterUrl = String.format("%s/chapter-redirect/%s.html", getIndex(), id);
                        return new ChapterInfoBuilder()
                                .buildTitle(node.getString("chaptername"))
                                .buildUrl(chapterUrl)
                                .build();
                    }
                });
            }
        };
    }

    protected ContentListConfig getContentListConfig(ElementNode node) {
        return new ContentListConfig() {
            @Override
            protected void addUrlList(ElementNode node, List<String> urlList, Map<String, Object> map) {
                String html = node.getHtml();
                String regex = "(eval.*?\\{.*?\\}.*?\\)\\)\\))";
                String js = StringUtil.match(regex, html);
                String code = DecryptUtil.decryptPackedJsCode(js);
                List<String> aesList = StringUtil.matchList("\"(.*?)\"", code);
                if (aesList.size() == 3) {
                    code = aesList.get(0);
                    String key = aesList.get(1);
                    String iv = aesList.get(2).substring(0, 16);
                    code = DecryptUtil.decryptAES(code, key, iv);
                    code = StringUtil.remove(code, "\\");
                    List<String> list = StringUtil.matchList("\"(.*?)\"", code);
                    urlList.addAll(list);
                }
            }
        };
    }

    protected RankMapConfig getRankMapConfig() {
        return new RankMapConfig() {
            @Override
            protected String getHtml() {
                return "<ul><li class=\"btn\"><a href=\"https://www.zhuzhumh.com/rank/1-1.html\">人气榜</a></li><li class=\"\"><a href=\"https://www.zhuzhumh.com/rank/2-1.html\">周读榜</a></li><li class=\"\"><a href=\"https://www.zhuzhumh.com/rank/3-1.html\">月读榜</a></li><li class=\"\"><a href=\"https://www.zhuzhumh.com/rank/4-1.html\">火爆榜</a></li><li class=\"\"><a href=\"https://www.zhuzhumh.com/rank/5-1.html\">更新榜</a></li><li class=\"\"><a href=\"https://www.zhuzhumh.com/rank/6-1.html\">新漫榜</a></li></ul><ul><li class=\"btn\"><a href=\"https://www.zhuzhumh.com/sort/1-1.html\">冒险热血</a></li><li class=\"\"><a href=\"https://www.zhuzhumh.com/sort/2-1.html\">武侠格斗</a></li><li class=\"\"><a href=\"https://www.zhuzhumh.com/sort/3-1.html\">科幻魔幻</a></li><li class=\"\"><a href=\"https://www.zhuzhumh.com/sort/4-1.html\">侦探推理</a></li><li class=\"\"><a href=\"https://www.zhuzhumh.com/sort/5-1.html\">耽美爱情</a></li><li class=\"\"><a href=\"https://www.zhuzhumh.com/sort/6-1.html\">生活漫画</a></li><li class=\"\"><a href=\"https://www.zhuzhumh.com/sort/12-1.html\">完结漫画</a></li><li class=\"\"><a href=\"https://www.zhuzhumh.com/sort/13-1.html\">连载漫画</a></li></ul>";
            }

            @Override
            protected String getCssQuery() {
                return "a";
            }

            @Override
            protected String getTitle(ElementNode node) {
                return node.ownText("a");
            }

            @Override
            protected String getUrl(ElementNode node) {
                String url = node.href("a");
                return StringUtil.replace(url, "-1.html", "-%d.html");
            }
        };
    }

    @Override
    protected InfoListConfig getRankListConfig(ElementNode node) {
        return new InfoListConfig() {
            @Override
            protected void addListConfig(List<ListConfig<EntityInfo>> configList) {
                configList.add(new ListConfig<EntityInfo>() {
                    @Override
                    protected String[] getQuery() {
                        return new String[]{"div.cy_list_mh ul"};
                    }

                    @Override
                    protected EntityInfo getData(ElementNode node) {
                        return new EntityInfoBuilder(getInfoClass())
                                .buildSourceId(getSourceId())
                                .buildTitle(node.ownText("li.title a"))
                                .buildAuthor(null)
                                .buildUpdateTime(null)
                                .buildUpdateChapter(node.ownText("li.updata span"))
                                .buildImgUrl(node.src("img"))
                                .buildDetailUrl(node.href("a"))
                                .build();
                    }
                });
            }
        };
    }
}
