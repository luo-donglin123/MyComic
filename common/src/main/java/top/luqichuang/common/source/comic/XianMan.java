package top.luqichuang.common.source.comic;

import java.util.List;
import java.util.Map;

import okhttp3.Request;
import top.luqichuang.common.en.CSourceEnum;
import top.luqichuang.common.model.ChapterInfo;
import top.luqichuang.common.model.EntityInfo;
import top.luqichuang.common.model.builder.ChapterInfoBuilder;
import top.luqichuang.common.model.builder.EntityInfoBuilder;
import top.luqichuang.common.model.comic.BaseComicSource;
import top.luqichuang.common.starter.ElementNode;
import top.luqichuang.common.util.DecryptUtil;
import top.luqichuang.common.util.NetUtil;
import top.luqichuang.common.util.StringUtil;

/**
 * @author 18472
 * @desc
 * @date 2024/3/30 13:09
 * @ver 1.0
 */
public class XianMan extends BaseComicSource {
    @Override
    public CSourceEnum getCSourceEnum() {
        return CSourceEnum.XIAN_MAN;
    }

    @Override
    public String getIndex() {
        return "https://www.gaonaojin.com";
    }

    @Override
    public Request getSearchRequest(String searchString) {
        String url = String.format("%s/index/index/search.html?keyboard=%s", getIndex(), searchString);
        return NetUtil.getRequest(url);
    }

    @Override
    protected InfoListConfig getInfoListConfig(ElementNode node) {
        return new InfoListConfig() {
            @Override
            protected void addListConfig(List<ListConfig<EntityInfo>> configList) {
                configList.add(new ListConfig<EntityInfo>() {
                    @Override
                    protected String[] getQuery() {
                        return new String[]{"ul.mh-list li"};
                    }

                    @Override
                    protected EntityInfo getData(ElementNode node) {
                        return new EntityInfoBuilder(getInfoClass())
                                .buildSourceId(getSourceId())
                                .buildTitle(node.ownText("h2.title a"))
                                .buildAuthor(null)
                                .buildUpdateTime(null)
                                .buildUpdateChapter(node.ownText("p.chapter a"))
                                .buildImgUrl(node.src("img"))
                                .buildImgUrl(node.attr("p.mh-cover", "data-original"))
                                .buildDetailUrl(getIndex() + node.href("a"))
                                .build();
                    }
                });
            }
        };
    }

    @Override
    protected InfoDetailConfig getInfoDetailConfig(ElementNode node) {
        return new InfoDetailConfig() {
            @Override
            protected void setData(EntityInfo info, ElementNode node) {
                String updateTime = node.ownText("p.tip span:eq(2)");
                updateTime = StringUtil.remove(updateTime, "更新时间：");
                new EntityInfoBuilder(info)
                        .buildSourceId(getSourceId())
                        .buildTitle(node.ownText("div.info h1"))
                        .buildAuthor(node.ownText("p.subtitle a"))
                        .buildIntro(node.ownText("p.content"))
                        .buildUpdateTime(updateTime)
                        .buildUpdateStatus(node.ownText("p.tip span span"))
                        .buildImgUrl(node.src("div.cover img"))
                        .build();
            }

            @Override
            protected void addListConfig(List<ListConfig<ChapterInfo>> configList) {
                configList.add(new ListConfig<ChapterInfo>() {
                    @Override
                    protected String[] getQuery() {
                        return new String[]{"ul#detail-list-select-1 li"};
                    }

                    @Override
                    protected ChapterInfo getData(ElementNode node) {
                        return new ChapterInfoBuilder()
                                .buildTitle(node.ownText("a"))
                                .buildUrl(getIndex() + node.href("a"))
                                .build();
                    }
                });
            }
        };
    }

    @Override
    protected ContentListConfig getContentListConfig(ElementNode node) {
        return new ContentListConfig() {
            @Override
            protected String getPrev() {
                return "https://res.xiaoqinre.com/";
            }

            @Override
            protected void addUrlList(ElementNode node, List<String> urlList, Map<String, Object> map) {
                String html = node.getHtml();
                String js = StringUtil.match("(eval\\(function.*?\\{\\}\\)\\))", html);
                String code = DecryptUtil.decryptPackedJsCode(js);
                urlList.addAll(StringUtil.matchList("\"(.*?)\"", code));
            }
        };
    }

    @Override
    protected RankMapConfig getRankMapConfig() {
        return new RankMapConfig() {
            @Override
            protected String getHtml() {
                return "<div class=\"container\"><figure class=\"cat-filter\"><div class=\"field-wrap\"><dl class=\"field-list\"><dt>题材:</dt><dd><a href=\"/f-1-0-0-0-0-0-1.html\">全部</a></dd><dd><a class=\"active\"href=\"/f-1-8-0-0-0-0-1.html\">仙侠</a></dd><dd><a href=\"/f-1-25-0-0-0-0-1.html\">其它</a></dd><dd><a href=\"/f-1-6-0-0-0-0-1.html\">冒险</a></dd><dd><a href=\"/f-1-16-0-0-0-0-1.html\">古风</a></dd><dd><a href=\"/f-1-7-0-0-0-0-1.html\">后宫</a></dd><dd><a href=\"/f-1-13-0-0-0-0-1.html\">奇幻</a></dd><dd><a href=\"/f-1-2-0-0-0-0-1.html\">恋爱</a></dd><dd><a href=\"/f-1-14-0-0-0-0-1.html\">恐怖</a></dd><dd><a href=\"/f-1-10-0-0-0-0-1.html\">悬疑</a></dd><dd><a href=\"/f-1-11-0-0-0-0-1.html\">推理</a></dd><dd><a href=\"/f-1-12-0-0-0-0-1.html\">搞笑</a></dd><dd><a href=\"/f-1-18-0-0-0-0-1.html\">日常</a></dd><dd><a href=\"/f-1-3-0-0-0-0-1.html\">校园</a></dd><dd><a href=\"/f-1-24-0-0-0-0-1.html\">欢乐向</a></dd><dd><a href=\"/f-1-9-0-0-0-0-1.html\">武侠</a></dd><dd><a href=\"/f-1-19-0-0-0-0-1.html\">治愈</a></dd><dd><a href=\"/f-1-20-0-0-0-0-1.html\">烧脑</a></dd><dd><a href=\"/f-1-1-0-0-0-0-1.html\">热血</a></dd><dd><a href=\"/f-1-15-0-0-0-0-1.html\">玄幻</a></dd><dd><a href=\"/f-1-4-0-0-0-0-1.html\">百合</a></dd><dd><a href=\"/f-1-23-0-0-0-0-1.html\">竞技</a></dd><dd><a href=\"/f-1-5-0-0-0-0-1.html\">耽美</a></dd><dd><a href=\"/f-1-17-0-0-0-0-1.html\">萌系</a></dd><dd><a href=\"/f-1-21-0-0-0-0-1.html\">邪恶</a></dd><dd><a href=\"/f-1-22-0-0-0-0-1.html\">都市</a></dd></dl><dl class=\"field-list\"><dt>地区:</dt><dd><a href=\"/f-1-8-1-0-0-0-1.html\">国产</a></dd><dd><a href=\"/f-1-8-2-0-0-0-1.html\">日本</a></dd><dd><a href=\"/f-1-8-3-0-0-0-1.html\">欧美</a></dd><dd><a href=\"/f-1-8-4-0-0-0-1.html\">港台</a></dd><dd><a href=\"/f-1-8-5-0-0-0-1.html\">韩国</a></dd></dl><dl class=\"field-list\"><dt>进度:</dt><dd><a href=\"/f-1-8-0-1-0-0-1.html\">连载</a></dd><dd><a href=\"/f-1-8-0-2-0-0-1.html\">完结</a></dd></dl><dl class=\"field-list\"><dt>字母:</dt><dd><a href=\"/f-1-8-0-0-A-0-1.html\">A</a></dd><dd><a href=\"/f-1-8-0-0-B-0-1.html\">B</a></dd><dd><a href=\"/f-1-8-0-0-C-0-1.html\">C</a></dd><dd><a href=\"/f-1-8-0-0-D-0-1.html\">D</a></dd><dd><a href=\"/f-1-8-0-0-E-0-1.html\">E</a></dd><dd><a href=\"/f-1-8-0-0-F-0-1.html\">F</a></dd><dd><a href=\"/f-1-8-0-0-G-0-1.html\">G</a></dd><dd><a href=\"/f-1-8-0-0-H-0-1.html\">H</a></dd><dd><a href=\"/f-1-8-0-0-I-0-1.html\">I</a></dd><dd><a href=\"/f-1-8-0-0-J-0-1.html\">J</a></dd><dd><a href=\"/f-1-8-0-0-K-0-1.html\">K</a></dd><dd><a href=\"/f-1-8-0-0-L-0-1.html\">L</a></dd><dd><a href=\"/f-1-8-0-0-M-0-1.html\">M</a></dd><dd><a href=\"/f-1-8-0-0-N-0-1.html\">N</a></dd><dd><a href=\"/f-1-8-0-0-O-0-1.html\">O</a></dd><dd><a href=\"/f-1-8-0-0-P-0-1.html\">P</a></dd><dd><a href=\"/f-1-8-0-0-Q-0-1.html\">Q</a></dd><dd><a href=\"/f-1-8-0-0-R-0-1.html\">R</a></dd><dd><a href=\"/f-1-8-0-0-S-0-1.html\">S</a></dd><dd><a href=\"/f-1-8-0-0-T-0-1.html\">T</a></dd><dd><a href=\"/f-1-8-0-0-U-0-1.html\">U</a></dd><dd><a href=\"/f-1-8-0-0-V-0-1.html\">V</a></dd><dd><a href=\"/f-1-8-0-0-W-0-1.html\">W</a></dd><dd><a href=\"/f-1-8-0-0-X-0-1.html\">X</a></dd><dd><a href=\"/f-1-8-0-0-Y-0-1.html\">Y</a></dd><dd><a href=\"/f-1-8-0-0-Z-0-1.html\">Z</a></dd></dl></div></figure></div>";
            }

            @Override
            protected String getCssQuery() {
                return "a";
            }

            @Override
            protected String getTitle(ElementNode node) {
                return node.ownText("a");
            }

            @Override
            protected String getUrl(ElementNode node) {
                String url = getIndex() + node.href("a");
                return StringUtil.replace(url, "-1.html", "-%d.html");
            }
        };
    }
}
