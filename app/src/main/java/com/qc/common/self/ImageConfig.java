package com.qc.common.self;

import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.qmuiteam.qmui.widget.QMUIProgressBar;

import java.util.Map;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/2/24 22:30
 * @ver 1.0
 */
public class ImageConfig {

    private String url;

    private Map<String, String> headers;

    private RelativeLayout layout;

    private ImageView imageView;

    private QMUIProgressBar progressBar;

    private TextView textView;

    private Object saveKey;

    private boolean isSave;

    private boolean isForce;

    private boolean isSnap;

    private int defaultBitmapId;

    private int errorBitmapId;

    private int drawableId;

    private int height;

    private int width;

    private ImageView.ScaleType startScaleType;

    private ImageView.ScaleType readyScaleType;

    private ImageView.ScaleType overScaleType;

    public ImageConfig(String url, RelativeLayout layout) {
        this.url = url;
        this.layout = layout;
    }

    public ImageConfig() {

    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

    public RelativeLayout getLayout() {
        return layout;
    }

    public void setLayout(RelativeLayout layout) {
        this.layout = layout;
    }

    public ImageView getImageView() {
        return imageView;
    }

    public void setImageView(ImageView imageView) {
        this.imageView = imageView;
    }

    public QMUIProgressBar getProgressBar() {
        return progressBar;
    }

    public void setProgressBar(QMUIProgressBar progressBar) {
        this.progressBar = progressBar;
    }

    public TextView getTextView() {
        return textView;
    }

    public void setTextView(TextView textView) {
        this.textView = textView;
    }

    public Object getSaveKey() {
        return saveKey;
    }

    public void setSaveKey(Object saveKey) {
        this.saveKey = saveKey;
    }

    public boolean isSave() {
        return isSave;
    }

    public void setSave(boolean save) {
        isSave = save;
    }

    public boolean isForce() {
        return isForce;
    }

    public void setForce(boolean force) {
        isForce = force;
    }

    public boolean isSnap() {
        return isSnap;
    }

    public void setSnap(boolean snap) {
        isSnap = snap;
    }

    public int getDefaultBitmapId() {
        return defaultBitmapId;
    }

    public void setDefaultBitmapId(int defaultBitmapId) {
        this.defaultBitmapId = defaultBitmapId;
    }

    public int getErrorBitmapId() {
        return errorBitmapId;
    }

    public void setErrorBitmapId(int errorBitmapId) {
        this.errorBitmapId = errorBitmapId;
    }

    public int getDrawableId() {
        return drawableId;
    }

    public void setDrawableId(int drawableId) {
        this.drawableId = drawableId;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public ImageView.ScaleType getStartScaleType() {
        return startScaleType;
    }

    public void setStartScaleType(ImageView.ScaleType startScaleType) {
        this.startScaleType = startScaleType;
    }

    public ImageView.ScaleType getReadyScaleType() {
        return readyScaleType;
    }

    public void setReadyScaleType(ImageView.ScaleType readyScaleType) {
        this.readyScaleType = readyScaleType;
    }

    public ImageView.ScaleType getOverScaleType() {
        return overScaleType;
    }

    public void setOverScaleType(ImageView.ScaleType overScaleType) {
        this.overScaleType = overScaleType;
    }
}
