package com.qc.common.en;


import com.alibaba.fastjson.JSONObject;
import com.qc.common.en.data.Data;
import com.qc.common.util.SourceUtil;

import java.util.LinkedHashMap;
import java.util.Map;

import the.one.base.util.SpUtil;
import top.luqichuang.common.en.CSourceEnum;
import top.luqichuang.common.en.NSourceEnum;
import top.luqichuang.common.en.VSourceEnum;
import top.luqichuang.common.model.Source;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/1/16 21:45
 * @ver 1.0
 */
public enum SettingEnum {

    PRELOAD_NUM("preloadNum", 10000),
    READ_CONTENT("readContent", Data.COMIC_CODE),
    IS_FULL_SCREEN("isFullScreen", true),
    IS_GRID("isGrid", true),
    IS_CACHE("isCache", true),
    NOVEL_FONT_SIZE("novelFontSize", 20),
    NOVEL_AUTO_SPEED("novelAutoSpeed", 4),
    VIDEO_PROGRESS("videoProgress", new LinkedHashMap<>()),
    DEFAULT_COMIC_SOURCE("defaultCSource", CSourceEnum.BAO_ZI.ID),
    DEFAULT_NOVEL_SOURCE("defaultNSource", NSourceEnum.AI_YUE.ID),
    DEFAULT_VIDEO_SOURCE("defaultVSource", VSourceEnum.AI_YUN.ID),
    COMIC_SOURCE_OPEN("comicSourceOpen", CSourceEnum.getMAP().keySet()),
    NOVEL_SOURCE_OPEN("novelSourceOpen", NSourceEnum.getMAP().keySet()),
    VIDEO_SOURCE_OPEN("videoSourceOpen", VSourceEnum.getMAP().keySet()),
    COMIC_SOURCE_TOTAL("comicSourceTotal", CSourceEnum.getMAP().keySet()),
    NOVEL_SOURCE_TOTAL("novelSourceTotal", NSourceEnum.getMAP().keySet()),
    VIDEO_SOURCE_TOTAL("videoSourceTotal", VSourceEnum.getMAP().keySet()),
    READER_MODE("readerMode", Data.READER_MODE_V),
    ;

    public final String KEY;
    public final Object DEFAULT_VALUE;

    SettingEnum(String KEY, Object DEFAULT_VALUE) {
        this.KEY = KEY;
        this.DEFAULT_VALUE = DEFAULT_VALUE;
    }

    private static final String DESC = "Desc";
    private static final String DEFAULT_SP = "{}";
    private static final JSONObject JSON = JSONObject.parseObject(SpUtil.getInstance().getString(Data.SP_SAVE_STR, DEFAULT_SP));

    public <T> T value() {
        Object value = JSON.get(KEY);
        if (value != null) {
            return (T) value;
        } else {
            setValue(DEFAULT_VALUE);
            return (T) DEFAULT_VALUE;
        }
    }

    public String valueDesc() {
        String desc = JSON.getString(KEY + DESC);
        if (desc != null && !desc.equals("")) {
            return desc;
        } else {
            return getDefaultDesc();
        }
    }

    public void setValue(Object value) {
        setValue(value, "");
    }

    public void setValue(Object value, String desc) {
        JSON.put(KEY, value);
        JSON.put(KEY + DESC, desc);
        SpUtil.getInstance().putString(Data.SP_SAVE_STR, JSON.toJSONString());
    }

    public Map<Object, String> getItemMap() {
        Map<Object, String> map = new LinkedHashMap<>();
        if (this == SettingEnum.DEFAULT_COMIC_SOURCE) {
            for (Source source : SourceUtil.getSourceList()) {
                map.put(source.getSourceId(), source.getSourceName());
            }
        } else if (this == SettingEnum.DEFAULT_NOVEL_SOURCE) {
            for (Source source : SourceUtil.getSourceList()) {
                map.put(source.getSourceId(), source.getSourceName());
            }
        } else if (this == SettingEnum.DEFAULT_VIDEO_SOURCE) {
            for (Source source : SourceUtil.getSourceList()) {
                map.put(source.getSourceId(), source.getSourceName());
            }
        } else if (this == SettingEnum.PRELOAD_NUM) {
            map.put(0, "关闭预加载");
            map.put(5, "预加载5页");
            map.put(10, "预加载10页");
            map.put(10000, "预加载所有");
        } else if (this == SettingEnum.READ_CONTENT) {
            map.put(Data.COMIC_CODE, "漫画");
            map.put(Data.NOVEL_CODE, "小说");
            map.put(Data.VIDEO_CODE, "番剧");
        }
        return map;
    }

    private String getDefaultDesc() {
        Map<Object, String> map = getItemMap();
        String desc = map.get(DEFAULT_VALUE);
        if (desc != null) {
            return desc;
        } else {
            return "";
        }
    }
}
