package com.qc.common.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.PixelFormat;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.Headers;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;
import com.qc.common.MyBaseApplication;
import com.qc.common.en.data.Data;
import com.qc.common.self.ImageConfig;
import com.qc.mycomic.R;
import com.qmuiteam.qmui.util.QMUIDisplayHelper;
import com.qmuiteam.qmui.widget.QMUIProgressBar;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import io.reactivex.android.schedulers.AndroidSchedulers;
import the.one.base.Interface.GlideProgressListener;
import the.one.base.util.glide.GlideProgressInterceptor;
import top.luqichuang.common.model.Content;
import top.luqichuang.common.model.Entity;

/**
 * @author 18472
 * @desc
 * @date 2024/2/25 10:30
 * @ver 1.0
 */
public class ImageUtil {

    private static final Map<String, Integer> STATE_MAP = new HashMap<>();
    private static final Map<String, Integer> PROGRESS_MAP = new HashMap<>();
    private static final Set<String> CACHE_SET = new HashSet<>();
    private static final Queue<ImageConfig> PRELOAD_QUEUE = new LinkedList<>();

    private static final int CACHE_NUM = 5;

    public static final int LOAD_ING = 1;
    public static final int LOAD_SUCCESS = 2;
    public static final int LOAD_FAIL = 3;

    private static final int MATCH_PARENT = ViewGroup.LayoutParams.MATCH_PARENT;
    private static final int WRAP_CONTENT = ViewGroup.LayoutParams.WRAP_CONTENT;

    private static Context getContext() {
        return MyBaseApplication.getInstance();
    }

    public static ImageConfig getDefaultConfig(String url, RelativeLayout layout) {
        ImageConfig config = new ImageConfig(url, layout);
        config.setDefaultBitmapId(R.drawable.ic_image_none);
        config.setErrorBitmapId(R.drawable.ic_image_none);
        config.setDrawableId(R.drawable.ic_image_background);
        config.setStartScaleType(ImageView.ScaleType.FIT_XY);
        config.setReadyScaleType(ImageView.ScaleType.FIT_XY);
        config.setOverScaleType(ImageView.ScaleType.FIT_XY);
        config.setWidth(MATCH_PARENT);
        config.setHeight(MATCH_PARENT);
        return config;
    }

    public static ImageConfig getReaderConfig(String url, RelativeLayout layout) {
        ImageConfig config = new ImageConfig(url, layout);
        config.setDefaultBitmapId(0);
        config.setErrorBitmapId(R.drawable.ic_image_error_24);
        config.setDrawableId(R.drawable.ic_image_reader_background);
        config.setStartScaleType(ImageView.ScaleType.CENTER);
        config.setReadyScaleType(ImageView.ScaleType.FIT_XY);
        config.setOverScaleType(ImageView.ScaleType.FIT_XY);
        config.setWidth(getScreenWidth());
        config.setHeight(QMUIDisplayHelper.dp2px(getContext(), 300));
        return config;
    }

    private static boolean initLayout(ImageConfig config, String url) {
        RelativeLayout layout = config.getLayout();
        if (layout != null) {
            if (config.getImageView() == null) {
                config.setImageView(layout.findViewById(R.id.imageView));
            }
            if (config.getProgressBar() == null) {
                config.setProgressBar(layout.findViewById(R.id.progressBar));
            }
            if (config.getTextView() == null) {
                config.setTextView(layout.findViewById(R.id.textView));
            }
            config.getImageView().setTag(url);
            config.getProgressBar().setTag(url);
            config.getTextView().setTag(url);
            config.getProgressBar().setVisibility(View.INVISIBLE);
            config.getTextView().setVisibility(View.INVISIBLE);
            return true;
        }
        return false;
    }

    public static void loadImage(ImageConfig config) {
        loadImage(config, config.getUrl());
    }

    public static void loadImage(ImageConfig config, String url) {
        if (!initLayout(config, url)) {
            return;
        }
        if (url == null || url.isEmpty()) {
            setLP(config.getLayout(), config.getWidth(), config.getHeight());
            setLP(config.getImageView(), config.getWidth(), config.getHeight());
            config.getImageView().setImageBitmap(getBitmap(config.getErrorBitmapId()));
            config.getImageView().setScaleType(config.getStartScaleType());
            return;
        }
        if (Glide.with(getContext()).isPaused()) {
            Glide.with(getContext()).onStart();
        }
        config.setUrl(url);
        if (!config.isForce() && loadImageLocal(config)) {
            return;
        }
        loadImageNet(config, url);
        config.setForce(false);
    }

    public static boolean loadImageLocal(ImageConfig config) {
        if (config.isSave() && config.getSaveKey() != null) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            File file = new File(getLocalImgUrl(config.getSaveKey()));
            if (file.exists()) {
                Bitmap bitmap = BitmapFactory.decodeFile(file.getPath(), options);
                if (bitmap != null) {
                    setLP(config.getLayout(), WRAP_CONTENT, WRAP_CONTENT);
                    setLP(config.getImageView(), bitmap);
                    config.getImageView().setImageBitmap(bitmap);
                    config.getImageView().setScaleType(config.getReadyScaleType());
                    return true;
                }
            }
        }
        return false;
    }

    public static void loadImageNet(ImageConfig config, String url) {
        GlideUrl glideUrl = getGlideUrl(config, url);
        addInterceptor(config, url);

        RelativeLayout layout = config.getLayout();
        ImageView imageView = config.getImageView();
        QMUIProgressBar progressBar = config.getProgressBar();
        TextView textView = config.getTextView();

        Glide.with(getContext())
                .asBitmap()
                .load(glideUrl)
                .dontAnimate()
                .priority(config.isForce() ? Priority.IMMEDIATE : Priority.HIGH)
                .addListener(getRequestListener(config, url))
                .into(new CustomTarget<Bitmap>() {
                    @Override
                    public void onLoadStarted(@Nullable Drawable placeholder) {
                        if (url.equals(imageView.getTag())) {
                            if (config.getDefaultBitmapId() != 0) {
                                imageView.setImageBitmap(getBitmap(config.getDefaultBitmapId()));
                                imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                            } else {
                                imageView.setImageBitmap(null);
                                imageView.setBackground(getDrawable(config.getDrawableId()));
                                imageView.setScaleType(config.getStartScaleType());
                            }
                            setLP(layout, config.getWidth(), config.getHeight());
                            setLP(imageView, config.getWidth(), config.getWidth());
                        }
                        if (url.equals(progressBar.getTag())) {
                            progressBar.setVisibility(View.VISIBLE);
                            textView.setVisibility(View.VISIBLE);
                            int progress = getProgress(url);
                            String text = progress + "%";
                            AndroidSchedulers.mainThread().scheduleDirect(() -> {
                                progressBar.setProgress(progress, false);
                                textView.setText(text);
                            });
                        }
                        STATE_MAP.put(url, LOAD_ING);
                    }

                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        if (url.equals(imageView.getTag())) {
                            setLP(layout, WRAP_CONTENT, WRAP_CONTENT);
                            setLP(imageView, resource);
                            imageView.setImageBitmap(resource);
                            if (config.getReadyScaleType() != config.getOverScaleType()) {
                                if (imageView.getLayoutParams().height + QMUIDisplayHelper.getActionBarHeight(getContext()) > QMUIDisplayHelper.getScreenHeight(getContext())) {
                                    imageView.setScaleType(config.getOverScaleType());
                                } else {
                                    imageView.setScaleType(config.getReadyScaleType());
                                }
                            } else {
                                imageView.setScaleType(config.getReadyScaleType());
                            }
                        }
                        if (url.equals(progressBar.getTag())) {
                            progressBar.setVisibility(View.INVISIBLE);
                            textView.setVisibility(View.INVISIBLE);
                        }
                        if (config.isSave() && config.getSaveKey() != null) {
                            saveBitmapBackPath(resource, config.getSaveKey());
                        }
                        STATE_MAP.put(url, LOAD_SUCCESS);
                    }

                    @Override
                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
                        if (url.equals(imageView.getTag())) {
                            setLP(layout, config.getWidth(), config.getHeight());
                            setLP(imageView, config.getWidth(), config.getHeight());
                            imageView.setImageBitmap(getBitmap(config.getErrorBitmapId()));
                            imageView.setScaleType(config.getStartScaleType());
                        }
                        if (url.equals(progressBar.getTag())) {
                            progressBar.setVisibility(View.INVISIBLE);
                            textView.setVisibility(View.INVISIBLE);
                        }
                        STATE_MAP.put(url, LOAD_FAIL);
                    }

                    @Override
                    public void onLoadCleared(@Nullable Drawable placeholder) {
                        onLoadFailed(placeholder);
                    }
                });
    }

    public static void preloadImage(ImageConfig config) {
        if (config != null) {
            PRELOAD_QUEUE.offer(config);
            preloadImageStart();
        }
    }

    public static void clearCacheMap() {
        PROGRESS_MAP.clear();
        STATE_MAP.clear();
        CACHE_SET.clear();
        PRELOAD_QUEUE.clear();
        Glide.with(MyBaseApplication.getInstance()).pauseAllRequests();
    }

    /**
     * 根据url获得图片加载状态
     *
     * @param content content
     * @return int
     */
    public static int getLoadStatus(Content content) {
        if (content != null) {
            String url = content.getUrl();
            Integer status = STATE_MAP.get(url);
            if (status != null) {
                return status;
            }
        }
        return LOAD_ING;
    }

    public static void setSaveKey(Entity entity, ImageConfig config) {
        if (entity.getInfo().getSourceId() == 0) {
            config.setSaveKey(null);
        } else {
            if (Data.contentCode == Data.COMIC_CODE) {
                config.setSaveKey("C_" + entity.getTitle() + "_" + entity.getSourceId());
            } else if (Data.contentCode == Data.NOVEL_CODE) {
                config.setSaveKey("N_" + entity.getTitle() + "_" + entity.getSourceId());
            } else {
                config.setSaveKey("V_" + entity.getTitle() + "_" + entity.getSourceId());
            }
        }
    }

    public static void pauseLoad(Context context) {
        Glide.with(context).pauseRequests();
    }

    public static void resumeLoad(Context context) {
        Glide.with(context).resumeRequests();
    }

    /*=============================================================================*/
    private static int getProgress(String url) {
        Integer progress = PROGRESS_MAP.get(url);
        if (progress == null) {
            return 0;
        } else {
            return progress;
        }
    }

    private static void addInterceptor(ImageConfig config, String url) {
        GlideProgressInterceptor.addListener(url, new GlideProgressListener() {
            @Override
            public void onProgress(int progress, boolean success) {
                progress = Math.max(progress, 1);
                progress = Math.min(progress, 99);
                PROGRESS_MAP.put(url, progress);
                if (config.getProgressBar() == null) {
                    return;
                }
                if (url.equals(config.getProgressBar().getTag())) {
                    config.getProgressBar().setVisibility(View.VISIBLE);
                    config.getTextView().setVisibility(View.VISIBLE);
                    int num = progress;
                    String text = num + "%";
                    AndroidSchedulers.mainThread().scheduleDirect(() -> {
                        config.getProgressBar().setProgress(num);
                        config.getTextView().setText(text);
                    });
                }
            }
        });
    }

    private static GlideUrl getGlideUrl(ImageConfig config, String url) {
        return new GlideUrl(url, () -> {
            if (config.getHeaders() != null) {
                return config.getHeaders();
            } else {
                return Headers.DEFAULT.getHeaders();
            }
        });
    }

    private static <T> RequestListener<T> getRequestListener(ImageConfig config, String url) {
        return new RequestListener<T>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<T> target, boolean isFirstResource) {
                onLoadEnd();
                return false;
            }

            @Override
            public boolean onResourceReady(T resource, Object model, Target<T> target, DataSource dataSource, boolean isFirstResource) {
                onLoadEnd();
                return false;
            }

            private void onLoadEnd() {
                PROGRESS_MAP.remove(url);
                GlideProgressInterceptor.removeListener(url);
                if (!CACHE_SET.isEmpty()) {
                    CACHE_SET.remove(url);
                    PRELOAD_QUEUE.remove(config);
                    preloadImageStart();
                }
            }
        };
    }

    private static void preloadImageStart() {
        if (!PRELOAD_QUEUE.isEmpty() && CACHE_SET.size() < CACHE_NUM) {
            ImageConfig config = PRELOAD_QUEUE.poll();
            if (config != null) {
                CACHE_SET.add(config.getUrl());
                addInterceptor(config, config.getUrl());
                GlideUrl glideUrl = getGlideUrl(config, config.getUrl());
                Glide.with(getContext())
                        .load(glideUrl)
                        .priority(Priority.NORMAL)
                        .addListener(getRequestListener(config, config.getUrl()))
                        .preload();
            }
        }
    }

    /*=============================================================================*/

    private static void setLP(View view, Bitmap bitmap) {
        ViewGroup.LayoutParams lp = view.getLayoutParams();
        int bWidth = bitmap.getWidth();
        int bHeight = bitmap.getHeight();
        int sWidth = getScreenWidth();
        int sHeight = bHeight * sWidth / bWidth;
        lp.width = sWidth;
        lp.height = sHeight;
        view.setLayoutParams(lp);
    }

    private static void setLP(View view, int width, int height) {
        ViewGroup.LayoutParams lp = view.getLayoutParams();
        lp.width = width;
        lp.height = height;
        view.setLayoutParams(lp);
    }

    /**
     * @return int
     * @desc 获取屏幕宽度
     */
    private static int getScreenWidth() {
        return QMUIDisplayHelper.getScreenWidth(getContext());
    }

    /**
     * 保存bitmap到本地并返回地址
     *
     * @param bm  bm
     * @param key key
     * @return String
     */
    private static String saveBitmapBackPath(Bitmap bm, Object key) {
        String path = null;
        File savedFile = DBUtil.createFile(getLocalImgUrl(key));
        BufferedOutputStream bos = null;
        try {
            bos = new BufferedOutputStream(new FileOutputStream(savedFile));
            bm.compress(Bitmap.CompressFormat.JPEG, 80, bos);
            path = savedFile.getAbsolutePath();
        } catch (Exception e) {
            savedFile.delete();
            e.printStackTrace();
        } finally {
            try {
                if (bos != null) {
                    bos.flush();
                    bos.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return path;
    }

    /*=============================================================================*/

    /**
     * 根据key获得本地图片地址
     *
     * @param key key
     * @return String
     */
    public static String getLocalImgUrl(Object key) {
        return Data.getImgPath() + "/img_" + key.toString();
    }

    /**
     * 根据id获得Drawable
     *
     * @param drawableId drawableId
     * @return Drawable
     */
    public static Drawable getDrawable(int drawableId) {
        return ContextCompat.getDrawable(getContext(), drawableId);
    }

    /**
     * 根据id获得Bitmap
     *
     * @param bitmapId bitmapId
     * @return Bitmap
     */
    public static Bitmap getBitmap(int bitmapId) {
        return drawableToBitmap(getDrawable(bitmapId));
    }

    /**
     * Drawable -> Bitmap
     *
     * @param drawable drawable
     * @return Bitmap
     */
    public static Bitmap drawableToBitmap(Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        } else {
            return drawableToBitmapByCanvas(drawable);
        }
    }

    /**
     * Bitmap -> Drawable
     *
     * @param bitmap bitmap
     * @return Drawable
     */
    public static Drawable bitmapToDrawable(Bitmap bitmap) {
        return new BitmapDrawable(getContext().getResources(), bitmap);
    }

    /**
     * Drawable -> Bitmap
     *
     * @param drawable drawable
     * @return Bitmap
     */
    public static Bitmap drawableToBitmapByCanvas(Drawable drawable) {
        int width = drawable.getIntrinsicWidth() > 0 ? drawable.getIntrinsicWidth() : 100;
        int height = drawable.getIntrinsicHeight() > 0 ? drawable.getIntrinsicHeight() : 100;
        Bitmap bitmap = Bitmap.createBitmap(width, height,
                drawable.getOpacity() != PixelFormat.OPAQUE ? Bitmap.Config.ARGB_8888 : Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    /**
     * byte[] -> Bitmap
     *
     * @param bytes      bytes
     * @param isCompress isCompress
     * @return Bitmap
     */
    private static Bitmap bytesToBitmap(byte[] bytes, boolean isCompress) {
        Bitmap bitmap;
        if (isCompress) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            options.inSampleSize = 2;
            bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length, options);
        } else {
            bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
        }
        //Log.i("TAG", "bytesToBitmap: bitmap.cSize = " + bitmap.getByteCount() / 1024 + "KB");
        return bitmap;
    }

    /**
     * 图片压缩
     *
     * @param bitmap bitmap
     * @return Bitmap
     */
    private static Bitmap compressBitmap(Bitmap bitmap) {
        int length = bitmap.getByteCount();
        if (length / 1024 > 2000) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            options.inSampleSize = 2;
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, baos);
            ByteArrayInputStream isBm = new ByteArrayInputStream(baos.toByteArray());
            bitmap = BitmapFactory.decodeStream(isBm, null, options);
        }
        return bitmap;
    }

    /**
     * 计算bitmap大小
     *
     * @param bitmap bitmap
     * @return int
     */
    public static int getBitmapSize(Bitmap bitmap) {
        return bitmap.getByteCount();
    }

}
