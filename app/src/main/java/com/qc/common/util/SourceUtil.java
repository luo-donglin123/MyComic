package com.qc.common.util;


import com.qc.common.en.SettingEnum;
import com.qc.common.en.ValueEnum;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import top.luqichuang.common.model.Content;
import top.luqichuang.common.model.Entity;
import top.luqichuang.common.model.EntityInfo;
import top.luqichuang.common.model.Source;
import top.luqichuang.common.util.MapUtil;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/6/10 15:51
 * @ver 1.0
 */
public class SourceUtil {

    private static Map<Integer, Source> sourceMap;
    private static Map<Integer, String> sourceNameMap;
    private static List<Source> sourceList;
    private static List<String> sourceNameList;

    private static Class<Entity> entityClass;
    private static Class<EntityInfo> infoClass;

    public static void init() {
        sourceMap = ValueEnum.SOURCE_MAP.value();
        sourceNameMap = new LinkedHashMap<>();
        sourceList = new ArrayList<>();
        sourceNameList = new ArrayList<>();
        entityClass = ValueEnum.CLASS_ENTITY.value();
        infoClass = ValueEnum.CLASS_ENTITY_INFO.value();
        for (Source source : sourceMap.values()) {
            sourceNameMap.put(source.getSourceId(), source.getSourceName());
            sourceList.add(source);
            sourceNameList.add(source.getSourceName());
        }

        //设置已开启数据源
        Set<Integer> openSet = new LinkedHashSet<>(((SettingEnum) ValueEnum.SE_SOURCE_OPEN.value()).value());
        Set<Integer> totalSet = new LinkedHashSet<>(((SettingEnum) ValueEnum.SE_SOURCE_TOTAL.value()).value());
        Set<Integer> allSet = new LinkedHashSet<>(SourceUtil.getSourceMap().keySet());
        if (totalSet.size() < allSet.size()) {
            ((SettingEnum) ValueEnum.SE_SOURCE_TOTAL.value()).setValue(new LinkedHashSet<>(allSet));
            allSet.removeAll(totalSet);
            if (!allSet.isEmpty()) {
                openSet.addAll(allSet);
                ((SettingEnum) ValueEnum.SE_SOURCE_OPEN.value()).setValue(new LinkedHashSet<>(openSet));
            }
        }
        //调整数据源为已开启数据源
        List<Source> sourceList = SourceUtil.getSourceList();
        Iterator<Source> iterator = sourceList.iterator();
        while (iterator.hasNext()) {
            int id = iterator.next().getSourceId();
            if (!openSet.contains(id)) {
                iterator.remove();
            }
        }
    }

    public static Source getSource(int sourceId) {
        return sourceMap.get(sourceId);
    }

    public static Source getSource(String sourceName) {
        return getSource(getSourceId(sourceName));
    }

    public static String getSourceName(int sourceId) {
        Source source = getSource(sourceId);
        if (source != null) {
            return source.getSourceName();
        }
        return null;
    }

    public static Integer getSourceId(String name) {
        return MapUtil.getKeyByValue(sourceNameMap, name);
    }

    public static List<Source> getSourceList() {
        return sourceList;
    }

    public static Map<Integer, Source> getSourceMap() {
        return sourceMap;
    }

    public static List<String> getSourceNameList() {
        return sourceNameList;
    }

    public static Class<Entity> getEntityClass() {
        return entityClass;
    }

    public static Class<EntityInfo> getEntityInfoClass() {
        return infoClass;
    }

    public static int size() {
        return sourceList.size();
    }

    public static EntityInfo getInfo() {
        try {
            return infoClass.newInstance();
        } catch (Exception e) {
            return null;
        }
    }

    public static Entity getEntity() {
        try {
            return entityClass.newInstance();
        } catch (Exception e) {
            return null;
        }
    }

    public static Entity getEntity(EntityInfo info) {
        try {
            Entity entity = entityClass.newInstance();
            entity.setSourceId(info.getSourceId());
            entity.setTitle(info.getTitle());
            entity.setInfo(info);
            entity.getInfoList().add(info);
            return entity;
        } catch (Exception e) {
            return null;
        }
    }

    /*--------------------------------------------------------------------------------------------*/
    public static List<EntityInfo> getInfoList(Source source, String html) {
        try {
            return source.getInfoList(html);
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    public static void setInfoDetail(Source source, EntityInfo info, String html, Map<String, Object> map) {
        try {
            source.setInfoDetail(info, html, map);
        } catch (Exception ignored) {
        }
    }

    public static List<Content> getContentList(Source source, String html, int chapterId, Map<String, Object> map) {
        try {
            return source.getContentList(html, chapterId, map);
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    public static List<EntityInfo> getRankInfoList(Source source, String html) {
        try {
            return source.getRankInfoList(html);
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

}
