package com.qc.common.ui.adapter;

import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.qc.common.self.ImageConfig;
import com.qc.mycomic.R;
import com.qmuiteam.qmui.util.QMUIDisplayHelper;
import com.qmuiteam.qmui.widget.roundwidget.QMUIRoundButtonDrawable;
import com.qmuiteam.qmui.widget.roundwidget.QMUIRoundLinearLayout;

import the.one.base.adapter.TheBaseViewHolder;
import top.luqichuang.common.model.Content;

public class ComicReaderAdapter2 extends ComicReaderAdapter {

    @Override
    protected void convert(@NonNull TheBaseViewHolder holder, Content content) {
        QMUIRoundLinearLayout linearLayout = holder.getView(R.id.linearLayout);
        if (linearLayout.getTag() == null) {
            QMUIRoundButtonDrawable drawable = (QMUIRoundButtonDrawable) linearLayout.getBackground();
            drawable.setStrokeData(0, null);
            int paddingTop = QMUIDisplayHelper.getActionBarHeight(getContext());
            linearLayout.setPadding(0, paddingTop, 0, 0);
            linearLayout.setTag("init");
        }
        super.convert(holder, content);
    }

    @Override
    protected void initConfig(ImageConfig config) {
        super.initConfig(config);
        config.setOverScaleType(ImageView.ScaleType.CENTER_INSIDE);
    }
}
