package com.qc.common.ui.fragment;

import java.util.List;

import top.luqichuang.common.model.Content;
import top.luqichuang.common.util.StringUtil;

/**
 * @author LuQiChuang
 * @desc
 * @date 2022/11/1 19:09
 * @ver 1.0
 */
public class ComicReaderFragment3 extends ComicReaderFragment2 {

    @Override
    protected int getSeekBarPosition(int progress) {
        return contentList.size() - 1 - progress;
    }

    @Override
    public void onComplete(List<Content> data) {
        StringUtil.swapList(data);
        recycleView.scrollToPosition(data.size() - 1 - chapterNum);
        super.onComplete(data);
        adapter.getLoadMoreModule().setEnableLoadMore(false);
    }
}
