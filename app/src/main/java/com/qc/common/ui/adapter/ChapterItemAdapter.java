package com.qc.common.ui.adapter;

import android.content.res.ColorStateList;

import com.qc.common.en.data.Data;
import com.qc.mycomic.R;
import com.qmuiteam.qmui.widget.roundwidget.QMUIRoundButtonDrawable;
import com.qmuiteam.qmui.widget.roundwidget.QMUIRoundLinearLayout;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import the.one.base.adapter.TheBaseQuickAdapter;
import the.one.base.adapter.TheBaseViewHolder;
import top.luqichuang.common.model.ChapterInfo;
import top.luqichuang.common.model.Entity;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/6/24 16:20
 * @ver 1.0
 */
public class ChapterItemAdapter extends TheBaseQuickAdapter<ChapterInfo> {

    private final Entity entity;

    public ChapterItemAdapter() {
        super(Data.isGrid ? R.layout.item_chapter : R.layout.item_reader_list);
        this.entity = Data.getEntity();
    }

    @Override
    protected void convert(@NotNull TheBaseViewHolder holder, ChapterInfo chapterInfo) {
        holder.setText(R.id.tvTitle, chapterInfo.getTitle());
        QMUIRoundLinearLayout linearLayout = holder.getView(R.id.linearLayout);
        QMUIRoundButtonDrawable drawable = (QMUIRoundButtonDrawable) linearLayout.getBackground();
        if (Objects.equals(chapterInfo.getId(), entity.getInfo().getCurChapterId())) {
            holder.setTextColor(R.id.tvTitle, getColor(R.color.white));
            drawable.setBgData(ColorStateList.valueOf(getColor(R.color.blue)));
        } else {
            holder.setTextColor(R.id.tvTitle, getColor(R.color.black));
            drawable.setBgData(ColorStateList.valueOf(getColor(R.color.white)));
        }
    }
}
