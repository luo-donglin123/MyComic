package com.qc.common.ui.fragment;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.qc.common.en.SettingEnum;
import com.qc.common.en.data.Data;
import com.qc.common.self.ImageConfig;
import com.qc.common.ui.adapter.ComicReaderAdapter;
import com.qc.common.util.ImageUtil;
import com.qc.mycomic.R;
import com.qc.mycomic.ui.fragment.ReaderDetailFragment;

import the.one.base.widge.TheCheckBox;
import top.luqichuang.common.model.Content;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/6/10 19:09
 * @ver 1.0
 */
public class ComicReaderFragment extends BaseReaderFragment {

    private ComicReaderAdapter comicReaderAdapter = new ComicReaderAdapter();

    @Override
    protected int getSettingsViewId() {
        return R.layout.fragment_reader_settings;
    }

    @Override
    protected void setSettingsView(View settingsView) {
        LinearLayout llSettingsContent = settingsView.findViewById(R.id.llSettingsContent);
        llSettingsContent.setOnClickListener(v -> {
        });

        LinearLayout llCancel = settingsView.findViewById(R.id.llCancel);
        llCancel.setOnClickListener(v -> {
            hideView(settingsView);
        });

        LinearLayout llFull = settingsView.findViewById(R.id.llFull);
        TheCheckBox checkBox = llFull.findViewById(R.id.checkBox);
        checkBox.setIsCheckDrawable(R.drawable.ic_baseline_check_circle_24);
        checkBox.setCheck(Data.isFull);
        checkBox.setOnClickListener(v -> {
            Data.isFull = !checkBox.isCheck();
            SettingEnum.IS_FULL_SCREEN.setValue(Data.isFull);
            checkBox.setCheck(Data.isFull);
        });
        llFull.setOnClickListener(v -> {
            Data.isFull = !checkBox.isCheck();
            SettingEnum.IS_FULL_SCREEN.setValue(Data.isFull);
            checkBox.setCheck(Data.isFull);
        });

        TheCheckBox checkBox0 = settingsView.findViewById(R.id.checkBoxReadMode0);
        TheCheckBox checkBox1 = settingsView.findViewById(R.id.checkBoxReadMode1);
        TheCheckBox checkBox2 = settingsView.findViewById(R.id.checkBoxReadMode2);
        checkBox0.setIsCheckDrawable(R.drawable.ic_baseline_check_circle_24);
        checkBox1.setIsCheckDrawable(R.drawable.ic_baseline_check_circle_24);
        checkBox2.setIsCheckDrawable(R.drawable.ic_baseline_check_circle_24);
        int readerMode = SettingEnum.READER_MODE.value();
        if (readerMode == Data.READER_MODE_V) {
            checkBox0.setCheck(true);
            checkBox1.setCheck(false);
            checkBox2.setCheck(false);
        } else if (readerMode == Data.READER_MODE_H_R) {
            checkBox0.setCheck(false);
            checkBox1.setCheck(true);
            checkBox2.setCheck(false);
        } else if (readerMode == Data.READER_MODE_H_L) {
            checkBox0.setCheck(false);
            checkBox1.setCheck(false);
            checkBox2.setCheck(true);
        }
        checkBox0.setOnClickListener(v -> {
            if (!checkBox0.isCheck()) {
                checkBox0.setCheck(true);
                checkBox1.setCheck(false);
                checkBox2.setCheck(false);
                SettingEnum.READER_MODE.setValue(Data.READER_MODE_V);
                finish();
                startFragment(new ComicReaderFragment());
            }
        });
        checkBox1.setOnClickListener(v -> {
            if (!checkBox1.isCheck()) {
                checkBox0.setCheck(false);
                checkBox1.setCheck(true);
                checkBox2.setCheck(false);
                SettingEnum.READER_MODE.setValue(Data.READER_MODE_H_R);
                finish();
                startFragment(new ComicReaderFragment2());
            }
        });
        checkBox2.setOnClickListener(v -> {
            if (!checkBox2.isCheck()) {
                checkBox0.setCheck(false);
                checkBox1.setCheck(false);
                checkBox2.setCheck(true);
                SettingEnum.READER_MODE.setValue(Data.READER_MODE_H_L);
                finish();
                startFragment(new ComicReaderFragment3());
            }
        });
    }

    @Override
    protected void firstLoadView() {

    }

    @Override
    protected BaseQuickAdapter getAdapter() {
        return comicReaderAdapter;
    }

    @Override
    public boolean onItemLongClick(@NonNull BaseQuickAdapter adapter, @NonNull View view, int position) {
        Content content = (Content) adapter.getData().get(position);
        if (ImageUtil.getLoadStatus(content) == ImageUtil.LOAD_SUCCESS) {
            Data.setContent(content);
            setFullScreen(false);
            startFragment(new ReaderDetailFragment());
        } else {
            RelativeLayout layout = view.findViewById(R.id.imageRelativeLayout);
            ImageConfig config = comicReaderAdapter.getImageConfig(layout, content);
            config.setForce(true);
            ImageUtil.loadImage(config);
        }
        return true;
    }
}
