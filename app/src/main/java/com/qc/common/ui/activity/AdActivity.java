package com.qc.common.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.appcompat.app.AppCompatActivity;

import com.qc.common.MyBaseApplication;
import com.qc.common.en.data.Data;
import com.qc.common.ui.presenter.UpdatePresenter;
import com.qc.common.util.DBUtil;
import com.qc.common.util.EntityUtil;
import com.qc.common.util.SourceUtil;
import com.qc.common.util.VersionUtil;
import com.qc.mycomic.R;
import com.superad.ad_lib.ADInitListener;
import com.superad.ad_lib.ADManage;
import com.superad.ad_lib.SuperSplashAD;
import com.superad.ad_lib.SuperSplashADListener;

import org.litepal.LitePal;

import java.io.File;

import the.one.base.util.ToastUtil;

public class AdActivity extends AppCompatActivity {

    ViewGroup splash_container;
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_ad);
        mContext = this;

        splash_container = findViewById(R.id.splash_container);

        doSomeThing();
        initAd();
//        loadMain();
    }

    private void doSomeThing() {
        //初始化软件版本号
        VersionUtil.initVersion(this);

        //初始化数据库
        LitePal.initialize(MyBaseApplication.getInstance());
        SourceUtil.init();
        EntityUtil.init();

        //设置文件存储路径
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
            Data.appPath = Data.SD_CARD_PATH + Data.DIR_APP_NAME;
        } else {
            File file = getExternalCacheDir();
            if (file != null) {
                Data.appPath = file.getParent();
            }
        }
        //自动备份
        DBUtil.autoBackup(this);

        //检查软件更新
        new Thread(() -> {
            new UpdatePresenter().checkApkUpdate();
        }).start();
    }

    protected void initAd() {
        new ADManage().initSDK(this, "1557347845024256002", new ADInitListener() {
            @Override
            public void onSuccess() {
//                Log.e("ad_init", "成功");
                loadAd();
            }

            @Override
            public void onError(int code, String message) {
//                Log.e("ad_init", "失败");
                ToastUtil.show("广告加载失败");
                loadMain();
            }
        });
    }

    protected void loadAd() {
        SuperSplashAD ad = new SuperSplashAD(mContext, splash_container, new SuperSplashADListener() {
            @Override
            public void onError(Object error) {
                ToastUtil.showLongToast("广告加载失败");
                onADDismissed();
            }

            @Override
            public void onAdLoad() {

            }

            @Override
            public void onADShow() {

            }

            @Override
            public void onADClicked() {

            }

            @Override
            public void onADDismissed() {
                splash_container.removeAllViews();
                splash_container.setVisibility(View.GONE);
                loadMain();
            }

            @Override
            public void onAdTypeNotSupport() {

            }
        });
    }

    protected void loadMain() {
        Intent intent = new Intent(AdActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}