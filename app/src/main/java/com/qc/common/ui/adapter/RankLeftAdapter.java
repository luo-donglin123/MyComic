package com.qc.common.ui.adapter;

import android.content.res.ColorStateList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.qc.mycomic.R;
import com.qmuiteam.qmui.widget.roundwidget.QMUIRoundButtonDrawable;
import com.qmuiteam.qmui.widget.roundwidget.QMUIRoundLinearLayout;

import java.util.List;

import the.one.base.adapter.TheBaseQuickAdapter;
import the.one.base.adapter.TheBaseViewHolder;

/**
 * @author LuQiChuang
 * @desc
 * @date 2020/8/12 15:25
 * @ver 1.0
 */
public class RankLeftAdapter extends TheBaseQuickAdapter<String> {

    private int position = 0;

    public RankLeftAdapter(int layoutResId, @Nullable List<String> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(@NonNull TheBaseViewHolder holder, String s) {
        holder.setText(R.id.textView, s);
        QMUIRoundLinearLayout linearLayout = holder.getView(R.id.linearLayout);
        QMUIRoundButtonDrawable drawable = (QMUIRoundButtonDrawable) linearLayout.getBackground();
        if (position == getItemPosition(s)) {
            holder.setTextColor(R.id.textView, getColor(R.color.white));
            drawable.setBgData(ColorStateList.valueOf(getColor(R.color.blue)));
        } else {
            holder.setTextColor(R.id.textView, getColor(R.color.black));
            drawable.setBgData(ColorStateList.valueOf(getColor(R.color.white)));
        }
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }
}
