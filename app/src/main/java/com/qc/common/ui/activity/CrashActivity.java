package com.qc.common.ui.activity;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.qc.common.MyBaseApplication;

import the.one.base.ui.activity.BaseCrashActivity;

/**
 * @author LuQiChuang
 * @desc
 * @date 2022/10/28 11:05
 * @ver 1.0
 */
public class CrashActivity extends BaseCrashActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyBaseApplication.addActivity(this);
    }
}
