package com.qc.common.ui.presenter;


import com.qc.common.ui.view.RankView;
import com.qc.common.util.DBUtil;

import okhttp3.Request;
import top.luqichuang.common.model.CacheData;
import top.luqichuang.common.model.Source;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/6/11 11:46
 * @ver 1.0
 */
public class RankPresenter extends SourcePresenter<RankView> {

    private Request curRequest;

    public void load(String url, int page, Source source) {
        newTread(() -> {
            String loadUrl;
            if (url.contains("%d")) {
                loadUrl = String.format(url, page);
            } else {
                loadUrl = url;
            }
            curRequest = source.getRankRequest(loadUrl);
            startLoad(curRequest, source, Source.RANK);
        });
    }

    public void clearCache(String url, Source source) {
        if (url == null) {
            return;
        }
        if (url.contains("%d")) {
            int i = 1;
            while (true) {
                Request request = source.getRankRequest(String.format(url, i++));
                CacheData cacheData = DBUtil.findCache(request.toString());
                if (cacheData != null) {
                    DBUtil.deleteCache(request.toString());
                } else {
                    break;
                }
            }
        } else {
            DBUtil.deleteCache(source.getRankRequest(url).toString());
        }
    }

    @Override
    public void loadSourceEnd(String errorMsg, String json, String tag, Request request) {
        if (request != null && request != curRequest) {
            return;
        }
        super.loadSourceEnd(errorMsg, json, tag, request);
    }
}