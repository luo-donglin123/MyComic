package com.qc.common.ui.adapter;

import android.widget.RelativeLayout;

import com.qc.common.self.ImageConfig;
import com.qc.common.util.ImageUtil;
import com.qc.common.util.SourceUtil;
import com.qc.mycomic.R;

import org.jetbrains.annotations.NotNull;

import the.one.base.adapter.TheBaseQuickAdapter;
import the.one.base.adapter.TheBaseViewHolder;
import top.luqichuang.common.model.Entity;
import top.luqichuang.common.model.Source;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/6/9 18:43
 * @ver 1.0
 */
public class ShelfAdapter extends TheBaseQuickAdapter<Entity> {

    public ShelfAdapter() {
        super(R.layout.item_shelf);
    }

    @Override
    protected void convert(@NotNull TheBaseViewHolder holder, Entity entity) {
        holder.setText(R.id.tvTitle, entity.getTitle());
        if (entity.getInfo().getCurChapterTitle() == null) {
            holder.setText(R.id.tvCurChapter, "阅读至：未阅读");
        } else {
            holder.setText(R.id.tvCurChapter, "阅读至：" + entity.getInfo().getCurChapterTitle());
        }
        if (entity.getInfo().getUpdateChapter() == null) {
            holder.setText(R.id.tvChapter, "更新至：未知");
        } else {
            holder.setText(R.id.tvChapter, "更新至：" + entity.getInfo().getUpdateChapter());
        }
        holder.setGone(R.id.llTextUpdate, !entity.isUpdate());
        RelativeLayout layout = holder.getView(R.id.imageRelativeLayout);
        if (layout.getTag() == null) {
            ImageConfig config = ImageUtil.getDefaultConfig(entity.getInfo().getImgUrl(), layout);
            config.setSave(true);
            ImageUtil.setSaveKey(entity, config);
            Source source = SourceUtil.getSource(entity.getSourceId());
            config.setHeaders(source.getImageHeaders());
            layout.setTag(config);
        }
        ImageConfig config = (ImageConfig) layout.getTag();
        config.setUrl(entity.getInfo().getImgUrl());
        ImageUtil.setSaveKey(entity, config);
        ImageUtil.loadImage(config);
    }

}
