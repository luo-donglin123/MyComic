package com.qc.common.ui.adapter;

import android.widget.RelativeLayout;

import com.qc.common.self.ImageConfig;
import com.qc.common.util.EntityUtil;
import com.qc.common.util.ImageUtil;
import com.qc.common.util.SourceUtil;
import com.qc.mycomic.R;

import org.jetbrains.annotations.NotNull;

import the.one.base.adapter.TheBaseQuickAdapter;
import the.one.base.adapter.TheBaseViewHolder;
import top.luqichuang.common.model.Entity;
import top.luqichuang.common.model.Source;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/6/11 15:33
 * @ver 1.0
 */
public class SearchAdapter extends TheBaseQuickAdapter<Entity> {

    public SearchAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(@NotNull TheBaseViewHolder holder, Entity entity) {
        holder.setText(R.id.tvTitle, entity.getInfo().getTitle());
        holder.setText(R.id.tvSource, "数据源数量：" + EntityUtil.sourceSize(entity));
        holder.setText(R.id.tvAuthor, entity.getInfo().getAuthor() != null ? entity.getInfo().getAuthor() : "作者未知");
        holder.setText(R.id.tvUpdateTime, entity.getInfo().getUpdateTime());
        holder.setText(R.id.tvUpdateChapter, entity.getInfo().getUpdateChapter());
        RelativeLayout layout = holder.getView(R.id.imageRelativeLayout);
        if (layout.getTag() == null) {
            ImageConfig config = ImageUtil.getDefaultConfig(entity.getInfo().getImgUrl(), layout);
            Source source = SourceUtil.getSource(entity.getSourceId());
            config.setHeaders(source.getImageHeaders());
            layout.setTag(config);
        }
        ImageConfig config = (ImageConfig) layout.getTag();
        config.setUrl(entity.getInfo().getImgUrl());
        ImageUtil.loadImage(config);
    }
}
