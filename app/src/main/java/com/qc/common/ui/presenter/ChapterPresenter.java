package com.qc.common.ui.presenter;

import com.qc.common.ui.view.ChapterView;
import com.qc.common.util.SourceUtil;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import okhttp3.Request;
import top.luqichuang.common.model.Entity;
import top.luqichuang.common.model.EntityInfo;
import top.luqichuang.common.model.Source;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/6/10 17:52
 * @ver 1.0
 */
public class ChapterPresenter extends SourcePresenter<ChapterView> {

    private Request curRequest;

    public void load(Entity entity) {
        newTread(() -> {
            Source source = SourceUtil.getSource(entity.getSourceId());
            curRequest = source.getDetailRequest(entity.getInfo().getDetailUrl());
            startLoad(curRequest, source, Source.DETAIL);
        });
    }

    public void updateSource(Entity entity) {
        newTread(() -> {
            List<Source> sourceList = SourceUtil.getSourceList();
            Set<Integer> set = new HashSet<>();
            for (EntityInfo info : entity.getInfoList()) {
                set.add(info.getSourceId());
            }
            for (Source source : sourceList) {
                if (set.contains(source.getSourceId())) {
                    loadSourceEnd(null, null, Source.SEARCH);
                } else {
                    Request request = source.getSearchRequest(entity.getTitle());
                    startLoad(request, source, Source.SEARCH);
                }
            }
        });
    }

    @Override
    public void loadSourceEnd(String errorMsg, String json, String tag, Request request) {
        if (Source.DETAIL.equals(tag) && request != null && request != curRequest) {
            return;
        }
        super.loadSourceEnd(errorMsg, json, tag, request);
    }
}
