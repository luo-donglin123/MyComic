package com.qc.common.ui.fragment;

import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.qc.common.en.data.Text;
import com.qc.common.util.SourceUtil;
import com.qc.mycomic.R;
import com.qmuiteam.qmui.qqface.QMUIQQFaceView;

import java.util.ArrayList;
import java.util.List;

import the.one.base.ui.fragment.BaseFragment;
import the.one.base.ui.fragment.BaseTitleTabFragment;
import top.luqichuang.common.model.Source;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/6/11 11:23
 * @ver 1.0
 */
public class SearchBaseFragment extends BaseTitleTabFragment {

    private List<Source> sourceList;

    @Override
    protected boolean showElevation() {
        return true;
    }

    @Override
    protected int getContentViewId() {
        return R.layout.layout_title_tab;
    }

    @Override
    protected void initView(View rootView) {
        super.initView(rootView);
        showLoadingPage();
        QMUIQQFaceView mTitle = mTopLayout.setTitle(Text.RANK);
        mTopLayout.setNeedChangedWithTheme(false);
        mTopLayout.setTitleGravity(Gravity.CENTER);
        mTitle.setTextColor(getColor(R.color.qmui_config_color_gray_1));
        mTitle.getPaint().setFakeBoldText(true);

        View view = getView(R.layout.fragment_search_right);
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mTopLayout.addRightView(view, R.id.topbar_right_button1, lp);
        view.setOnClickListener(v -> {
            startFragment(new SearchFragment());
        });
        initList();
        startInit();
    }

    @Override
    protected void onLazyInit() {
    }

    private void initList() {
        sourceList = SourceUtil.getSourceList();
    }

    @Override
    protected void addTabs() {
        for (Source source : sourceList) {
            if (source.isValid() && source.getRankMap() != null) {
                addTab(source.getSourceName());
            }
        }
    }

    @Override
    protected void addFragment(ArrayList<BaseFragment> fragments) {
        for (Source source : sourceList) {
            if (source.isValid() && source.getRankMap() != null) {
                fragments.add(RankFragment.getInstance(source.getSourceId()));
            }
        }
    }
}