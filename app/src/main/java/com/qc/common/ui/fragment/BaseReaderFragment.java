package com.qc.common.ui.fragment;

import android.annotation.SuppressLint;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.qc.common.en.SettingEnum;
import com.qc.common.en.data.Data;
import com.qc.common.en.data.Text;
import com.qc.common.self.ImageConfig;
import com.qc.common.self.ScrollSpeedLinearLayoutManager;
import com.qc.common.ui.adapter.ReaderListAdapter;
import com.qc.common.ui.presenter.ReaderPresenter;
import com.qc.common.ui.view.ReaderView;
import com.qc.common.util.AnimationUtil;
import com.qc.common.util.DBUtil;
import com.qc.common.util.EntityUtil;
import com.qc.common.util.ImageUtil;
import com.qc.common.util.SourceUtil;
import com.qc.mycomic.R;
import com.qmuiteam.qmui.util.QMUIDisplayHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import okhttp3.Request;
import the.one.base.ui.fragment.BaseListFragment;
import the.one.base.ui.presenter.BasePresenter;
import the.one.base.widge.TheCheckBox;
import top.luqichuang.common.model.ChapterInfo;
import top.luqichuang.common.model.Content;
import top.luqichuang.common.model.Entity;
import top.luqichuang.common.model.EntityInfo;
import top.luqichuang.common.model.Source;
import top.luqichuang.common.util.CacheUtil;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/6/10 22:17
 * @ver 1.0
 */
public abstract class BaseReaderFragment extends BaseListFragment<Content> implements ReaderView {

    protected Entity entity;
    protected EntityInfo entityInfo;
    protected List<ChapterInfo> chapterInfoList;
    protected ReaderPresenter presenter = new ReaderPresenter();
    protected List<Content> contentList = new ArrayList<>();
    protected ReaderListAdapter readerListAdapter;
    protected int curChapterId;

    protected View topView;
    protected View bottomView;
    protected View darkView;
    protected View rightView;
    protected View settingsView;
    private TextView tvChapter;
    private TextView tvProgress;
    private TextView tvInfo;
    private TextView tvChapterName;
    private TextView tvChapterProgress;
    private LinearLayout llLeft;
    private LinearLayout llRight;
    private LinearLayout llList;
    private LinearLayout llDark;
    private LinearLayout llFav;
    private LinearLayout llSettings;
    private LinearLayout llChapter;
    protected SeekBar seekBar;

    protected int chapterNum;
    protected int first;
    protected int childCount;
    protected int bottomIndex;
    protected int preloadNum = SettingEnum.PRELOAD_NUM.value();
    protected boolean isSmooth = false;
    protected boolean isFullScreen = false;
    protected boolean preloadLock = true;//请求新数据期间禁止预加载
    protected boolean isFollow = false;//拖动滑块时是否让内容跟随变动
    protected boolean smoothScroll = false;//拖动滑块时smooth
    protected float touchX;
    protected float touchY;
    protected int action = MotionEvent.ACTION_UP;

    public BaseReaderFragment() {
        this.entity = Data.getEntity();
        this.entityInfo = entity.getInfo();
        this.chapterInfoList = Data.getChapterInfoList();
        this.curChapterId = -1;
        chapterNum = entityInfo.getChapterNum();
    }

    @Override
    public void onResume() {
        super.onResume();
        goneViews();
    }

    @Override
    protected void initView(View rootView) {
        super.initView(rootView);
        mTopLayout.setVisibility(View.GONE);
        String[] tips = {
                "加载上一章",
                "释放加载",
                "正在加载中"
        };
        pullLayout.setTips(tips);
        addView();
        showLoadingPage();
        setListener();
        firstLoadView();
        requestServer();
    }

    @Override
    protected void initAdapter() {
        super.initAdapter();
        adapter.setAnimationEnable(false);
    }

    @Override
    protected void initRecycleView(RecyclerView recycleView, int type, BaseQuickAdapter adapter) {
        super.initRecycleView(recycleView, type, adapter);
        ScrollSpeedLinearLayoutManager manager = (ScrollSpeedLinearLayoutManager) recycleView.getLayoutManager();
        if (manager != null) {
            manager.initSpeed();
        }
    }

    @Override
    protected void onLazyInit() {
    }

    protected void setFullScreen(boolean isFull) {
        if (isFull && !isFullScreen) {
            isFullScreen = true;
            QMUIDisplayHelper.setFullScreen(_mActivity);
        } else if (!isFull && isFullScreen) {
            isFullScreen = false;
            QMUIDisplayHelper.cancelFullScreen(_mActivity);
        }
    }

    protected void addView() {
        if (topView == null) {
            topView = getView(R.layout.fragment_reader_display);
            tvChapter = topView.findViewById(R.id.tvChapter);
            tvProgress = topView.findViewById(R.id.tvProgress);
            mStatusLayout.addView(topView, 1, getLP());
        }
        if (darkView == null) {
            darkView = getView(R.layout.fragment_dark);
            mStatusLayout.addView(darkView, 2, getLP());
        }
        if (bottomView == null) {
            bottomView = getView(R.layout.fragment_reader_bottom);
            mStatusLayout.addView(bottomView, 3, getLP());
            llLeft = bottomView.findViewById(R.id.llLeft);
            llRight = bottomView.findViewById(R.id.llRight);
            seekBar = bottomView.findViewById(R.id.seekBar);
            llList = bottomView.findViewById(R.id.llList);
            llDark = bottomView.findViewById(R.id.llDark);
            llFav = bottomView.findViewById(R.id.llFav);
            llChapter = bottomView.findViewById(R.id.llChapter);
            llSettings = bottomView.findViewById(R.id.llSettings);
            tvChapterName = bottomView.findViewById(R.id.tvChapterName);
            tvChapterProgress = bottomView.findViewById(R.id.tvChapterProgress);
        }
        if (rightView == null) {
            rightView = getView(R.layout.fragment_reader_list);
            List<ChapterInfo> items = chapterInfoList;
            readerListAdapter = new ReaderListAdapter(R.layout.item_reader_list, items);
            readerListAdapter.setPosition(EntityUtil.getPosition(chapterInfoList, curChapterId, entityInfo.getOrder()));
            RecyclerView listView = rightView.findViewById(R.id.recycleView);
            listView.setLayoutManager(getLayoutManager(TYPE_LIST));
            listView.setAdapter(readerListAdapter);
            listView.scrollToPosition(readerListAdapter.getPosition());
            listView.addItemDecoration(new DividerItemDecoration(_mActivity, DividerItemDecoration.VERTICAL));
            readerListAdapter.setOnItemClickListener(new OnItemClickListener() {
                @Override
                public void onItemClick(@NonNull BaseQuickAdapter<?, ?> adapter, @NonNull View view, int position) {
                    if (readerListAdapter.getPosition() != position) {
                        readerListAdapter.setPosition(position);
                        EntityUtil.initChapterId(entityInfo, EntityUtil.getChapterId(chapterInfoList, position));
                        onFirstLoading();
                    }
                }
            });
            mStatusLayout.addView(rightView, 4, getLP());
            tvInfo = rightView.findViewById(R.id.tvInfo);
            TextView tvTitle = rightView.findViewById(R.id.tvTitle);
            tvTitle.setText(entity.getTitle());
        }
        if (settingsView == null) {
            settingsView = getView(getSettingsViewId());
            mStatusLayout.addView(settingsView, 5, getLP());
        }
    }

    protected abstract int getSettingsViewId();

    protected abstract void setSettingsView(View settingsView);

    protected void setScrollValue(Content content) {
        //章节变化时需要变化的数据
        if (curChapterId != content.getChapterId()) {
            //初始化curChapterId info
            EntityUtil.initChapterId(entityInfo, content.getChapterId());
            curChapterId = content.getChapterId();

            //设置同一章节中不变的view
            tvChapter.setText(entityInfo.getCurChapterTitle());
            tvChapterName.setText(entityInfo.getCurChapterTitle());
            seekBar.setMax(content.getTotal() - 1);
            int num = content.getChapterId() + 1 - EntityUtil.getFirstChapterId(chapterInfoList, entityInfo.getOrder());
            int size = chapterInfoList.size();
            tvInfo.setText(String.format(Locale.CHINA, Text.FORMAT_READER_INFO_CHAPTER, num, size));
            readerListAdapter.setPosition(EntityUtil.getListPositionById(chapterInfoList, curChapterId, entityInfo.getOrder()));

            //保存数据
            DBUtil.saveInfoData(entityInfo);
        }
        //章节不变时需要实时变化的数据
        tvProgress.setText(EntityUtil.toStringProgress(content));
        tvChapterProgress.setText(EntityUtil.toStringProgress(content));
    }

    protected int getSeekBarPosition(int progress) {
        return first - contentList.get(first).getCur() + progress;
    }

    @SuppressLint("ClickableViewAccessibility")
    protected void setListener() {
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            private int position;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (contentList != null && !contentList.isEmpty() && isSmooth) {
                    position = getSeekBarPosition(progress);
                    tvChapterProgress.setText(EntityUtil.toStringProgress(contentList.get(position)));
                    if (isFollow) {
                        smooth();
                    }
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                isSmooth = true;
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                isSmooth = false;
                if (!isFollow) {
                    smooth();
                }
            }

            private void smooth() {
                if (checkPosition(position)) {
                    first = position;
                    if (smoothScroll) {
                        recycleView.smoothScrollToPosition(position);
                    } else {
                        recycleView.scrollToPosition(position);
                    }
                    setScrollValue(contentList.get(first));
                }
            }
        });

        llLeft.setOnClickListener(v -> {
            Content content = contentList.get(first);
            if (content.getTotal() < contentList.size()) {
                for (int i = 0; i < first; i++) {
                    if (content.getChapterId() - 1 == contentList.get(i).getChapterId()) {
                        ((LinearLayoutManager) recycleView.getLayoutManager()).scrollToPositionWithOffset(i, 0);
                        seekBar.setProgress(0);
                        return;
                    }
                }
            }
            int prevId = EntityUtil.getPrevChapterId(entityInfo);
            if (EntityUtil.checkChapterId(chapterInfoList, prevId, entityInfo.getOrder())) {
                EntityUtil.initChapterId(entityInfo, prevId);
                onFirstLoading();
            } else {
                showFailTips(Text.TIP_NO_CHAPTER_PREV);
            }
        });

        llRight.setOnClickListener(v -> {
            Content content = contentList.get(first);
            if (content.getTotal() < contentList.size()) {
                for (int i = first; i < contentList.size(); i++) {
                    if (content.getChapterId() + 1 == contentList.get(i).getChapterId()) {
                        ((LinearLayoutManager) recycleView.getLayoutManager()).scrollToPositionWithOffset(i, 0);
                        seekBar.setProgress(0);
                        return;
                    }
                }
            }
            int nextId = EntityUtil.getNextChapterId(entityInfo);
            if (EntityUtil.checkChapterId(chapterInfoList, nextId, entityInfo.getOrder())) {
                EntityUtil.initChapterId(entityInfo, nextId);
                onFirstLoading();
            } else {
                showFailTips(Text.TIP_NO_CHAPTER_NEXT);
            }
        });

        llList.setOnClickListener(v -> {
            hideView(bottomView);
            displayView(rightView);
        });

        LinearLayout llBottomMain = bottomView.findViewById(R.id.llBottomMain);
        llBottomMain.setOnClickListener(v -> {
        });

        TextView tvDark = bottomView.findViewById(R.id.tvDark);
        ImageButton ibDark = bottomView.findViewById(R.id.ibDark);

        String timeText = Data.isLight ? Text.TIME_SUN : Text.TIME_MOON;
        int timeDrawableId = Data.isLight ? R.drawable.ic_baseline_brightness_1_24 : R.drawable.ic_baseline_brightness_2_24;
        tvDark.setText(timeText);
        ibDark.setImageDrawable(getDrawable(timeDrawableId));
        llDark.setOnClickListener(v -> {
            if (!Data.isLight) {
                Data.isLight = true;
                hideView(darkView);
                tvDark.setText(Text.TIME_SUN);
                AnimationUtil.changeDrawable(ibDark, getDrawable(R.drawable.ic_baseline_brightness_1_24));
            } else {
                Data.isLight = false;
                displayView(darkView);
                tvDark.setText(Text.TIME_MOON);
                AnimationUtil.changeDrawable(ibDark, getDrawable(R.drawable.ic_baseline_brightness_2_24));
            }
        });

        TextView tvFav = bottomView.findViewById(R.id.tvFav);
        ImageButton ibFav = bottomView.findViewById(R.id.ibFav);
        if (entity.getStatus() == EntityUtil.STATUS_FAV) {
            tvFav.setText(Text.FAV_YES);
            ibFav.setImageDrawable(getDrawable(R.drawable.ic_baseline_favorite_24));
        } else {
            tvFav.setText(Text.FAV_NO);
            ibFav.setImageDrawable(getDrawable(R.drawable.ic_baseline_favorite_border_24));
        }
        llFav.setOnClickListener(v -> {
            if (entity.getStatus() == EntityUtil.STATUS_FAV) {
                tvFav.setText(Text.FAV_NO);
                AnimationUtil.changeDrawable(ibFav, getDrawable(R.drawable.ic_baseline_favorite_border_24));
            } else {
                tvFav.setText(Text.FAV_YES);
                AnimationUtil.changeDrawable(ibFav, getDrawable(R.drawable.ic_baseline_favorite_24));
            }
            EntityUtil.removeEntity(entity);
            entity.setStatus(entity.getStatus() == EntityUtil.STATUS_FAV ? EntityUtil.STATUS_HIS : EntityUtil.STATUS_FAV);
            EntityUtil.first(entity);
            DBUtil.save(entity, DBUtil.SAVE_CUR);
        });

        llSettings.setOnClickListener(v -> {
            hideView(bottomView);
            displayView(settingsView);
        });

        llChapter.setOnClickListener(v -> {
            onBackPressed();
        });

        recycleView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(@NonNull RecyclerView rv, @NonNull MotionEvent e) {
                touchX = e.getX();
                touchY = e.getY();
                action = e.getAction();
                return false;
            }

            @Override
            public void onTouchEvent(@NonNull RecyclerView rv, @NonNull MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });

        setSettingsView(settingsView);
    }

    private boolean refreshNum(RecyclerView recyclerView) {
        LinearLayoutManager manager = (LinearLayoutManager) recyclerView.getLayoutManager();
        if (manager != null) {
            int firstPosition = manager.findFirstVisibleItemPosition();
            if (first != firstPosition) {
                first = firstPosition;
                childCount = manager.getChildCount();
                return first >= 0 && first < contentList.size();
            }
        }
        return false;
    }

    @Override
    protected RecyclerView.OnScrollListener getOnScrollListener() {
        return new RecyclerView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (null == _mActivity) return;
                scrollChanged();
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                //防止滑动seekBar与onScrolled发生冲突
                //手动滑动时取消显示菜单
                if (!isSmooth && action == MotionEvent.ACTION_MOVE) {
                    goneViews();
                }
                if (refreshNum(recyclerView)) {
                    Content content = contentList.get(first);
                    setScrollValue(content);

                    //防止滑动seekBar与onScrolled发生冲突
                    if (!isSmooth) {
                        //设置seekBar position
                        seekBar.setProgress(content.getCur());
                    }
                }
            }
        };
    }

    protected void scrollChanged() {
        try {
            if (Data.contentCode == Data.NOVEL_CODE) {
                TheCheckBox checkBoxAuto = settingsView.findViewById(R.id.checkBoxAuto);
                if (checkBoxAuto != null) {
                    checkBoxAuto.setCheck(false);
                    _mActivity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                }
            }
            Content content = contentList.get(first);
            if (content != null && entityInfo.getChapterNum() != content.getCur()) {
//                //设置数据
//                setScrollValue(content);
                //保存当前页码
                entityInfo.setChapterNum(content.getCur());
                DBUtil.saveInfoData(entityInfo);
                //预加载
                preLoadImage(content);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void preLoadImage(Content content) {
        if (content.getUrl() != null) {
            int bottom = first + childCount;
            int min = Math.max(bottom, bottomIndex);
            int max = Math.min(bottom + preloadNum, contentList.size());
            for (int i = min; i < max; i++) {
                Source source = SourceUtil.getSource(entity.getSourceId());
                ImageConfig config = new ImageConfig();
                config.setUrl(contentList.get(i).getUrl());
                config.setHeaders(source.getImageHeaders());
                ImageUtil.preloadImage(config);
            }
            bottomIndex = max;
        }
        if (adapter.getLoadMoreModule().isEnableLoadMore()) {
            //阅读至章节一半时开始预加载下一章
            if (first + content.getTotal() / 2 > contentList.size() && !preloadLock) {
                preloadLock = true;
                adapter.getLoadMoreModule().loadMoreToLoading();
            }
        }
    }

    @Override
    public void onFirstLoading() {
        goneViews();
        first = 0;
        childCount = 0;
        super.onFirstLoading();
    }

    @Override
    protected void requestServer() {
        if (isFirst) {
            startLoad(entity.getInfo().getCurChapterId());
        } else {
            int chapterId;
            if (isHeadFresh) {
                //获取上一章id
                chapterId = contentList.get(first).getChapterId() - 1;
            } else {
                //获取下一章id
                chapterId = contentList.get(contentList.size() - 1).getChapterId() + 1;
            }
            if (EntityUtil.checkChapterId(chapterInfoList, chapterId, entityInfo.getOrder())) {
                //通过检查，加载相应章节
                startLoad(chapterId);
            } else {
                if (isHeadFresh) {
                    //加载上一章时未通过
                    setPullLayoutEnabled(false);
                    showFailTips(Text.TIP_NO_CHAPTER_PREV);
                } else {
                    //加载下一章时未通过
                    onComplete(null);
                }
            }
        }
    }

    private void startLoad(int chapterId) {
        presenter.load(entity, chapterId);
    }

    protected boolean checkMenuClick(int position) {
        int height = QMUIDisplayHelper.getScreenHeight(getContext());
        int length = height / 3;
        if (touchY < length) {
            recycleView.smoothScrollBy(0, -(height / 2), null, 100);
        } else if (touchY > length * 2) {
            recycleView.smoothScrollBy(0, (height / 2), null, 100);
        } else {
            return true;
        }
        scrollChanged();
        return false;
    }

    @Override
    public void onItemClick(@NonNull BaseQuickAdapter<?, ?> adapter, @NonNull View view, int position) {
        if (!hideViews()) {
            if (!checkMenuClick(position)) {
                return;
            }
            setFullScreen(false);
            displayView(bottomView);
        }
    }

    protected boolean checkPosition(int position) {
        return position >= 0 && position < adapter.getData().size();
    }

    protected boolean hideView(View view) {
        return AnimationUtil.changeViewVisibility(view, false);
    }

    protected boolean displayView(View view) {
        return AnimationUtil.changeViewVisibility(view, true);
    }

    protected boolean hideViews() {
        boolean success = false;
        View[] views = new View[]{bottomView, rightView, settingsView};
        for (View view : views) {
            boolean result = hideView(view);
            if (result && !success) {
                success = true;
            }
        }
        setFullScreen(Data.isFull);
        return success;
    }

    protected void goneViews(View... views) {
        for (View view : views) {
            if (view.getVisibility() != View.GONE) {
                view.clearAnimation();
                view.setVisibility(View.GONE);
            }
        }
    }

    protected void goneViews() {
        goneViews(bottomView, rightView, settingsView);
        if (Data.isLight) {
            goneViews(darkView);
        }
        setFullScreen(Data.isFull);
    }

    protected ViewGroup.LayoutParams getLP() {
        int width = QMUIDisplayHelper.getScreenWidth(_mActivity);
        int height = QMUIDisplayHelper.getScreenHeight(_mActivity) + QMUIDisplayHelper.getStatusBarHeight(_mActivity) + QMUIDisplayHelper.getActionBarHeight(_mActivity);
        return new ViewGroup.LayoutParams(width, height);
    }

    protected RecyclerView.LayoutManager getLayoutManager(int type) {
        RecyclerView.LayoutManager layoutManager;
        switch (type) {
            case TYPE_GRID:
                layoutManager = new GridLayoutManager(getActivity(), setColumn());
                break;
            case TYPE_STAGGERED:
                layoutManager = new StaggeredGridLayoutManager(setColumn(), StaggeredGridLayoutManager.VERTICAL);
                ((StaggeredGridLayoutManager) layoutManager).setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_NONE);
                break;
            default:
                layoutManager = new ScrollSpeedLinearLayoutManager(getActivity());
                break;
        }
        return layoutManager;
    }

    @Override
    public BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void loadSourceComplete(String errorMsg, String json, String tag, Request request) {
        if (errorMsg != null) {
            onFail(errorMsg);
            return;
        }
        List<Content> contentList = CacheUtil.toArray(json, Content.class);
        if (contentList.isEmpty()) {
            DBUtil.deleteCache(request.toString());
            onFail(Text.TIP_NO_DATA);
            return;
        }
        if (isFirst || isHeadFresh) {
            recycleView.scrollToPosition(chapterNum);
            seekBar.setProgress(chapterNum);
            bottomIndex = chapterNum;
            setScrollValue(contentList.get(chapterNum));
            onComplete(contentList);
            goneViews();
        } else {
            onComplete(contentList);
        }
        this.contentList = adapter.getData();
        preLoadImage(this.contentList.get(0));
        chapterNum = 0;
        preloadLock = false;
    }

    protected abstract void firstLoadView();

    @Override
    public void onDestroy() {
        setFullScreen(false);
        ImageUtil.clearCacheMap();
        super.onDestroy();
    }
}