package com.qc.common.ui.presenter;


import com.qc.common.en.data.Data;
import com.qc.common.util.DBUtil;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import io.reactivex.android.schedulers.AndroidSchedulers;
import okhttp3.Request;
import the.one.base.ui.presenter.BasePresenter;
import the.one.base.ui.view.BaseView;
import top.luqichuang.common.model.CacheData;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/6/10 17:52
 * @ver 1.0
 */
public abstract class MyPresenter<V extends BaseView> extends BasePresenter<V> {

    private ExecutorService service = Executors.newFixedThreadPool(4);

    protected String getCacheJson(Request request) {
        if (!Data.isCache) {
            return null;
        }
        CacheData cacheData = DBUtil.findCache(request.toString());
        if (cacheData != null) {
            long cacheTime = cacheData.getDate().getTime();
            long curTime = System.currentTimeMillis();
            if (curTime - cacheTime < cacheData.getExpire()) {
                return cacheData.getJson();
            } else {
                DBUtil.deleteCache(cacheData);
            }
        }
        return null;
    }

    protected void mainTread(Runnable runnable) {
        AndroidSchedulers.mainThread().scheduleDirect(runnable);
    }

    protected void newTread(Runnable runnable) {
        service.execute(runnable);
    }

}
